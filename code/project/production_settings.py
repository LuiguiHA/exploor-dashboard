from project.settings import *

STATIC_ROOT = os.path.join(BASE_DIR, 'static') 
STATICFILES_FINDERS = [
    # 'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

RAVEN_CONFIG = {
   'dsn': 'https://d67de43a3f1949d6ab98c4c7d8559b35:8cc2e47076be4dbdb00ed8c0603b31e2@sentry.io/1190881',
   # If you are using git, you can also automatically configure the
   # release based on the git info.
   # ‘release’: raven.fetch_git_sha(os.path.dirname(os.pardir)),
}

DEBUG = True