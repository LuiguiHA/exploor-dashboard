COMPANY_NOT_SENT = (1001, "Missing company id's")
COMPANY_INVALID_SENT = (1002, "Content of companies invalid")
TOKEN_INVALID_SENT = (1003, "Invalid token")
SERVICE_DONT_EXIST_PK = (1004, "service id not available")
SERVICE_NOT_SENT = (1005, "Missing service id")
