from django import template
from django.conf import settings

register = template.Library()

@register.simple_tag(takes_context=True)
def get_user_name(context):
    if context.request.user.is_authenticated:
        return context.request.user.get_full_name()
    else:
        return ''