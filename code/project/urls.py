"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from apps.clientes.views import RegisterView, Activate, ResendEmail, ValidateEmailView, LoginSuccess
from apps.servicios.api_views import ServiceList, ServiceDetail
from apps.ordenes.api_views import OrderApiView
from apps.configuracion.views import ConfigView, CountryView, SaveEmailInfoView
from apps.agentes.views import AgentView, InviteView, SearchAgentView, \
                               AgentListView, UnLinkAgentView, ReInviteView, AgentInternView
from rest_framework_jwt.views import obtain_jwt_token
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^admin/login/$', LoginSuccess.as_view(),
           {'template_name': 'admin/login.html'}, name='auth_login_'),
    url(r'^admin/', admin.site.urls),
    url(r'^$', RegisterView.as_view()),
    url(r'^validate-email/$', ValidateEmailView.as_view()),
    url(r'^resend$', ResendEmail.as_view()),
    url(r'^admin/administracion/$', admin.site.admin_view(ConfigView.as_view()), name="config"),
    url(r'^country/$', CountryView.as_view(), name="country"),
    url(r'^save-email/$', admin.site.admin_view(SaveEmailInfoView.as_view()), name="save_email"),
    url(r'^agent/(?P<token>[0-9A-Za-z\-]+)$', AgentView.as_view(), name="agent"),
    url(r'^invite/$', admin.site.admin_view(InviteView.as_view()), name="invite"),
    url(r'^search-agent/$', admin.site.admin_view(SearchAgentView.as_view()), name="search"),
    url(r'^agent-list/$', admin.site.admin_view(AgentListView.as_view()), name="agent-list"),
    url(r'^agent-intern/$', admin.site.admin_view(AgentInternView.as_view()), name="agent_intern"),
    url(r'^unlink-agent/$', admin.site.admin_view(UnLinkAgentView.as_view()), name="unlink-agent"),
    url(r'^reinvite/$', admin.site.admin_view(ReInviteView.as_view()), name="reinvite"),
    # url(r'^admin/administracion/my-account/$', admin.site.admin_view(AccountView.as_view()), name="account"),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        Activate.as_view(), name='activate'),
    url('', include('social_django.urls', namespace='social')),
    #################### RESET PASSWORD ####################
    url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
    ##################### API ###############################
    url(r'^services/$', ServiceList.as_view()),
    url(r'^service/(\d+)$', ServiceDetail.as_view()),
    url(r'^order$', OrderApiView.as_view()),
]  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

from social_django.models import UserSocialAuth, Nonce, Association

admin.site.unregister(UserSocialAuth)
admin.site.unregister(Nonce)
admin.site.unregister(Association)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
