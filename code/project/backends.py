from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

class EmailBackend(ModelBackend):
    def authenticate(self, username=None, password=None, **kwargs):
        UserModel = get_user_model()
        print (UserModel)
        try:
            print (username)
            user = UserModel.objects.get(email=username)
            print (user)
            print (getattr(user, 'is_active', False))
            print (user.check_password(password))
            if getattr(user, 'is_active', False) and user.check_password(password):
                return user
            else:
                return None
        except UserModel.DoesNotExist:
            return None

        return None
