from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE
from apps.servicios.models import DeletedObjectManager


class Office(SafeDeleteModel):
    objects = DeletedObjectManager()

    name = models.CharField(
        max_length=50,
        verbose_name="Nombre"
    )

    company = models.ForeignKey(
        "usuarios.Company",
        related_name="offices",
        verbose_name="Empresa",
        on_delete=models.DO_NOTHING
    )

    created_at = models.DateTimeField(
       auto_now_add=True,
       editable=False
    )

    updated_at = models.DateTimeField(
       auto_now=True,
       editable=False
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Oficina"
        verbose_name_plural = "Oficinas"
