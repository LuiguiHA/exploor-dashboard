# from django.contrib import admin
# from django.conf.urls import url
# from django.contrib.auth.models import User
# from django.contrib.auth.models import Group
# from django.contrib import admin
# from django.urls import reverse
# from django.core import serializers
# from django.shortcuts import redirect
# from .models import Office
#
#
# @admin.register(Office)
# class OfficeAdmin(admin.ModelAdmin):
#
#     search_fields = ['name']
#     list_filter_custom = ["company"]
#     list_display = ["name","company"]
#
#     def get_form(self, request, obj=None, **kwargs):
#         current_user = request.user
#         if not request.user.is_superuser:
#             self.exclude = ('company',)
#             self.list_display = ('name',)
#         else:
#             self.exclude = []
#             self.list_display = ('name', 'company')
#         return super(OfficeAdmin, self).get_form(request, obj, **kwargs)
#
#     def get_queryset(self, request):
#         qs = super(OfficeAdmin, self).get_queryset(request)
#         if request.user.is_superuser:
#             return qs
#         return qs.filter(company=request.user.profile.company)
#
#     def get_list_filter(self, request):
#         if request.user.is_superuser:
#             return self.list_filter_custom
#         else:
#             return []
#
#     def get_list_display(self,request):
#         if request.user.is_superuser:
#             return self.list_display
#         else:
#             return ["name"]
#
#     def save_model(self, request, obj, form, change):
#         if not request.user.is_superuser:
#             obj.company = request.user.profile.company
#         obj.save()
