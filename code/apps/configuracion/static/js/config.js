ELEMENT.locale(ELEMENT.lang.es)
// console.log()
Vue.use(VeeValidate,{fieldsBagName:'fieldsVee',errorBagName:'errorsVee', events:"input|change"})
VeeValidate.Validator.setLocale("es")
var BeatLoader = VueSpinner.BeatLoader
// Vue.component('file-upload', VueUploadComponent)
var app = new Vue({
  el: '#config',
  delimiters: ['${', '}'],
  components: {
      BeatLoader
  },
  data: {
    activeUsersTab: 'confirmed',
    userInfo: window.userInfo,
    companies: window.companiesInfo,
    countries: window.countries,
    cities: window.cities,
    countryUrl: window.countryUrl,
    saveEmailUrl: window.saveEmailUrl,
    agentInternUrl: window.agentInternUrl,
    user: {
      companyName: "",
      companyUrl: "",
      companyPhone: "",
      companyEmail: "",
      companyAddress: "",
      companyRuc: "",
      companyCountry: "",
      companyCity: "",
      name: "",
      lastName: "",
      email: "",
      password: "",
      isOperator: window.operator,
      isCompany: false,
      citiesAgent: [],
      changeLogo: false,
      pruebita: ''
    },
    loadingEmails: false,
    agentsEmail: [],
    optionsEmail: [],
    image: null,
    imageAgent: null,
    imageAgentUrl: '',
    imageUrl: '',
    privateKey: window.privateKey,
    publicKey: window.publicKey,
    step : 1,
    emailInfo: window.notification,
    emailInfoEnglish: window.notificationEnglish,
    notificationLanguage: '',
    loading: false,
    emailFirst: '',
    agentsConfirm: [],
    agentsPending: [],
    loadingAgents: true,
    loadingSaveEmail: false
  },
  watch: {
    step: function () {
      this.loadingAgents = true
      if(this.step == 3){
        axios.get('/agent-list/')
        .then((response) =>{
          this.agentsConfirm = response.data.agents_confirm
          this.agentsPending = response.data.agents_pending
          this.loadingAgents = false
        });
      }
      if (this.step == 4){
        $(this.$refs.agentsInviter.$refs.input).alphanum({
          allow :'@._-',
          allowSpace: false
        });
      }
    }
  },
  mounted () {
    this.parseUser();
    document.getElementById('config').style.display = 'block';
  },
  methods:{
    parseUser: function() {
      this.user.name = this.userInfo.name
      this.user.lastName = this.userInfo.last_name
      this.user.email = this.userInfo.email
      this.emailFirst = this.userInfo.email
      if(this.user.isOperator == 'False'){
        this.user.companyName = this.userInfo.company_name
        this.user.companyUrl = this.userInfo.company_url
        this.user.companyEmail = this.userInfo.company_email
        this.user.companyAddress = this.userInfo.company_address
        this.user.companyPhone = this.userInfo.company_phone
        this.user.companyCountry = this.userInfo.company_country
        this.user.companyCity = this.userInfo.company_city
        this.user.citiesAgent = this.userInfo.cities_agent
        if(this.userInfo.is_company == 1){
          this.user.isCompany = true
        } else {
          this.user.isCompany = false
        }
        this.imageAgentUrl = this.userInfo.logo
      }
      
      // this.company.name = this.companyInfo.name
      // this.company.phone = this.companyInfo.phone
      // this.company.email = this.companyInfo.email
      // this.company.address = this.companyInfo.address
      // this.company.ruc = this.companyInfo.ruc
      // this.company.url = this.companyInfo.url
      // this.company.country = this.companyInfo.country
      // this.company.city = this.companyInfo.city
      // this.imageUrl = this.companyInfo.logo
      for (var i = this.companies.length - 1; i >= 0; i--) {
        var company = this.companies[i]
        company['changeLogo'] = false
      }
    },
    removeLogo: function (){
      this.companies[0].changeLogo = true
      this.companies[0].logo = ''
      this.image = null
    },
    removeLogoAgent: function (){
      this.user.changeLogo = true
      this.imageAgentUrl = ''
      this.imageAgent = null
    },
    gotoStep: function (index){
      this.step = index
    },
    changeImage: function(file){
      this.companies[0].changeLogo = true
      this.companies[0].logo = file.url
      this.image = file.raw
    },
    changeImageAgent: function(file){
      this.user.changeLogo = true
      this.imageAgentUrl = file.url
      this.imageAgent = file.raw
    },
    changeCountry: function () {
      // this.$nextTick(function () {
      //   this.errorsVee.remove('city')
      // })
      if (this.companies[0].country  == 0){
        this.cities = []
        this.companies[0].city = ""
      }
      else{
        axios.post(this.countryUrl, {
          countryId: this.companies[0].country
        })
        .then((response) =>{
          var cities = response.data.cities
          this.companies[0].city = ""
          this.cities = cities
        })
      }
    },
    changeCountryAgent: function () {
      if (this.user.companyCountry  == 0){
        this.user.citiesAgent = []
        this.user.companyCity = ""
      }
      else{
        axios.post(this.countryUrl, {
          countryId: this.user.companyCountry
        })
        .then((response) =>{
          var cities = response.data.cities
          this.user.companyCity = ""
          this.user.citiesAgent = cities
        })
      }
    },
    saveData: function (){
      var formData = new FormData()
      formData.append('image', this.image)
      formData.append('imageAgent', this.imageAgent)
      formData.append('user', JSON.stringify(this.user))
      formData.append('companies', JSON.stringify(this.companies))
      formData.append('privateKey', this.privateKey)
      formData.append('publicKey', this.publicKey)
      axios.post("", formData)
      .then((response) =>{
        this.loading = false;
        // location.reload(true)
        this.$message.success('Felicitaciones, Tus datos se guardaron exitosamente.');
      })
    },
    save: function () {
      this.$validator.validateAll().then((result) => {
        if (result){
          this.loading = true
          if (this.emailFirst != this.user.email){
            axios.post('/validate-email/',{'email': this.user.email})
            .then((response) =>{
                if(response.data.success == true){ //si el email fue encontrado
                    this.loading = false
                    this.$message.error('Oops, El correo ya esta siendo usado por otro usuario.');
                } else{
                  this.saveData()
                }
            })
          }
          else {
            this.saveData()
          }
        }
        else{
          this.$message.error('Oops, Corriga los errores para poder guardar sus datos.');
        }
      });

    },
    inviteAgent: function () {
      this.loadingEmails = true
      axios.post("/invite/", {'emails':this.agentsEmail})
      .then((response) =>{
        this.loadingEmails = false
        this.agentsEmail = []
        this.$message.success('Felicitaciones, Las invitaciones se realizaron con éxito.');
      })
    },
    searchAgent: function (query) {
      if (query !== '') {
          this.loading = true;
          axios.post("/search-agent/", {'query':query})
          .then((response) =>{
            this.loading = false;
            if (response.data.success){
              this.optionsEmail = response.data.agents
            }
          })
        } else {
          this.optionsEmail = [];
        }
    },
    unLinkAgent: function (index, email){
      this.agentsConfirm.splice(index, 1)
      axios.post("/unlink-agent/", {'email':email})
      .then((response) =>{
        this.$message.success('¡El Agente de correo "'+ email +'" fue desvinculado con éxito!');
      })
    },
    reInviteAgent: function (index, email){
      axios.post("/reinvite/", {'email':email})
      .then((response) =>{
        this.agentsPending[index].date = response.data.date
        this.$message.success('¡Se envió la invitación a "'+ email +'" exitosamente!');
      })
    },
    setEmailInfo: function(value){
      if (this.notificationLanguage == 'es'){
        var position = this.$refs['emailInfo'].$el.getElementsByTagName('TEXTAREA')[0].selectionStart
        if(position){
          var before = this.emailInfo.substring(0, position)
          var after = this.emailInfo.substring(position, this.emailInfo.length)
          this.emailInfo = before + '{{' + value + '}}' + after
        } else{
          this.emailInfo = '{{' + value + '}}'
        }
      } else{
        var position = this.$refs['emailInfoEnglish'].$el.getElementsByTagName('TEXTAREA')[0].selectionStart
        if(position){
          var before = this.emailInfoEnglish.substring(0, position)
          var after = this.emailInfoEnglish.substring(position, this.emailInfoEnglish.length)
          this.emailInfoEnglish = before + '{{' + value + '}}' + after
        } else{
          this.emailInfoEnglish = '{{' + value + '}}'
        }
      }
      
    },
    saveEmailInfo: function(){
      if (this.emailInfo == '' || this.emailInfoEnglish == ''){
        this.$message.error('Por favor completar los dos campos.');
        return
      }
      this.loadingSaveEmail = true
      axios.post(this.saveEmailUrl,{
        info: this.emailInfo,
        englishInfo: this.emailInfoEnglish
      })
      .then((response) => {
        this.loadingSaveEmail = false
        this.$message.success('Se guardo el mensaje exitosamente.');
      })
    },
    setAgentIntern: function(value, id){
      axios.get(this.agentInternUrl,{
        params: {
          id: id,
          isIntern: value
        }
      })
      .then((response)=>{
        this.$message.success('Agente actualizado correctamente.');
      })
    },
    textAreaSelected: function(value){
      this.notificationLanguage = value
    },
  }


})
