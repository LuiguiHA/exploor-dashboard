from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE
from apps.usuarios.models import User

class Notification(SafeDeleteModel):
    user = models.ForeignKey(
        User
    )
    info = models.TextField(
        null=True
    )
    english_info = models.TextField(
        null=True
    )