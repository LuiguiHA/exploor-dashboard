from django.shortcuts import render, redirect
from django.views.generic import TemplateView, View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import update_session_auth_hash
from django.http import JsonResponse
from apps.usuarios.models import Company, Country, City
from apps.configuracion.models import Notification
from apps.agentes.models import Agent, AgentInvite
import json
import arrow
import time
from dateutil import tz
# Create your views here.


class ConfigView(View):
    template_name = "admin/config.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ConfigView, self).dispatch(request, *args, **kwargs)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = request.POST
        user = json.loads(data.get('user'))
        companies = json.loads(data.get('companies'))
        images = request.FILES
        if user.get('isOperator') == 'True':
            # DATA OF COMPANY
            company = companies[0]
            company_object = Company.objects.get(users=request.user)
            company_object.name = company.get('name').capitalize()
            company_object.company_web_url = company.get('url')
            company_object.phone = company.get('phone')
            company_object.email = company.get('email')
            company_object.address = company.get('address')
            company_object.ruc = company.get('ruc')
            company_object.private_key = data.get('privateKey')
            company_object.public_key = data.get('publicKey')
            company_object.country = None
            company_object.city = None
            if company.get('changeLogo'):
                company_object.logo = images.get('image')
            if company.get('country') != 0 and company.get('country') != "":
                country = Country.objects.get(id=company.get('country'))
                company_object.country = country.name
                if type(company.get('city')) is int:
                    city = City.objects.get(id=company.get('city'))
                    company_object.city = city.name
                else:
                    company_object.city = company.get('city')
            company_object.save()

        # DATA OF USER
        user_object = request.user
        user_object.first_name = user.get('name').capitalize()
        user_object.last_name = user.get('lastName').capitalize()
        email_preview = user_object.email
        user_object.email = user.get('email')
        if user.get('password'):
            user_object.set_password(user.get('password'))
            update_session_auth_hash(request, user_object)
        user_object.save()
        # DATA OF AGENT
        if user.get('isOperator') == 'False':
            # MODIFICA EL CORREO DE LAS INVITACIONES POR SI EL AGENTE LO ACTUALIZO
            agent_invites = list(AgentInvite.objects.filter(email=email_preview))
            for agent_invite in agent_invites:
                agent_invite.email = user.get('email')
                agent_invite.save()
                
            agent = Agent.objects.get(user=user_object)
            agent.is_company = False
            if user.get('changeLogo'):
                agent.logo = images.get('imageAgent')
            agent.private_key = data.get('privateKey')
            agent.public_key = data.get('publicKey')
            agent.name = None
            agent.url = None
            agent.phone = None
            agent.address = None
            agent.email = None
            agent.ruc = None
            agent.country = None
            agent.city = None
            if user.get('isCompany'):
                agent.is_company = True
                agent.name = user.get('companyName')
                agent.url = user.get('companyUrl')
                agent.phone = user.get('companyPhone')
                agent.address = user.get('companyAddress')
                agent.email = user.get('companyEmail')
                agent.ruc = user.get('companyRuc')
                if user.get('companyCountry') != 0 and \
                user.get('companyCountry') != '':
                    country = Country.objects.get(id=user.get('companyCountry'))
                    agent.country = country.name
                    if type(user.get('companyCity')) is int:
                        city = City.objects.get(id=user.get('companyCity'))
                        agent.city = city.name
                    else:
                        agent.city = user.get('companyCity')
            agent.save()

        return JsonResponse({'success': True})

    def get(self, request, *args, **kwargs):
        user = request.user
        countries = Country.objects.all()
        countries_list = []
        cities_list = []
        for country in countries:
            dic = {}
            dic["id"] = country.id
            dic["name"] = country.name
            countries_list.append(dic)
        companies = list(Company.objects.filter(users=user))
        companies_info = []
        # COUNTRY AND CITY OF COMPANY OF OPERATOR
        for company in companies:
            if company.country:
                country_object = Country.objects.get(name=company.country)
                country = country_object.id
                cities = City.objects.filter(country=country_object)
                for obj in cities:
                    dic = {}
                    dic["value"] = obj.id
                    dic["text"] = obj.name
                    cities_list.append(dic)

                if len(cities) == 0 or company.city == '':
                    city = company.city
                else:
                    city_object = City.objects.get(name=company.city,country=country_object)
                    city = city_object.id
            else:
                country = ""
                city = ""

            company_info = {
                'name': company.name.capitalize(),
                'url': company.company_web_url,
                'phone': company.phone if company.phone else "",
                'address': company.address if company.address else "",
                'email': company.email if company.email else "",
                'ruc': company.ruc if company.ruc else "",
                'country': country,
                'city': city,
                'logo': company.logo.url if company.logo else '',
            }
            companies_info.append(company_info)

        if request.user.type_user_id == 1 or request.user.type_user_id == 2:
            is_operator = True
            user_info = {
                'name': user.first_name.capitalize(),
                'last_name': user.last_name.capitalize(),
                'email': user.email
            }
            # LLAVES OF COMPANY
            private_key = company.private_key if company.private_key else ''
            public_key = company.public_key if company.public_key else ''
        else:
            agent = Agent.objects.get(user=user)
            # LLAVES OF AGENT
            private_key = agent.private_key if agent.private_key else ''
            public_key = agent.public_key if agent.public_key else ''
            is_operator = False
            # COUNTRY AND CITY OF COMPANY OF AGENT
            cities_agent_list = []
            country = ''
            city = ''
            if agent.country:
                country_object = Country.objects.get(name=agent.country)
                country = country_object.id
                cities = City.objects.filter(country=country_object)
                for obj in cities:
                    dic = {}
                    dic["value"] = obj.id
                    dic["text"] = obj.name
                    cities_agent_list.append(dic)
                if agent.city:
                    if len(cities) == 0:
                        city = agent.city
                    else:
                        city_object = City.objects.get(name=agent.city)
                        city = city_object.id
            user_info = {
                'name': user.first_name.title(),
                'last_name': user.last_name.title(),
                'email': user.email,
                'company_name': agent.name if agent.name else "",
                'company_ruc': agent.ruc if agent.ruc else "",
                'company_phone': agent.phone if agent.phone else "",
                'company_email': agent.email if agent.email else "",
                'company_url': agent.url if agent.url else "",
                'company_address': agent.address if agent.address else "",
                'is_company': 1 if agent.is_company else 2,
                'company_country': country,
                'company_city': city,
                'cities_agent': cities_agent_list,
                'logo': agent.logo.url if agent.logo else ''
            }
        
        noti_exists = Notification.objects.filter(user=request.user).exists()
        info = ''
        english_info = ''
        if noti_exists:
            noti = Notification.objects.get(user=request.user)
            info = noti.info
            english_info = noti.english_info
        
        context = {'menu_index': 4,
                   'logged_user': json.dumps(user_info),
                   'companies': json.dumps(companies_info),
                   'countries': json.dumps(countries_list),
                   'cities': json.dumps(cities_list),
                   'operator': is_operator,
                   'public_key': public_key,
                   'private_key': private_key,
                   'notification': info,
                   'notification_english': english_info}

        return render(request, self.template_name, context)


class CountryView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CountryView, self).dispatch(request, *args, **kwargs)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        country_id = data.get('countryId')
        country = Country.objects.get(id=country_id)
        cities = City.objects.filter(country=country)
        cities_list = []
        for obj in cities:
            dic = {}
            dic["value"] = obj.id
            dic["text"] = obj.name
            cities_list.append(dic)
        return JsonResponse({'cities': cities_list, 'success': True})


class SaveEmailInfoView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(SaveEmailInfoView, self).dispatch(request, *args, **kwargs)
    
    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        info = data.get('info')
        english_info = data.get('englishInfo')
        quantity = list(Notification.objects.filter(user=request.user))
        if len(quantity) > 0:
            noti = Notification.objects.get(user=request.user)
            noti.info = info
            noti.english_info = english_info
            noti.save()
        else:
            notification = Notification.objects.create(user=request.user, info=info, english_info=english_info)
            notification.save()
        return JsonResponse({'success': True})