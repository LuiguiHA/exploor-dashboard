from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE
from safedelete.managers import SafeDeleteManager
from safedelete.config import DELETED_VISIBLE_BY_PK
from stdimage.models import StdImageField
# Create your models here.


class DeletedObjectManager(SafeDeleteManager):
    _safedelete_visibility = DELETED_VISIBLE_BY_PK


class ServiceCategory(SafeDeleteModel):

    name = models.CharField(
        max_length=250,
    )

    description = models.TextField()

    def __str__(self):
        return self.name


class PriceCategory (SafeDeleteModel):
    """ Categorias de precio del service """
    objects = DeletedObjectManager()

    name = models.CharField(
        null=True,
        max_length=350
    )

    description = models.CharField(
        null=True,
        max_length=350
    )

    excluded = models.BooleanField(
        default=False
    )

    fields_disabled = models.CharField(
        max_length=250,
        null=True
    )


class Service(SafeDeleteModel):
    UNITS_CHOICES = (
        ('HORAS', 'Horas'),
        ('MINUTOS', 'Minutos'),
        ('DÍAS', 'Días'),
    )

    """ Services All """
    objects = DeletedObjectManager()

    category = models.ForeignKey(
        ServiceCategory,
        null=True,
        on_delete=models.DO_NOTHING,
        verbose_name="Categoría"
    )

    title = models.TextField(
        null=True
    )

    code = models.CharField(
        null=True,
        max_length=50,
        verbose_name="Código"
    )

    duration = models.CharField(
        null=True,
        max_length=50,
        verbose_name="Duración"
    )

    duration_unit = models.CharField(
        max_length=50,
        choices=UNITS_CHOICES
    )
    min_people = models.IntegerField(
        null=True,
    )

    max_people = models.IntegerField(
        null=True,
    )

    user = models.ForeignKey(
        "usuarios.User",
        related_name="services",
        on_delete=models.DO_NOTHING,
        verbose_name="Usuario"
    )

    description_short = models.TextField(
        null=True,
    )

    description_long = models.TextField(
        null=True
    )

    video = models.CharField(
        max_length=500,
        null=True
    )

    since = models.DateTimeField(
        null=True
    )

    until = models.DateTimeField(
        null=True
    )

    terms_conditions = models.TextField(
        null=True
    )

    published = models.BooleanField(
        verbose_name="Publicado",
        default=False
    )

    itinerary = models.TextField(
        null=True
    )

    included = models.TextField(
        null=True
    )

    not_included = models.TextField(
        null=True
    )

    recommendations = models.TextField(
        null=True
    )

    additional_information = models.TextField(
        null=True
    )

    draft = models.BooleanField(
        verbose_name="Borrador"
    )

    currency = models.ForeignKey(
        'ordenes.Currency',
        null=True,
        on_delete=models.DO_NOTHING
    )

    company = models.ForeignKey(
        'usuarios.Company',
        verbose_name="Compañia"
    )

    created_at = models.DateTimeField(
       auto_now_add=True,
       editable=False
    )

    updated_at = models.DateTimeField(
       auto_now=True,
       editable=False
    )

    def title_custom(self):
        if self.title and not self.draft:
            return self.title
        elif self.title and self.draft:
            return self.title + " - <strong>borrador</strong>"
        else:
            return "Borrador #{} - <strong>borrador</strong>".format(self.id)

    title_custom.short_description = "Nombre"
    title_custom.allow_tags = True

    class Meta:
        verbose_name = 'Servicio'
        verbose_name_plural = 'Servicios'

    def __str__(self):
        if self.title and not self.draft:
            return self.title
        elif self.title and self.draft:
            return self.title + " - borrador"
        else:
            return "Borrador #{} - borrador".format(self.id)

    def company_name(self):
        companies = self.user.company_set.all()
        return ','.join([company.name.capitalize() for company in companies])
    company_name.short_description = "Empresa"


class ServiceSchedule (SafeDeleteModel):

    day = models.IntegerField(
        null=True,
        blank=True
    )

    hour = models.TimeField(
        null=True
    )

    service = models.ForeignKey(
        Service,
        on_delete=models.DO_NOTHING,
        related_name="schedule"
    )

    stock = models.IntegerField(
        default = 0
    )
    total = models.IntegerField(
        default = 0
    )

    class Meta:
        verbose_name = "Horario disponible del servicio"
        verbose_name_plural = "Horarios disponibles de servicio"
        ordering = ["-day", "-hour"]

    def __str__(self):
        return "Horario " + str(self.id)


class ServiceExcludedDates (SafeDeleteModel):

    since = models.DateTimeField(
        null=True
    )

    until = models.DateTimeField(
        null=True
    )

    repeat_every_year = models.BooleanField(
        verbose_name="Repetir todos los años"
    )

    service = models.ForeignKey(
        Service,
        related_name="excluded_dates",
        on_delete=models.DO_NOTHING
    )

    class Meta:
        verbose_name = "Fecha excluida"
        verbose_name_plural = "Fechas excluida"

    def __str__(self):
        return "Fecha exlcuida " + str(self.id)


class ServicePrice (SafeDeleteModel):
    """ Monto a pagar de las categorias de precios de los serviceos """
    objects = DeletedObjectManager()

    POR_PERSONA = 1
    POR_GRUPO = 2
    PRICE_CHOICES = (
        (POR_PERSONA, 'Por persona'),
        (POR_GRUPO, 'Por grupo'),
    )

    category = models.ForeignKey(
        PriceCategory,
        on_delete=models.DO_NOTHING
    )

    price = models.FloatField(
        null=True,
    )

    last_price = models.FloatField(
        null=True,
    )

    min_age = models.IntegerField(
        null=True,
    )

    max_age = models.IntegerField(
        null=True,
    )

    min_people = models.IntegerField(
        null=True,
        blank=True
    )

    max_people = models.IntegerField(
        null=True,
        blank=True
    )

    group_price_applied_to = models.IntegerField(
        null=True,
        blank=True,
        choices=PRICE_CHOICES
    )

    service = models.ForeignKey(
        Service,
        related_name="prices",
        on_delete=models.DO_NOTHING
    )

    enabled = models.BooleanField(
    )

    default = models.BooleanField(
    )

    class Meta:
        ordering = ["-id"]
        verbose_name = "Precio de servicio"
        verbose_name_plural = "Precios de servicios"

    def __str__(self):
        if self.service.currency and self.price:
            return self.category.name + " - " + self.service.currency.symbol + '{:.2f}'.format(self.price)
        elif self.price:
            return self.category.name + " - " + '{:.2f}'.format(self.price)
        else:
            return self.category.name


class ServiceImage(SafeDeleteModel):

    image = StdImageField(
        upload_to='service_images',
        verbose_name="Imagen",
        variations={
            'large': (600, 400),
            'medium': (300, 200),
            'small': (100, 100)
        }
    )

    service = models.ForeignKey(
        Service,
        related_name="images",
        on_delete=models.DO_NOTHING
    )

    class Meta:
        verbose_name = "Imagen de servicio"
        verbose_name_plural = "Imágenes de servicios"

    def __str__(self):
        return "Imagen " + str(self.id)


class FieldsHelp (SafeDeleteModel):
    field_name = models.CharField(
        max_length=30,
        null=True
    )
    help_text = models.TextField(
        null=True
    )
