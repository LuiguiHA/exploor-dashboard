# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-12 19:01
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import stdimage.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FieldsHelp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('field_name', models.CharField(max_length=30, null=True)),
                ('help_text', models.TextField(null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PriceCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('name', models.CharField(max_length=350, null=True)),
                ('description', models.CharField(max_length=350, null=True)),
                ('excluded', models.BooleanField(default=False)),
                ('fields_disabled', models.CharField(max_length=250, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('title', models.CharField(max_length=50, null=True)),
                ('code', models.CharField(max_length=50, null=True, verbose_name='Código')),
                ('min_people', models.IntegerField(null=True)),
                ('max_people', models.IntegerField(null=True)),
                ('description_short', models.CharField(max_length=250, null=True)),
                ('description_long', models.TextField(null=True)),
                ('video', models.CharField(max_length=250, null=True)),
                ('since', models.DateTimeField(null=True)),
                ('until', models.DateTimeField(null=True)),
                ('terms_conditions', models.TextField(null=True)),
                ('published', models.BooleanField(default=False, verbose_name='Publicado')),
                ('itinerary', models.TextField(null=True)),
                ('included', models.TextField(null=True)),
                ('not_included', models.TextField(null=True)),
                ('recommendations', models.TextField(null=True)),
                ('additional_information', models.TextField(null=True)),
                ('draft', models.BooleanField(verbose_name='Borrador')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Servicio',
                'verbose_name_plural': 'Servicios',
            },
        ),
        migrations.CreateModel(
            name='ServiceCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ServiceExcludedDates',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('since', models.DateTimeField(null=True)),
                ('until', models.DateTimeField(null=True)),
                ('repeat_every_year', models.BooleanField(verbose_name='Repetir todos los años')),
                ('service', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='excluded_dates', to='servicios.Service')),
            ],
            options={
                'verbose_name': 'Fecha excluida',
                'verbose_name_plural': 'Fechas excluida',
            },
        ),
        migrations.CreateModel(
            name='ServiceImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('image', stdimage.models.StdImageField(upload_to='service_images', verbose_name='Imagen')),
                ('service', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='images', to='servicios.Service')),
            ],
            options={
                'verbose_name': 'Imagen de servicio',
                'verbose_name_plural': 'Imágenes de servicios',
            },
        ),
        migrations.CreateModel(
            name='ServicePrice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('price', models.FloatField(null=True)),
                ('last_price', models.FloatField(null=True)),
                ('min_people', models.IntegerField(blank=True, null=True)),
                ('max_people', models.IntegerField(blank=True, null=True)),
                ('group_price_applied_to', models.IntegerField(blank=True, choices=[(1, 'Por persona'), (2, 'Por grupo')], null=True)),
                ('enabled', models.BooleanField()),
                ('default', models.BooleanField()),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='servicios.PriceCategory')),
                ('service', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='prices', to='servicios.Service')),
            ],
            options={
                'verbose_name': 'Precio de servicio',
                'verbose_name_plural': 'Precios de servicios',
                'ordering': ['-id'],
            },
        ),
        migrations.CreateModel(
            name='ServiceSchedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('deleted', models.DateTimeField(editable=False, null=True)),
                ('day', models.IntegerField(blank=True, null=True)),
                ('hour', models.TimeField(null=True)),
                ('service', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='schedule', to='servicios.Service')),
            ],
            options={
                'verbose_name': 'Horario disponible del servicio',
                'verbose_name_plural': 'Horarios disponibles de servicio',
                'ordering': ['-day', '-hour'],
            },
        ),
        migrations.AddField(
            model_name='service',
            name='category',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='servicios.ServiceCategory', verbose_name='Categoría'),
        ),
    ]
