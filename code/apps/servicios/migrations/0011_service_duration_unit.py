# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-04-11 18:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('servicios', '0010_service_company'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='duration_unit',
            field=models.CharField(choices=[('HORAS', 'Horas'), ('MINUTOS', 'Minutos'), ('DÍAS', 'Días')], default='DÍAS', max_length=50),
            preserve_default=False,
        ),
    ]
