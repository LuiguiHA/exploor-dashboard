# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-01-30 18:55
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('servicios', '0002_auto_20171212_1401'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='company',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='services', to='usuarios.Company', verbose_name='Empresa'),
        ),
    ]
