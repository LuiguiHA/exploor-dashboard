from django.contrib import admin
from django.conf.urls import url
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib import admin
from django.urls import reverse
from django.core import serializers
from django.shortcuts import redirect

from apps.ordenes.models import Currency
from apps.usuarios.models import Company
from apps.servicios.models import (Service, ServiceCategory,
    FieldsHelp, PriceCategory, ServicePrice, ServiceSchedule,
    ServiceExcludedDates, ServiceImage)
from .views import (RegisterServiceView, ImageServiceView, DeleteImageServiceView,
    DeleteServiceView,ServiceByNameView)
import json
import jsonpickle
import arrow
from datetime import timedelta, tzinfo
import datetime
from dateutil import tz
import base64
from hashids import Hashids


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):

    search_fields = ['title','code']

    list_display = ["title_custom", "code", "category", "published","company_name"]

    list_filter_custom = ["company", "category", "published"]

    def has_add_permission(self, request):
        if request.user.is_superuser:
            return False
        else:
            return True

    def get_queryset(self, request):
        qs = super(ServiceAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        if request.user.type_user.id == 1 or request.user.type_user.id == 2:
            return qs.filter(user=request.user)

    def get_list_filter(self, request):
        if request.user.is_superuser:
            return self.list_filter_custom
        else:
            return ["category", "published"]

    def get_list_display(self, request):
        if request.user.is_superuser:
            return self.list_display
        else:
            return ["title_custom", "code", "category", "published"]

    def add_view(self, request, form_url='', extra_context=None):
        hashids = Hashids(min_length=6)

        price_categories = PriceCategory.objects.all()
        service = Service()
        service.user = request.user
        company = Company.objects.get(id=request.user.get_company_ids()[0])
        service.company = company
        service.draft = True
        service.save()

        service.code = hashids.encode(service.id).upper()
        service.save()
        for price_category in price_categories:
            service_price = ServicePrice()

            if price_category.name == 'Grupo':
                service_price.group_price_applied_to = 1

            service_price.category = price_category
            service_price.service = service
            service_price.enabled = False
            service_price.default = True
            service_price.save()

        service_id = service.id

        return redirect(reverse('admin:servicios_service_change', args=[service_id]))

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        try:
            int(object_id)

            hashids = Hashids(min_length=6)
            price_categories = PriceCategory.objects.all()
            service = Service.objects.get(id=int(object_id))
            user = request.user
            if not user.is_superuser:
                if service.company.id not in user.get_company_ids():
                    return redirect(reverse('admin:servicios_service_changelist'))

            service_id = service.id

            categories = list(ServiceCategory.objects.all().values('id','name', 'description'))

            fields_help = list(FieldsHelp.objects.all().values('field_name','help_text'))

            def get_schedule(schedule):
                date = datetime.datetime.now()
                date = date.replace(
                    hour=schedule.hour.hour,
                    minute=schedule.hour.minute,
                    second=schedule.hour.second
                )
                return {
                    "day": schedule.day,
                    "hour": arrow.get(
                        date, tz.gettz('America/Lima')
                    ).shift(hours=-5).format('YYYY-MM-DD HH:mm:ss ZZ')
                }

            service_schedule = ServiceSchedule.objects.filter(service=service)

            service_schedule = [get_schedule(schedule) for schedule in service_schedule]


            def get_excluded_date(excluded_date):
                return {'date':[
                            arrow.get(excluded_date.since).format('YYYY-MM-DDTHH:mm:ss.SSSSZ'),
                            arrow.get(excluded_date.until).format('YYYY-MM-DDTHH:mm:ss.SSSSZ'),
                        ],'repeated':excluded_date.repeat_every_year}

            service_excluded_dates = ServiceExcludedDates.objects.filter(service=service)
            service_excluded_dates = [get_excluded_date(excluded_date) for excluded_date in service_excluded_dates]
            service_prices = list(ServicePrice.objects.select_related("category").filter(service=service))
            def encode_image(image):
                try:
                    return ["data:image/png;base64," + base64.b64encode(image.image.read()).decode(), image.id]
                except:
                    return ""

            service_images = ServiceImage.objects.filter(service=service)
            service_images = [encode_image(service_image) for service_image in service_images]


            currencies = list(Currency.objects.all().values('id', 'name','symbol'))

            date = datetime.datetime.now()
            return super(ServiceAdmin, self).changeform_view(request, object_id, form_url, {
                'categories':categories,
                'currencies':currencies,
                'fields_help': json.dumps(fields_help),
                'service_id' : json.dumps(service_id),
                'service' : jsonpickle.encode(service, unpicklable=False),
                'service_prices' : jsonpickle.encode(service_prices, unpicklable=False),
                'service_schedule' : jsonpickle.encode(service_schedule),
                'service_excluded_dates' : jsonpickle.encode(service_excluded_dates),
                'service_images' : json.dumps(service_images),
                'is_editing' : json.dumps(True),
                'today': date.strftime('%Y-%m-%d'),
                'user_name': request.user.get_full_name()
            })
        except:
            return super(ServiceAdmin, self).changeform_view(request, object_id, form_url, extra_context)


    def response_add(self, request, obj, post_url_continue=None):
        # print("request", request)
        # url_to_redirect = reverse('admin:serviceos_service_change', args=[]
        return redirect('/admin/sales/invoice')

    def get_urls(self):
    	urls = super(ServiceAdmin, self).get_urls()

    	return [
            url(r'^save-service/$', RegisterServiceView.as_view(),name="save_service"),
            url(r'^image-service/$', ImageServiceView.as_view(),name="image_service"),
            url(r'^delete-image-service/$', DeleteImageServiceView.as_view(),name="delete_image_service"),
            url(r'^delete-service/$', DeleteServiceView.as_view(),name="delete_service"),
    		url(r'^service-by-name/$', ServiceByNameView.as_view(),name="service_by_name"),
    	] + urls
