from django.apps import AppConfig


class ServiciosConfig(AppConfig):
    name = 'servicios'
    verbose_name = 'Administración de servicios'
