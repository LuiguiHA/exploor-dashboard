from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Service
from .serializers import ServiceResponseSerializer, ServiceResponse, \
                         ServiceSerializer
from project.serializers import ErrorMessageResponseSerializer, ErrorMessageResponse
from rest_framework import permissions
from project import api_errors


class ServiceList(APIView):

	def get(self, request, format=None):
		token = request.auth
		try:
			profile = Profile.objects.get(token=token)
		except:
			return Response(ErrorMessageResponseSerializer(
				ErrorMessageResponse(api_errors.TOKEN_INVALID_SENT)
			).data)

		distribution_channel = profile.distribution_channel
		companies = distribution_channel.companies.all()
		companies_get_id = request.GET.get('companies')
		if companies_get_id:
			companies_get_id = companies_get_id.split(',')

			filtered_companies_get_id = []
			for company_get_id in companies_get_id:
				try:
					filtered_companies_get_id.append(int(company_get_id))
				except:
					pass

			if len(filtered_companies_get_id) > 0:
				if request.GET.get('fields'):
					fields = request.GET.get('fields').split(',')
					return Response(ServiceResponseSerializer(
						ServiceResponse(companies, filtered_companies_get_id,fields)
					).data)

				return Response(ServiceResponseSerializer(
						ServiceResponse(companies, filtered_companies_get_id,[])
					).data)
			else:
				return Response(ErrorMessageResponseSerializer(
					ErrorMessageResponse(api_errors.COMPANY_INVALID_SENT)
				).data)

		else:
			return Response(ErrorMessageResponseSerializer(
				ErrorMessageResponse(api_errors.COMPANY_NOT_SENT)
			).data)

class ServiceDetail(APIView):

	def get(self, request, pk, format=None):
		token =request.auth
		try:
			profile = Profile.objects.get(token=token)
		except:
			return Response(ErrorMessageResponseSerializer(
				ErrorMessageResponse(api_errors.TOKEN_INVALID_SENT)
			).data)
		distribution_channel = profile.distribution_channel
		companies = distribution_channel.companies.all()
		companies_id = []

		for company in companies:
			companies_id.append(company.id)
		services = Service.objects.filter(company__in=companies_id,\
										 draft=False,published=True \
										 ).filter(id=pk)
		if services:
			if request.GET.get('fields'):
				fields = request.GET.get('fields').split(',')
				return Response(ServiceSerializer(services,fields=fields, many=True).data)

			return Response(ServiceSerializer(services, many=True).data)
		else:
			return Response(ErrorMessageResponseSerializer(
				ErrorMessageResponse(api_errors.SERVICE_DONT_EXIST_PK)
			).data)
