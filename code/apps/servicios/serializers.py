from rest_framework import serializers
from .models import Service, ServiceCategory, ServiceImage, ServiceSchedule,\
                    ServiceExcludedDates, ServicePrice
from apps.usuarios.models import Company
from apps.ordenes.models import Currency

from datetime import datetime
import calendar 

#CLASS SERIALIZER DINAMIC
class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    """
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    """

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)

# GENERAL SERIALIZERS 
class ServiceCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceCategory
        fields = ('name','description')

class ServiceSerializer(DynamicFieldsModelSerializer):
    category = ServiceCategorySerializer(read_only=True)
    media = serializers.SerializerMethodField(read_only=True)
    company_name = serializers.SerializerMethodField(read_only=True)
    schedule = serializers.SerializerMethodField()
    excluded_dates = serializers.SerializerMethodField()
    prices = serializers.SerializerMethodField()
    since = serializers.SerializerMethodField()
    until = serializers.SerializerMethodField()
    class Meta:
        model = Service
        fields = ('id','title','company_name','category','code','min_people',
            'max_people','since','until','description_short','description_long','itinerary',
            'included','not_included','recommendations','additional_information',
            'terms_conditions','media','prices','schedule','excluded_dates', 'duration', 'duration_unit')

    def get_prices(self, service):
        prices_dic = {}
        prices_dic['currency_symbol'] = service.currency.symbol
        prices_dic['currency_code'] = service.currency.code
        prices_dic['currency_name'] = service.currency.name
        prices_list = ServicePrice.objects.filter(service=service, enabled=True)

        def get_price_dic(price):
            dic = {}
            dic['id'] = price.id
            dic['type'] = price.category.name
            dic['price_value'] = price.price
            dic['formated_price'] = service.currency.symbol + ' ' + '{:.2f}'.format(price.price)
            if price.category.name == 'Grupo':
                dic['min_people'] = price.min_people
                dic['max_people'] = price.max_people
                if price.group_price_applied_to == 1:
                    dic['price_by'] = 'persona'
                else:
                    dic['price_by'] = 'grupo'

            return dic

        prices_list = [get_price_dic(price) for price in prices_list]
        prices_dic['list'] = prices_list

        return prices_dic

    def get_since(self,service):
        since = service.since.strftime('%Y-%m-%d')
        return since

    def get_until(self,service):
        until = service.until.strftime('%Y-%m-%d')
        return until

    def get_excluded_dates(self,service):
        excluded_dates_list = ServiceExcludedDates.objects.filter(service=service)

        def get_excluded_date_dic(excluded_date):
            return {'start': excluded_date.since.strftime('%Y-%m-%d'),
                    'end': excluded_date.until.strftime('%Y-%m-%d'),
                    'every_year': excluded_date.repeat_every_year}

        excluded_dates_list = [get_excluded_date_dic(excluded_date) \
                              for excluded_date in excluded_dates_list]

        return excluded_dates_list

    def get_schedule(self,service):
        schedules_list = ServiceSchedule.objects.filter(service=service).order_by('day')
        final_schedules_list = []

        def get_schedule_dic(schedule):
            exists_day_index = False
            schedule_dic = {}
            for dic in final_schedules_list:
                if dic["day_index"] == schedule.day:
                    exists_day_index = True
                    hours_list = dic["hours"]
                    hours_list.append(schedule.hour.strftime('%H:%M'))

            if exists_day_index == False:
                hours_list = []
                hours_list.append(schedule.hour.strftime('%H:%M'))
                schedule_dic["day_index"] = schedule.day
                schedule_dic["day_name"] = calendar.day_name[schedule.day-1]
                schedule_dic["hours"] = hours_list

                final_schedules_list.append(schedule_dic)

        for schedule in schedules_list:
            get_schedule_dic(schedule)

        return final_schedules_list
    
    def get_media(self,service):
        media_dic = {}
        video = service.video
        media_dic["video"] = video
        images_list = ServiceImage.objects.filter(service=service).order_by('id')

        def get_image(image):
            return {'small': image.image.small.url,
                    'large': image.image.large.url,
                    'medium': image.image.medium.url,
                    'original': image.image.url}

        images_list = [get_image(image) for image in images_list]

        media_dic["images"] = images_list

        return media_dic

    def get_company_name(self,service):
        name = service.user.company_name()
        return name


#OBJECTS AND SERIALIZERS PERSONALIZER

class ServiceResponseSerializer(serializers.Serializer):
    services = serializers.DictField()
    message = serializers.CharField()

class ServiceResponse(object):

    def __init__(self, companies, companies_ids,fields):
        services_list = {}
        message = ""
        companies_request_ids = []
        requested_companies = companies.filter(id__in=companies_ids)
        for company in requested_companies:
            companies_request_ids.append(company.id)
            services = Service.objects.filter(
                company=company, 
                draft=False,
                published=True
            )
            if len(fields) > 0:
                serializers = ServiceSerializer(services,fields=fields, many=True)
                services_list[company.id] = serializers.data
            else:
                serializers = ServiceSerializer(services, many=True)
                services_list[company.id] = serializers.data
        
        for company_request_id in companies_request_ids:
            message = message + "Company " + str(company_request_id) \
                      + ": " + "Success, "

        companies_without_permission_ids = set(companies_ids) - set(companies_request_ids)
        for companiy_without_permission_id in companies_without_permission_ids:
            message = message + "Company " + str(companiy_without_permission_id) \
                      + ": " + "Without Permission, "

        message = message.rstrip(", ")

        self.message = message
        self.services = services_list






