from django.shortcuts import render, redirect
from django.views.generic import TemplateView, View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from apps.ordenes.models import Currency
from apps.servicios.models import ServiceCategory, PriceCategory, Service,\
            ServicePrice, ServiceSchedule, ServiceExcludedDates, ServiceImage
from apps.usuarios.models import Company
import json
import arrow
import time
from dateutil import tz
# Create your views here.


class RegisterServiceView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(RegisterServiceView, self).dispatch(request, *args, **kwargs)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        # user = request.user
        # company = user.profile.company
        id_category = data.get("selectedCategory")
        service_category = ServiceCategory.objects.get(id=id_category)
        draft = data.get("draft")
        title = data.get("title")
        code = data.get("code")
        duration = data.get("duration")
        duration_unit = data.get('durationUnits')
        min_age = data.get("minAge")
        max_age = data.get("maxAge")
        max_people = data.get("max")
        min_people = data.get("min")
        published = data.get("published")
        if max_people == "":
            max_people = None
        if min_people == "":
            min_people = None
        description_short = data.get("descriptionShort")
        description_long = data.get("descriptionLong")
        additional_information = data.get("additionalInformation")
        itinerary = data.get("itinerary")
        included = data.get("included")
        not_included = data.get("notIncluded")
        recommendations = data.get("recommendations")
        video = data.get("link")
        terms_conditions = data.get("terms")
        since = data.get('since')
        until = data.get('until')
        prices = data.get('prices')
        currency = data.get('currency')
        schedule = data.get('schedule')
        excluded_dates = data.get('excludedDates')
        
        service = Service.objects.get(id=data.get("serviceId"))
        if not request.user.is_superuser:
            company = Company.objects.get(users=request.user)
            service.company = company
        service.draft = draft
        service.title = title
        service.code = code
        service.duration = duration
        service.duration_unit = duration_unit
        if currency:
            service.currency = Currency.objects.get(pk=currency)
        service.max_people = max_people
        service.min_people = min_people
        service.description_short = description_short
        service.description_long = description_long
        service.additional_information = additional_information
        service.itinerary = itinerary
        service.included = included
        service.not_included = not_included
        service.recommendations = recommendations
        service.terms_conditions = terms_conditions
        service.video = video
        service.published = published
        if since:
            service.since = arrow.get(since).datetime
        else:
            service.since = None

        if until:
            service.until = arrow.get(until).datetime
        else:
            service.until = None
        service.category = service_category
        # service.company = company
        service.save()

        #Prices
        service.prices.filter(default=False).delete()
        for price in prices:
            if not price.get("default") and price.get("enabled"):
                price_instance = ServicePrice()
                category = PriceCategory.objects.get(pk=price.get("category"))
                price_instance.category = category
            elif price.get("default") and price.get("enabled"):
                price_instance = ServicePrice.objects.get(pk=price.get("id"))
            else:
                price_instance = ServicePrice.objects.get(pk=price.get("id"))
                price_instance.price = None
                price_instance.min_age = None
                price_instance.max_age = None
                price_instance.max_people = None
                price_instance.min_people = None
                price_instance.group_price_applied_to = None
                price_instance.enabled = False
                price_instance.save()
                continue

            price_service = price.get("price")
            if price_service == "":
                price_service = None
            price_instance.price = price_service

            max_people_group = price.get("max_people_group")
            if max_people_group == "":
                max_people_group = None

            min_people_group = price.get("min_people_group")
            if min_people_group == "":
                min_people_group = None

            price_instance.min_people = min_people_group
            price_instance.max_people = max_people_group
            if price.get('category_name') == "Niño":
                price_instance.min_age = price.get('min_age') if price.get('min_age') else None
                price_instance.max_age = price.get('max_age') if price.get('max_age') else None
            price_instance.group_price_applied_to = price.get("groupPriceAppliedTo")
            price_instance.service = service
            price_instance.enabled = price.get("enabled")
            price_instance.default = price.get("default")
            price_instance.save()

        service.schedule.all().delete()
        for day_schedule in schedule:
            if day_schedule.get("hour"):
                day = day_schedule.get("day")
                hour = arrow.get(day_schedule.get("hour")).time()
                service_schedule = ServiceSchedule()
                service_schedule.hour = hour
                service_schedule.day = day
                service_schedule.service = service
                service_schedule.save()

        service.excluded_dates.all().delete()
        for excluded_date in excluded_dates:
            excluded_date_list = excluded_date["date"]
            repeat_every_year = excluded_date["repeated"]
            if isinstance(excluded_date_list, list):
                if excluded_date_list[0] is not None: # cuando le das al 'x' del datetimepicker manda lista de None
                    service_excluded_date = ServiceExcludedDates()
                    since_date = excluded_date_list[0]
                    until_date = excluded_date_list[1]
                    service_excluded_date.since = arrow.get(since_date , 'YYYY-MM-DDTHH:mm:ss.SSSSZ').datetime
                    service_excluded_date.until = arrow.get(until_date, 'YYYY-MM-DDTHH:mm:ss.SSSSZ').datetime
                    service_excluded_date.repeat_every_year = repeat_every_year
                    service_excluded_date.service = service
                    service_excluded_date.save()



        return JsonResponse({'service_id': service.id})


class ImageServiceView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ImageServiceView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        service = Service.objects.get(id=request.POST.get("serviceId"))
        service_image = ServiceImage()
        service_image.image = request.FILES['file']
        service_image.service = service
        service_image.save()

        return JsonResponse({'service_image_id': service_image.id})


class DeleteImageServiceView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteImageServiceView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)

        # service = Service.objects.get(id=data.get("serviceId"))
        if data.get("serviceImageId"):
            ServiceImage.objects.get(pk=data.get("serviceImageId")).delete()

        return JsonResponse({'success': True})


class DeleteServiceView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteServiceView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)

        service = Service.objects.get(id=data.get("serviceId")).delete()

        return JsonResponse({'success': True})

class ServiceByNameView(View):
    def get(self, request, *args, **kwargs):
        query = request.GET.get("q")

        if not request.user.is_superuser:
            services = \
                Service.objects\
                    .filter(
                        title__icontains=query,
                        published=True,
                        company__id__in=request.user.get_company_ids()
                    )[:10]
        else:
            services = \
                Service.objects\
                    .filter(
                        title__icontains=query,
                        published=True
                    )[:10]

        def datetime_serializer(obj):
            """Default JSON serializer."""
            import calendar, datetime

            if isinstance(obj, datetime.datetime):
                if obj.utcoffset() is not None:
                    obj = obj - obj.utcoffset()
                millis = int(
                    calendar.timegm(obj.timetuple()) * 1000 +
                    obj.microsecond / 1000
                )
                return millis
            raise TypeError('Not sure how to serialize %s' % (obj,))

        results = []
        for service in services:
            service_item = {}
            service_item["deleted"] = False
            service_item["title"] = service.title
            service_item["id"] = service.id
            service_item["since"] = datetime_serializer(service.since)
            service_item["until"] = datetime_serializer(service.until)
            service_item["min_people"] = service.min_people
            service_item["max_people"] = service.max_people
            service_item["category"] = service.category.name
            service_item["currency"] = service.currency.symbol
            service_item["prices"] = \
                list(
                    service.prices\
                        .filter(enabled=True)\
                        .values(
                            "id", "price", "min_people", "max_people",
                            "group_price_applied_to", "category__name",
                            "category__id","last_price","category__fields_disabled"
                        )
                )

            service_item["excluded_dates"] = \
                list(
                    service.excluded_dates\
                        .values("since", "until")
                )

            list_schedule = service.schedule.all()

            def get_schedule(schedule):
                import datetime
                date = datetime.datetime.now()
                date = date.replace(
                    hour=schedule.hour.hour,
                    minute=schedule.hour.minute,
                    second=schedule.hour.second
                )
                return {
                    "day": schedule.day,
                    "hour": arrow.get(
                        date, tz.gettz('America/Lima')
                    ).shift(hours=-5).format('hh:mm A')
                }

            list_schedule = [get_schedule(schedule) for schedule in list_schedule ]

            service_item["schedule"] = list_schedule
            results.append(service_item)

        return JsonResponse(json.dumps(results, default=datetime_serializer), safe=False)
