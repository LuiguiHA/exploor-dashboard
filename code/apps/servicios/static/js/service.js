ELEMENT.locale(ELEMENT.lang.es)
// console.log()
Vue.use(VeeValidate,{fieldsBagName:'fieldsVee',errorBagName:'errorsVee', events:"input|change"})
// VeeValidate.Validator.setLocale("es")


Vue.use(window.VueQuillEditor)

// var dropzone = window["vue2-dropzone"]
var dropzone = vue2Dropzone
var app = new Vue({
  el: '#service',
  delimiters: ['${', '}'],
  components:{
    dropzone
  },
  data: {
    _: window._,
    errorImages: [],
    test: '',
    categories: window.categories,
    dontShowHelpOnGroupField: false,
    lastPriceCategoryOnFocus: null,
    lastPriceFieldOnFocus: null,
    lastOpenedTimeSelector: null,
    lastOpenedDateValiditySelector: null,
    lastOpenedDateExcludedSelector: null,
    blurQuillWysiwygTimeout: 0,
    quillWysiwygWasFocused: false,
    mouseX: 0,
    mouseY: 0,
    isOverHelp: false,
    Cropper: window.Cropper,
    moment: window.moment,
    uploadImageUrl: window.imageServiceUrl,
    deleteServiceUrl: window.deleteServiceUrl,
    changelistServiceUrl: window.changelistServiceUrl,
    isEditing: window.is_editing,
    selected: '',
    dropzoneOptions: {
      maxFilesize: 5,
      url: window.imageServiceUrl,
      maxNumberOfFiles: 200,
      addRemoveLinks: true,
      dictDefaultMessage: '<span class="fas fa-cloud-upload-alt"></span><br/>Arrastra tus imágenes aquí<br/>ó<br/>Haz click aquí',
      dictFileTooBig: 'La imágen no debe pesar más de 4 MB',
      dictRemoveFile: 'Eliminar',
      dictCancelUpload: 'Cancelar',
      init: function(){

      }
    },
    service: {
      serviceId: window.serviceId,
      current: window.service,
      currency: '',
      current_prices: window.service_prices,
      current_schedule: window.service_schedule,
      current_excluded_dates: window.service_excluded_dates,
      current_images: window.service_images,
      published: false,
      selectedCategory: null,
      title: null,
      code: null,
      prices: [],
      min: null,
      max: null,
      descriptionShort: null,
      descriptionLong: null,
      itinerary: null,
      included: null,
      notIncluded: null,
      recommendations: null,
      additionalInformation: null,
      link: null,
      since: '',
      until: '',
      duration: '',
      durationUnits: null,
      schedule: [],
      excludedDates: [],
      terms: null,
    },
    quillOptions: {
      modules: {
        toolbar: [
          ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
          [{ 'list': 'ordered' }, { 'list': 'bullet' }],
          [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent

          [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

          [{ 'align': [] }]
        ]
      },
      placeholder: 'Ingresa tu texto aquí...'
    },
    dialogVisible: false,
    dayRepeatId: [null, null, null, null, null, null, null],
    step: 1,
    currencies: window.currencies,
    helpInfo: '',
    errorPrice: '',
    edit: false,
    windowHeight: 0,
    deleteConfirmDialog: false,
    userWantsToFinalize: false,
    categoryDisabledMessage: 'Para habilitar este precio debe estar deshabilitado: ',
    savingMessage: '',
    isSaving: false,
    dirty: false,
    last_since: null,
    last_until: null,
    excludedDatesCalendarOptions: {
      shortcuts: []
    },
    today: window.today,
    savingService: false,
    deletingService: false,
    showHelp: true,
    groupsConflict: {},
    value1: ''
  },
  created () {
    document.getElementById('service').style.display = 'block'
    this.$validator.localize('es');
    document.body.addEventListener('mousemove', this.onMouseMove)
  },
  mounted () {
    // SENDING ADD PARAMETER DROPZONE
    this.$refs.dropzone.dropzone.on("sending", function(file, xhr, formData) {
      // Will send the filesize along with the file as POST data.
      formData.append("serviceId", this.service.serviceId);
    }.bind(this));
    this.$nextTick(function () {
      window.addEventListener('resize', this.getWindowHeight)
      this.getWindowHeight()
    })

    this.parseCurrentService()

    // this.addPricesValidation()
    this.quantityisValid()
    this.validateAgeValid()
    this.quantityGroupisValid()
    this.addMinGroupValidation()
    window.addEventListener('beforeunload', this.beforeUnload)

    moment.locale('es')
    
  },
  computed: {
    dynamicHeight: function () {
      return String(this.windowHeight - 160) + 'px'
    },
    dynamicHeightHelp: function () {
      return String(this.windowHeight - 160) + 'px'
    },
    nextButtonDisabled: function () {
      if (this.step === 1) {
        return this.service.selectedCategory == null
      }
      return false
    },
    _() {
      return _
    },
    calculateWithContainer: function () {
      if (this.step == 1){
        return 24
      }
      else {
        if (this.showHelp){
          return 18
        }
        else{
          return 24
        }
      }
    },
    calculateOffset: function () {
      return this.showHelp ? 1 : 0
    }
  },
  watch: {
    service: {
      handler: function () {
        if (window.dismissSaving) return
        this.dirty = true
        clearTimeout(window.timeOutSave)

        window.timeOutSave = setTimeout(function () {
          if (this.service.draft) {
            this.saveService()
          }
        }.bind(this), 500)
      },
      deep: true
    },
    step: function (newValue) {
      if (newValue > 1) {
        this.setHelp('step' + newValue)
      }
    },
    'service.since': function (newValue) {
      if (newValue == null) {
        this.service.since = ''
      }
    },
    'service.until': function (newValue) {
      if (newValue == null) {
        this.service.until = ''
      }
    }
  },
  methods: {
    visibleChangeGroup: function (help, price, index) {
      this.setHelp(help)
      this.lastPriceFieldOnFocus = null
      this.$nextTick(function () {
        if (price.min_people_group !== null && price.min_people_group >= 0) {
          this.$validator.validate('min_' + index, price.min_people_group)
        }
        if (price.max_people_group !== null && price.max_people_group >= 0) {
          this.$validator.validate('max_' + index, price.max_people_group)
        }
      }.bind(this))
    },
    mouseOverPriceRow: function (help, category) {
      this.helpInfo = help
      if (this.lastPriceFieldOnFocus !== null) {
        setTimeout(function () {
          if (this.lastPriceCategoryOnFocus === category) {
            this.dontShowHelpOnGroupField = true
            $(this.lastPriceFieldOnFocus.$el).find('input').focus()
          }
        }.bind(this), 50)
      }
    },
    focusGroupField: function (help, fieldRef, category) {
      if (this.dontShowHelpOnGroupField === false) {
        this.setHelp(help)
      } else {
        this.dontShowHelpOnGroupField = false
      }

      this.lastPriceFieldOnFocus = this.$refs[fieldRef][0]
      this.lastPriceCategoryOnFocus = category
    },
    changeGroupField: function (price, index) {
      this.validateGroupsDifferent(price)

      this.$nextTick(function () {
        if (price.min_people_group !== null && price.min_people_group >= 0) {
          this.$validator.validate('min_' + index, price.min_people_group)
        }
        if (price.max_people_group !== null && price.max_people_group >= 0) {
          this.$validator.validate('max_' + index, price.max_people_group)
        }
      }.bind(this))

    },
    uuidv1: function () {
      return window.uuidv1()
    },
    onOpenTimeSelector: function (refString) {
      this.lastOpenedTimeSelector = this.$refs[refString][0]
      // console.log(refString, this.$refs[refString][0])
    },
    onOpenDateValidtySelector: function (refString) {
      this.saveDate(refString)
      this.lastOpenedDateValiditySelector = this.$refs[refString]
    },
    onOpenDateExcludedSelector: function (refString) {
      this.setHelp('excluded_date')
      this.lastOpenedDateExcludedSelector = this.$refs[refString][0]
    },
    onScroll: function () {
      this.detectMouseOverElements(this.mouseX, this.mouseY)
      this.checkForOpenedDateTimePickerPanel(
        '.el-time-panel:visible', this.lastOpenedTimeSelector)
      this.checkForOpenedDateTimePickerPanel(
        '.el-picker-panel:visible', this.lastOpenedDateValiditySelector)
      this.checkForOpenedDateTimePickerPanel(
        '.el-picker-panel:visible', this.lastOpenedDateExcludedSelector)
    },
    checkForOpenedDateTimePickerPanel: function (selector, component) {
      _.forEach($(selector), function (timePanel) {
        var offset = $(timePanel).offset()
        var height = $(timePanel).outerHeight()
        var windowHeight = $(window).height()
        var diffToEnd = Math.abs((offset.top + height) - windowHeight)
        if (offset.top < 115 || diffToEnd < 60) {
          if (component) {
            component.hidePicker()
            component.refInput[0].blur()
          }
        }
      }.bind(this))
    },
    focusQuillWysiwyg: function (help) {
      this.setHelp(help)
      this.quillWysiwygWasFocused = true
    },
    onMouseMove: function (event) {
      this.mouseX = event.clientX
      this.mouseY = event.clientY
      this.detectMouseOverElements(this.mouseX, this.mouseY)
    },
    detectMouseOverElements: function (mouseX, mouseY) {
      var stepContainer = $('.step-' + this.step)
      var stepContainerOffset = stepContainer.offset()
      var stepChildren = $('.step-' + this.step).find('.hasMouseOver')
      var childOver = _.find(stepChildren, function (child) {
        var childOffset = $(child).offset()
        var childWidth = $(child).outerWidth()
        var childHeight = $(child).outerHeight()
        var childX = childOffset.left
        var childY = childOffset.top
        var isInsideLeft = mouseX > childX
        var isInsideRight = mouseX < (childX + childWidth)
        var isInsideTop = mouseY > childY
        var isInsideBottom = mouseY < (childY + childHeight)
        // console.log('isInsideLeft', isInsideLeft)
        // console.log('isInsideRight', isInsideRight)
        // console.log('isInsideTop', isInsideTop)
        // console.log('isInsideBottom', isInsideBottom)

        return isInsideLeft && isInsideRight && isInsideTop && isInsideBottom
      });

      if (childOver !== undefined) {
        var helpOver = $(childOver).data('help-over')
        this.setHelp(helpOver)
        this.isOverHelp = true
      }
    },
    dropzoneError: function(file, message, xhr){
      this.errorImages.push([file, message])
    },
    dropzoneQueueComplete: function (file, xhr, formData){
      if(this.errorImages.length === 0) return
      var fileNameAndMessage = _.join(
        _.map(this.errorImages,function (x) {
            return '- ' + x[0].name + ' => ' + x[1]
        }),'<br>'
      )
      var finalMessage = 'Los siguientes archivos no se subieron:<br><br>' + fileNameAndMessage

      this.$message({
        dangerouslyUseHTMLString: true,
        message: finalMessage,
        type: 'error',
        showClose: true,
        duration: 6000
      })

      _.forEach(this.errorImages, function (file) {
        this.$refs.dropzone.removeFile(file[0])
      }.bind(this))

      this.errorImages = []
    },
    scheduleRepeat: function(dayNoIncluded){
      var service_schedule = _.filter(this.service.schedule,function(schedule){
        return schedule.day != dayNoIncluded
      })
      var days = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"]
      var scheduleRepeat2 = _.map(service_schedule,function(schedule){
          return {"name": days[schedule.day - 1],"id": schedule.day}
      })
      scheduleRepeat2 = _.uniqWith(scheduleRepeat2, _.isEqual)
      return _.orderBy(scheduleRepeat2,["id"],["asc"])
    },
    setHoursRepeat: function(index){
      var dayId = this.dayRepeatId[index - 1]
      var scheduleOfDayId = _.filter(this.service.schedule,function(schedule){ return schedule.day == dayId})
      var scheduleOfDayIndex = _.filter(this.service.schedule,function(schedule){ return schedule.day == index})
      var hoursOfIndex = []
      if (scheduleOfDayIndex.length > 0){
        hoursOfIndex = _.map(scheduleOfDayIndex,function(schedule){
          return moment(schedule.hour).format("HH-mm")
        })
      }
      _.forEach(scheduleOfDayId,function(schedule){
        hour = moment(schedule.hour).format("HH-mm")

        var hourExist = _.find(hoursOfIndex,function(hourIndex){ return hourIndex == hour})

        if (hourExist == undefined){
          this.service.schedule.push({"day": index, "hour": schedule.hour})
        }
      }.bind(this))

      this.dayRepeatId[index - 1] = null
    },
    getServiceDateOptions: function(isSince,isExcluded){
      return {
        disabledDate: function(date){
          var scheduleOfDay = _.find(this.service.schedule, function(schedule){
            return schedule.day == moment(date).isoWeekday()
          })
            if (scheduleOfDay == undefined){ // true cuando deshabilita la fecha
              return true
            }
            else{
              var today = moment(this.today)
              var isBeforeToday = moment(date).isBefore(today)
              if (isBeforeToday){
                return true
              }
              if (!(this.service.since instanceof Date) && !(this.service.until instanceof Date)){
                return false // no se deshabilita ninguna fecha
              }
              else if (!(this.service.since instanceof Date) && (this.service.until instanceof Date)){
                var isBeforeUntil = moment(date).isSameOrBefore(moment(this.service.until))
                return !isBeforeUntil
              }
              else if ((this.service.since instanceof Date) && !(this.service.until instanceof Date)){
                if (!isSince){
                  var isAfterSince = moment(date).isSameOrAfter(moment(this.service.since))
                  return !isAfterSince
                }
                else{ return false}

              }
            else {
              var isAfterSince = moment(date).isSameOrAfter(moment(this.service.since))
              var isBeforeUntil = moment(date).isSameOrBefore(moment(this.service.until))
              if (isSince){
                return !(isBeforeUntil)
              }
              else if (isExcluded){
                return !(isAfterSince && isBeforeUntil)
              }
              else{
                return !(isAfterSince)
              }
            }
          }
        }.bind(this)
      }
    },
    saveDate: function(isSince){
      if (isSince == 'since'){
        this.setHelp('since')
        if (this.service.since){
          this.last_since = this.service.since
        }
      }
      else{
        this.setHelp('until')
        if (this.service.until){
          this.last_until = this.service.until
        }
      }

    },
    validateDateInExcludeDates: function(isSince){
      
      if (this.service.excludedDates){
        var newDate = ""
        if (isSince){
          newDate = this.service.since
        }
        else{
          newDate = this.service.until
        }
        var isExcluded = _.find(this.service.excludedDates,function(ed){
          var ed_since = moment(ed.date[0])
          var ed_until = moment(ed.date[1])
          var isAfterSince = moment(newDate).isAfter(ed_since)
          var isBeforeUntil = moment(newDate).isBefore(ed_until)
          return isAfterSince & isBeforeUntil
        })
        if (isExcluded){
          this.$message({
            message: 'La fecha ' + moment(newDate).format("YYYY-MM-DD") + ' esta incluida en las fechas excluidas.',
            type: 'error',
            showClose: true,
          });
          if (isSince){
            this.service.since = this.last_since
          }
          else{
            this.service.until = this.last_until
          }
        }
      }
    },
    validateExcludeDatesEquals: function(){
      if (this.service.excludedDates){
        var excluded_dates_list = []
        _.forEach(this.service.excludedDates,function(ed){
          if (ed.date){
            date = moment(ed.date[0]).format("YYYY-MM-DD") + '/' + moment(ed.date[1]).format("YYYY-MM-DD")
            excluded_dates_list.push(date)
          }
        })
        excluded_dates_uniq_list = _.uniq(excluded_dates_list)
        if (excluded_dates_uniq_list.length != excluded_dates_list.length){
          return true
        }
        else {return false}
      }
      else{
          return false
      }
    },
    quickOptionSelected: function(picker, monthName){
      var start = moment().month(_.toLower(monthName)).startOf("month")
      var end = moment().month(_.toLower(monthName)).endOf("month")
      picker.$emit('pick', [start, end])
    },
    beforeUnload: function(event){
      if(this.isSaving || this.dirty){
        var confirmationMessage = "Si abandonas esta página, se perderán todos los cambios no guardados. ¿Estás seguro de que quieres salir de esta página?";
        event.returnValue = confirmationMessage;
        return confirmationMessage
      } else {
        return false
      }

    },
    addMinGroupValidation: function () {
      var _this = this
      VeeValidate.Validator.extend('min_group', {
        getMessage: function(field, params, data){
            return data.message
        },
        validate: function(value, args) {
          var valueOfField = parseFloat(_.isNaN(parseFloat(value)) ? value.target.value : value)
          var price = args[0]
          if (_this.service.min && _.isNumber(_this.service.min) && valueOfField < _this.service.min) {
            return {
              valid: false,
              data: { message: 'La cantidad mínima debe ser mayor a ' +  _this.service.min }
            }
          }

          if (_this.service.max && _.isNumber(_this.service.max) && valueOfField >= _this.service.max) {
            return {
              valid: false,
              data: { message: 'La cantidad mínima debe ser menor a ' +  _this.service.max }
            }
          }

          if (price.max_people_group > 0) {
            var maxValue = price.max_people_group - 1
            if (valueOfField > maxValue) {
              return {
                valid: false,
                data: { message: 'La cantidad mínima debe ser menor a ' +  price.max_people_group }
              }
            }
          }
          return true
        }
      })
    },
    quantityisValid: function(){
      var _this = this
      VeeValidate.Validator.extend('quantity', {
        getMessage: function(field, args){
            return "La cantidad máxima debe ser 0 o superior a la cantidad mínima."
        },
        validate: function(value) {
          if (_this.service.max == 0){
            return true
          }
          if (_this.service.max < _this.service.min){
            return false
          }
          return true
        }
      })
    },
    quantityGroupisValid: function(){
      var _this = this
      var message = ''
      VeeValidate.Validator.extend('quantityGroupMinValue', {
        getMessage: function(field, args){
            return message
        },
        validate: function(value, args) {
          if (parseInt(args[1]) <= parseInt(args[0])){
            message = "La cantidad máxima debe ser "+ (parseInt(args[0]) + 1) + " o superior."
            return false
          }
          if (_this.service.min != null && _this.service.min != "" && parseInt(args[1]) <= (_this.service.min + 1)){
            message = "La cantidad máxima debe ser " + (_this.service.min + 1) + " o superior."
            return false
          }  
          return true
        }
      })
      VeeValidate.Validator.extend('quantityGroupMaxValue', {
        getMessage: function(field, args){
            return "La cantidad máxima debe ser menor a "+ _this.service.max
        },
        validate: function(value, args) {
          if (_this.service.max === 0 || _this.service.max === null || _this.service.max === undefined) {
            return true
          }
          if (_this.service.max && _.isNumber(_this.service.max) && parseInt(args[0]) > _this.service.max) {
            return false
          }
          return true
        }
      })
    },
    validateAgeValid: function(){
      VeeValidate.Validator.extend('minAge', {
        getMessage: function(field, args){
            return message
        },
        validate: function(value, args) {
          var maxAge = eval(args[0])
          if (value == 0){
            message = "La edad mínima debe ser mayor a 0."
            return false
          }
          if (maxAge != null && maxAge != '' && maxAge != undefined){
            if (value >= maxAge){
              message = "La edad mínima debe ser menor a " + maxAge
              return false
            } 
          }
          return true
        }
      })
      VeeValidate.Validator.extend('maxAge', {
        getMessage: function(field, args){
            return message
        },
        validate: function(value, args) {
          var minAge = eval(args[0])
          if (value == 0){
            message = "La edad máxima debe ser mayor a 0."
            return false
          }
          if (minAge != null && minAge != '' && minAge != undefined){
            if (value <= minAge){
              message = "La edad máxima debe ser mayor a " + minAge
              return false
            } 
          }
          return true
        }
      })
    },
    parseCurrentService: function(){
      if (this.service.current){
        window.dismissSaving = true
        this.service.draft = this.service.current.draft;
        this.service.title = this.service.current.title;
        this.service.code = this.service.current.code;
        this.service.duration = this.service.current.duration;
        this.service.min = this.service.current.min_people;
        this.service.max = this.service.current.max_people;
        this.service.descriptionShort = this.service.current.description_short;
        this.service.descriptionLong = this.service.current.description_long;
        this.service.selectedCategory = this.service.current.category_id;
        this.service.itinerary = this.service.current.itinerary;
        this.service.included = this.service.current.included;
        this.service.notIncluded = this.service.current.not_included;
        this.service.recommendations = this.service.current.recommendations;
        this.service.additionalInformation = this.service.current.additional_information;
        this.service.currency = this.service.current.currency_id
        this.service.link = this.service.current.video
        this.service.durationUnits = this.service.current.duration_unit
        if (this.service.current.until){
          this.service.until = moment(this.service.current.until).toDate();
        }
        if (this.service.current.since){
          this.service.since = moment(this.service.current.since).toDate();
        }

      // this.$set(this.service, 'until', this.service.current.until)
      // this.$set(this.service, 'since', this.service.current.since)

        this.service.terms = this.service.current.terms_conditions;
        this.service.published = this.service.current.published;
        if (this.service.current_prices) {
          var prices = []
          for (var i = this.service.current_prices.length - 1; i >= 0; i--) {
            var price = this.service.current_prices[i]
            var obj = {
              id:price.id,
              category: price.category_id,
              category_name: price._category_cache.name,
              description: price._category_cache.description,
              price: price.price,
              last_price: price.last_price,
              min_people_group:price.min_people,
              max_people_group:price.max_people,
              groupPriceAppliedTo:price.group_price_applied_to,
              enabled: price.enabled,
              default: price.default,
              min_age: price.min_age,
              max_age: price.max_age,
              fields_disabled: price._category_cache.fields_disabled
            }
            prices.push(obj)
          }
          this.service.prices = prices
        }
        window._.forEach(this.service.prices, function(p){
          if(p.enabled)
            this.validateServicePrice(p.category,p.enabled)
        }.bind(this))

        if (this.service.current_schedule) {
          var schedules = []
          for (var i = this.service.current_schedule.length - 1; i >= 0; i--) {
            var schedule = this.service.current_schedule[i]
            var obj = {
              day: schedule.day,
              hour: moment(schedule.hour,'YYYY-MM-DD H:mm:ss ZZ').toDate(),
            }
            schedules.push(obj)
          }
          this.service.schedule = schedules
        }
        if (this.service.current_excluded_dates) {
          this.service.excludedDates = this.service.current_excluded_dates
        }

        this.$nextTick(function() {
          this.errorsVee.clear()
          window.dismissSaving = false
          if (this.service.current.draft == true){
            this.step = 1
          }
          else{
            this.step = 2
          }
        }.bind(this))
      }

      if (this.service.draft) {
        this.$nextTick(function () {
          this.errorsVee.clear()
        }.bind(this))
      }
    },
    validateGroups: function(){
      var pricesClone = _.cloneDeep(this.service.prices)
      var pricesGroup = _.filter(pricesClone, function(p){
        return p.category_name == "Grupo"
      })
      var peopleGroup = _.map(pricesGroup,function(pg){

        return {'min':pg.min_people_group,'max':pg.max_people_group}
      })
      var uniqPeopleGroup = _.uniqWith(peopleGroup, _.isEqual);
      if (peopleGroup.length == uniqPeopleGroup.length){
        return true // sin errores
      } else {
        return false // grupos con cantidad min y max iguales
      }
    },
    validateGroupsEmpty: function(){
      pricesClone = _.cloneDeep(this.service.prices)
      var pricesGroup = _.filter(this.service.prices,function(p){
        return p.category_name == "Grupo" && p.enabled
      })
      if(pricesGroup.length == 0){
        return false
      }
      var peopleGroup = _.filter(pricesGroup,function(pg){
        return pg.min_people_group == null || pg.max_people_group == null
      })
      return peopleGroup.length > 0
    },
    validateHours: function(){
      scheduleClone = _.cloneDeep(this.service.schedule)
      scheduleGroup = _.groupBy(scheduleClone,function(s){
        return s.day
      })
      scheduleRepeat = _.mapValues(scheduleGroup,function(scheduleArray){
        var s = _.map(scheduleArray,function(sa){
          sa.hour = moment(sa.hour).format("HH-mm")
          return sa
        })
        var len = s.length
        var hours = _.uniqWith(s, _.isEqual);
        return len == hours.length
      })
      return scheduleRepeat
    },
    gotoStep: function(index){
      if (this.service.selectedCategory == null){
        return false
      }
      else{
        document.getElementById("scroll").scrollTop = 0
        this.step = index
        return true
      }

    },
    confirmDelete: function(){
      this.deletingService = true
      this.deleteConfirmDialog = true
      axios.post(this.deleteServiceUrl, {
        serviceId: this.service.serviceId
      })
      .then((response) =>{
        this.deletingService = false
        window.location = this.changelistServiceUrl
      })
    },
    deleteService: function(){
      this.deleteConfirmDialog = true
    },
    getImagePortion: function(imgObj, newWidth, newHeight, startX, startY, ratio){
     /* the parameters: - the image element - the new width - the new height - the x point we start taking pixels - the y point we start taking pixels - the ratio */
     //set up canvas for thumbnail
     var tnCanvas = document.createElement('canvas');
     var tnCanvasContext = tnCanvas.getContext('2d');
     tnCanvas.width = newWidth; tnCanvas.height = newHeight;

     /* use the sourceCanvas to duplicate the entire image. This step was crucial for iOS4 and under devices. Follow the link at the end of this post to see what happens when you don’t do this */
     var bufferCanvas = document.createElement('canvas');
     var bufferContext = bufferCanvas.getContext('2d');
     bufferCanvas.width = imgObj.width;
     bufferCanvas.height = imgObj.height;
     bufferContext.drawImage(imgObj, 0, 0);

     /* now we use the drawImage method to take the pixels from our bufferCanvas and draw them into our thumbnail canvas */
     console.log("DIMENSIONES")
     console.log(newWidth * ratio)
     console.log(newHeight * ratio)
     tnCanvasContext.drawImage(bufferCanvas, 0,0,newWidth, newHeight,0,0,newWidth,newHeight);
    //  tnCanvasContext.drawImage(bufferCanvas, startX,startY,newWidth * ratio, newHeight * ratio,0,0,newWidth,newHeight);
     return tnCanvas.toDataURL();
    },
    loadImages: function(){
      //TODO load all images
      if(this.service.current_images){
        for (var i = this.service.current_images.length - 1; i >= 0; i--) {
          var image = this.service.current_images[i]

            var imageObj = new Image();
            imageObj.src = image[0];
            var _this = this
            imageObj.onload = (function(id){
              return function(e){
                //TODO fix dimensions
                // if(imageObj.width > imageObj.height){
                //   imageObj.height = 200
                //   var x = (imageObj.width - 200) / 2
                //   var y = 0
                // } else {
                //   imageObj.width = 200
                //   var x = 0
                //   var y = (imageObj.height - 200) / 2
                // }
                var x = (this.width) / 2
                var y = (this.height) / 2
                var imageData = _this.getImagePortion(e.srcElement, this.width, this.height, x, y, 1)

                var mockFile = { name: "", size: 12345, serviceImageId: id };
                _this.$refs.dropzone.manuallyAddFile(
                  mockFile,
                  imageData,
                  null,
                  null,
                  {
                    dontSubstractMaxFiles: false,
                    addToFiles: true
                  }
                );
              }
            }.bind(this))(image[1])

        }
      }
    },
    getWindowHeight: function(event) {
      this.windowHeight = document.documentElement.clientHeight;
    },
    dropzoneMounted: function(){
      setTimeout(function() {
        this.loadImages()

      }.bind(this), 1000);
    },
    fileRemoved: function(file, error, xhr){
      if(this.step != 2 ) return
      if (file.status != 'error'){
        axios.post(window.deleteImageServiceUrl, {
          serviceImageId: file.serviceImageId,
          serviceId: this.service.serviceId
        })
        .then((response) =>{
          this.$message({
            message: 'Imagen eliminada exitosamente.',
            type: 'success',
            showClose: true,
          });
        })
      } 
    },
    uploadSuccess: function (file, response) {
      file.serviceImageId = response.service_image_id
      // this.$message({
      //   message: 'Imagen subida exitosamente.',
      //   type: 'success'
      // });
    },
    getPriceTooltipInfo: function(price){
      if(price.fields_disabled){
        var prices_disabled = price.fields_disabled.split(",")
        var categoriesName = _.map(prices_disabled,function(priceDisabledId){
          var priceFound = _.find(this.service.prices,function(p){
            return p.category == parseInt(priceDisabledId)
          })
          return priceFound.category_name


        }.bind(this))

        return this.categoryDisabledMessage + " " + _.join(categoriesName, ", ") + "."

      } else {
        return "Habilita este precio si tienes precios para grupos"
      }
    },
    addPrice: function(){
      var priceGroup = _.find(this.service.prices,function(p){
        return p.category_name == "Grupo" && p.default == true
      })
      this.service.prices.push({
        id:(this.service.prices).length + 1,
        category: priceGroup.category,
        price: null,
        min_people_group: null,
        max_people_group: null,
        groupPriceAppliedTo:1,
        default: false,
        category_name: priceGroup.category_name,
        description : priceGroup.description,
        enabled: true,
        fields_disabled: priceGroup.fields_disabled
      })
    },
    removePrice: function(index, state, idGroup){
      if ((state && this.groupsConflict['id'] == undefined) || (state && this.groupsConflict['id'] != undefined && this.groupsConflict['id'] == idGroup)){
        this.service.prices.splice(index, 1)
        this.$nextTick(function(){
          this.errorsVee.remove('min_'+2+'-'+index)
          this.errorsVee.remove('max_'+2+'-'+index)
          this.errorsVee.remove('price_'+2+'-'+index)
        }.bind(this))
        if (this.groupsConflict['id'] != undefined){
          this.groupsConflict = {}
        }
      }
    },
    deleteExcludedDate: function(index){
      this.service.excludedDates.splice(index, 1)

    },
    addExcludedDate:function(){
      this.service.excludedDates.push({date:"", repeated:false})
    },
    addHour: function(day){
      this.service.schedule.push({"day": day, "hour": new Date()})
    },
    removeHour: function(index, day){
      if (this.service.schedule.length == 1){
        if (this.service.since || this.service.until && this.service.excludedDates.length > 0){
          this.$confirm('¿Estás seguro que deseas eliminar este horario? Al eliminar todos los horarios, la fecha de vigencia y las fechas excluidas también se eliminarán.')
          .then(function(){
              this.service.schedule.splice(index, 1)
              this.service.since = ""
              this.service.until = ""
              this.$nextTick(function(){
                this.errorsVee.remove("since")
                this.errorsVee.remove("until")
              }.bind(this))
              this.service.excludedDates = []
            }.bind(this))
          .catch(function(){})
        }
        if ((this.service.since || this.service.until) && this.service.excludedDates.length == 0){
          this.$confirm('¿Estás seguro que deseas eliminar este horario? Al eliminar todos los horarios, la fecha de vigencia se eliminará.')
          .then(function(){
              this.service.schedule.splice(index, 1)
              this.service.since = ""
              this.service.until = ""
              this.$nextTick(function(){
                this.errorsVee.remove("since")
                this.errorsVee.remove("until")
              }.bind(this))
              this.service.excludedDates = []
            }.bind(this))
          .catch(function(){})
        }
        if (this.service.since == "" && this.service.until == "" && this.service.excludedDates.length > 0){
          this.$confirm('¿Estás seguro que deseas eliminar este horario? Al eliminar todos los horarios, las fechas excluidas también se eliminarán.')
          .then(function(){
              this.service.schedule.splice(index, 1)
              this.service.excludedDates = []
            }.bind(this))
          .catch(function(){})
        }
        if (this.service.since == "" && this.service.until == "" && this.service.excludedDates.length == 0){
          this.service.schedule.splice(index, 1)
        }
      }
      else{
        this.service.schedule.splice(index, 1)
      }
    },
    selectCategory:function(categoryId){
      this.service.selectedCategory = categoryId
    },
    nextStep: function(){
      if (this.step == 1){
       document.getElementById("scroll").scrollTop = 0;
       this.step += 1
     }
     else if (this.step == 2){
       document.getElementById("scroll").scrollTop = 0;
       this.step += 1
     }
     else if (this.step == 3){
       document.getElementById("scroll").scrollTop = 0;
       this.step = 1
     }
    },
    prevStep: function(){
      if(this.step > 1){
        document.getElementById("scroll").scrollTop = 0;
        this.step -= 1
      }
    },
    // showDescriptionHelp: function(description, index){
    //   this.helpInfo = description
    //   if (index != undefined){
    //
    //     _.forEach(this.$refs['price_'+index][0].$el.children,function(element){
    //       element.blur()
    //     })
    //     if (this.$refs['min_'+index]){
    //       _.forEach(this.$refs['min_'+index][0].$el.children,function(element){
    //         element.blur()
    //       })
    //       _.forEach(this.$refs['max_'+index][0].$el.children,function(element){
    //         element.blur()
    //       })
    //     }
    //   }
    //
    // },
    setHelp: function (value) {
      var textField = _.find(window.fieldsHelpList, function(o) { return o.field_name == value });
      this.helpInfo = textField.help_text

      if(value == "code"){
        this.helpInfo = this.helpInfo.replace("#code", this.service.code)
      }
      // if(isFocus){
      //   this.helpInfo = textField.help_text
      //
      //   if(value == "code"){
      //     this.helpInfo = this.helpInfo.replace("#code", this.service.code)
      //   }
      // } else {
      //   if (!isFocus){
      //       this.helpInfo = textField.help_text
      //   }
      // }
    },
    priceDisabled: function(state){
      var found = _.find(this.service.prices,function(p){
        return p.enabled
      })
      if (found){
        return state
      }
      else{
        return false
      }
    },
    validateServicePrice: function(categoryId, enabled){

      this.validateErrorPrices()
      var price = _.find(this.service.prices,function(p){
        return p.category == categoryId && p.default == true
      })

      _.forEach(this.service.prices, function (p) {
        p.disabled = false
        if (price.category_name === 'Grupo') {
          if (p.default === false) {
            p.disabled = price.disabled
            p.enabled = price.enabled
          }
        }
      })

      var prices_disabled = _.flatMap(this.service.prices, function (price) {
        if (price.enabled) {
          if (price.fields_disabled) {
            return price.fields_disabled.split(',')
          }
        }
        return []
      })

      prices_disabled = _.uniq(prices_disabled)

      _.forEach(this.service.prices, function (p) {
        var priceFound = _.find(prices_disabled, function (pd) {
          return p.category === parseInt(pd)
        })
        if (priceFound) {
          p.enabled = false
          p.disabled = true
        }
      })
      this.$nextTick(function () {
        // PARA LOS PRECIOS TIPO GRUPO
        var groupPrices = _.filter(this.service.prices,function(p){
          return p.category_name == 'Grupo'
        })
        _.forEach(groupPrices, function (g, index) {
          var realIndex = index === 0 ? 2 : index + 4
          if (enabled){
            if (g.min_people_group !== "" && g.min_people_group !== null && g.min_people_group >= 0) {
              this.$validator.validate('min_' + realIndex , g.min_people_group)
            }
            if (g.max_people_group !== "" && g.max_people_group !== null && g.max_people_group >= 0) {
              this.$validator.validate('max_' + realIndex, g.max_people_group)
            }
            if (g.price !== "" && g.price !== null && g.price >= 0) {
              if (realIndex == 2) {
                this.$validator.validate('price_' + realIndex, g.price)
              } else {
                this.$validator.validate('price_2-' + realIndex, g.price)
              }
            }
          } else {
              this.errorsVee.remove('min_' + realIndex)
              this.errorsVee.remove('max_' + realIndex)
              if (realIndex == 2){
                this.errorsVee.remove('price_' + realIndex)
              } else {
                this.errorsVee.remove('price_2-' + realIndex)
              }
              if (g.groupPriceAppliedTo == null){
                this.errorsVee.remove('groupPriceAppliedTo_2')
              }
          }

        }.bind(this))
        // PARA LOS PRECIOS QUE NO SON TIPO GRUPO
        var prices = _.filter(this.service.prices, function(p){
          return p.category_name != 'Grupo'
        })
        _.forEach(prices, function(p, index){
          var realIndex = index >= 2 ? index + 1 : index 
          if(enabled){
            if (p.price !== null && p.price !== "" && p.price >= 0) {
              this.$validator.validate('price_' + realIndex)
            }
            if (p.category_name == "Niño"){
              if (p.min_age !== null && p.min_age !== "" && p.min_age >= 0){
                this.$validator.validate('age_min_'+ realIndex)
              }
              if (p.max_age !== null && p.max_age !== "" && p.max_age >= 0){
                this.$validator.validate('age_max_'+ realIndex)
              }
            }
          }else{
            this.errorsVee.remove('price_' + realIndex)
            if (p.category_name == "Niño"){
              this.errorsVee.remove('age_min_'+ realIndex)
              this.errorsVee.remove('age_max_'+ realIndex)
            }
          }
        }.bind(this))
      })
    },
    validateErrorPrices: function(showMessage){
      var error = _.find(this.service.prices,function(p){ return p.enabled})
      if (error){
        this.errorPrice = ""
        return false
      }
      else{
        if (showMessage){
          this.$message({
            message: 'Debe habilitar al menos un tipo de precio.',
            type: 'error',
            showClose: true,
          });
        }
        return true
      }
    },
    validateSinceAndUntilDateInSchedule: function(){

      var daysEnabled = _.map(this.service.schedule,function(s){
        return s.day
      })
      var daysEnabled = _.uniq(daysEnabled)
      var daySince = moment(this.service.since).isoWeekday()
      var foundSince = _.find(daysEnabled,function(de){
        return de == daySince
      })
      var dayUntil = moment(this.service.until).isoWeekday()
      var foundUntil = _.find(daysEnabled,function(de){
        return de == dayUntil
      })
      if (foundSince == undefined && foundUntil != undefined){
        this.$message({
          message: 'La fecha del campo "Desde" pertenece a un día de la semana que no tiene horario.',
          type: 'error',
          showClose: true,
        });
      }
      if (foundSince != undefined && foundUntil == undefined){
        this.$message({
          message: 'La fecha del campo "Hasta" pertenece a un día de la semana que no tiene horario.',
          type: 'error',
          showClose: true,
        });
      }
      if (foundSince == undefined && foundUntil == undefined){
        this.$message({
          message: 'Las fechas de los campos "Desde" y "Hasta" pertenecen a un día de la semana que no tiene horario.',
          type: 'error',
          showClose: true,
        });
      }

      return foundSince != undefined && foundUntil != undefined
    },
    validateExcludeDatesInSchedule: function(){
      var daysEnabled = _.map(this.service.schedule,function(s){
        return s.day
      })
      daysEnabled = _.uniq(daysEnabled)

      if (this.service.excludedDates){
         var datesInSchedule = _.map(this.service.excludedDates,function(ed){
            if (ed.date instanceof Array){
              if (ed.date[0] != null){
                var ed_since = moment(ed.date[0]).isoWeekday()
                var ed_until = moment(ed.date[1]).isoWeekday()
                return {'since': ed_since, 'until': ed_until, 'index': this.service.excludedDates.indexOf(ed) + 1}
                  }
            }
          }.bind(this))
         datesInSchedule = _.filter(datesInSchedule,function(ds){ return ds != undefined })

         var founDate = _.reduce(datesInSchedule,function(acc,date){
            var foundSince = _.find(daysEnabled,function(de){
              return de == date.since
            })
            var foundUntil = _.find(daysEnabled,function(de){
              return de == date.until
            })
            if (foundSince == undefined || foundUntil == undefined){
              acc.push(date)
            }
            return acc
          },[])

         if (founDate.length > 0){
          var datesIndex = _.map(founDate,function(fd){
            return fd.index
          })
          datesIndex = _.join(_.uniq(datesIndex), ", ")

          this.$message({
            message: 'Las fechas excluidas de las filas '+ datesIndex +' pertenecen a días de la semana que no tienen horario.',
            type: 'error',
            showClose: true,
          });
          return false
         }
         else{
          return true
         }
      }
      else{
        return true
      }
    },
    validateGroupsDifferent: function(price){
      var minValue = price !== undefined ? price.min_people_group : null
      var maxValue = price !== undefined ? price.max_people_group : null
      var idGroup = price !== undefined ? price.id : undefined
      // if ((minValue == "" || maxValue == "") && idGroup != undefined){
      //   return true
      // }
      if (minValue == null || maxValue == null){

        if (this.groupsConflict['id'] == undefined){
          return true
        }
        this.$message({
          message: 'Existe un grupo que esta haciendo conflicto.',
          type: 'error',
          showClose: true,
        });
        return false
      } else{
        var groupPrices = _.filter(this.service.prices,function(price){ return price.category_name == "Grupo"})
        var group1 = _.find(groupPrices,function(price1){

            var group2 = _.find(groupPrices,function(price2){
              equalGroup = _.isEqual(price1,price2)
              if (equalGroup == false){
                var min = price1.min_people_group < price2.min_people_group ||
                          price1.min_people_group > price2.max_people_group
                var max = price1.max_people_group < price2.min_people_group ||
                          price1.max_people_group > price2.max_people_group
                return min == false || max == false
              }
              else{
                return false
              }
            })
            return group2 != undefined
        })
        if (group1 != undefined){
          this.groupsConflict = {}
          this.groupsConflict['id'] = idGroup
          return false
        }
        else{
          this.groupsConflict = {}
          return true
        }
      }

    },
    validateBeforeSubmit: function(){
      var beforeSubmit = _.clone(this.service.draft)
      this.userWantsToFinalize = true
      this.$set(this.service, 'draft', false)
      window.ELEMENT.Message.closeAll()
      var hours = this.validateHours()
      var errorsMessage = ''


      this.$forceUpdate()

      this.$nextTick(function(){
        this.savingService = true
        this.$validator.validateAll().then((result) => {
          if (result) {
            if (this.validateSinceAndUntilDateInSchedule() == false) {
              this.savingService = false
              // this.$set(this.service, 'draft', beforeSubmit)
              return
            }
            if (this.validateExcludeDatesInSchedule() == false) {
              this.savingService = false
              // this.$set(this.service, 'draft', beforeSubmit)
              this.$forceUpdate()
              return
            }
            if (this.validateGroupsDifferent() == false) {
              this.savingService = false
              // this.$set(this.service, 'draft', beforeSubmit)
              return
            }
            if (this.validateErrorPrices(true) == true) {
              this.errorPrice = 'Pendiente: Habilitar al menos un tipo de precio.'
              this.savingService = false
              // this.$set(this.service, 'draft', beforeSubmit)
              return
            }
            this.isSaving = true
            this.savingMessage = this.getSavingMessage()

            this.$set(this.service, 'draft', false)

            axios.post(window.saveServiceUrl, this.service)
            .then((response) =>{
              this.savingService = false
              this.isSaving = false
              this.dirty = false
              this.savingMessage = this.getLastSavedChangeMessage()
              window.location = this.changelistServiceUrl
            })
            return
          }
          this.$set(this.service, 'draft', beforeSubmit)
          this.savingService = false
          var errors = _.map(this.errorsVee.items,function(error){
                if (error.field.indexOf('min_') == 0 || error.field.indexOf('max_') == 0){
                  var index = parseInt(error.field.substr(4));
                  if (index == 2) index = index - 1
                  else index = index - 3
                  return '<p style="margin-top:5px;">&nbsp;&nbsp;- Grupo Nº ' + index + ': ' + error.msg + '</p>'
                }
                return '<p style="margin-top:5px;">&nbsp;&nbsp;- ' + error.msg + '</p>'
          });
          errors = _.uniq(errors)
          var priceError = this.validateErrorPrices(false)
          if (priceError) {
            errors.unshift('<p style="margin-top:5px;">&nbsp;&nbsp;- Habilitar al menos un tipo de precio.</p>')
            // this.$message({
            //   message: 'El formulario tiene errores, por favor revise los datos.\n' + '       - Habilitar al menos un tipo de precio',
            //   type: 'error',
            // });
            this.savingService = false
            this.errorPrice = 'Pendiente: Habilitar al menos un tipo de precio.'
            this.$set(this.service, 'draft', beforeSubmit)
            // return
          }
          this.errorPrice = ""

          if(_.keys(hours).length == 0){
            errors.unshift('<p style="margin-top:5px;">&nbsp;&nbsp;- No has agregado horarios para este servicio, por favor revisa los datos.</p>')
          } else {
            var hourErrorFound = false
            _.forIn(hours, function(value, key){
              if(value == false){
                hourErrorFound = true
              }
            }.bind(this))
            if(hourErrorFound){
              errors.unshift('<p style="margin-top:5px;">&nbsp;&nbsp;- Hay horas repetidas en el mismo día en el horario.<p>')
            }
          }

          if(this.validateGroupsEmpty() == true){

            errors.unshift('<p style="margin-top:5px;">&nbsp;&nbsp;- Existen grupos con campos vacíos.</p>')
          }

          if(this.validateGroups() == false){
            errors.unshift('<p style="margin-top:5px;">&nbsp;&nbsp;- Existen grupos con datos repetidos.</p>')
          }
          if (this.validateExcludeDatesEquals()){
            errors.unshift('<p style="margin-top:5px;">&nbsp;&nbsp;- Existen fechas excluidas repetidas.</p>')
          }

          this.$message({
            dangerouslyUseHTMLString: true,
            message: '<p>El formulario tiene errores, por favor revise los datos:</p><br>' + errors.join(''),
            type: 'error',
            duration: 6000,
            showClose: true,
          });
          // if(errorsMessage.length > 0) {
          //   this.$message({
          //     message: errorsMessage,
          //     type: 'error',
          //     duration: 7000,
          //     showClose: true,
          //   })
          //   this.$set(this.service, 'draft', beforeSubmit)
          // }
        });
      }.bind(this))


    },
    saveService: function(){

      this.savingMessage = this.getSavingMessage()
      this.isSaving = true

      axios.post(window.saveServiceUrl, this.service)
      .then((response) =>{
        this.isSaving = false
        this.dirty = false
        this.savingMessage = this.getLastSavedChangeMessage()
      })
    },
    getSavingMessage: function(){
      return 'Guardando cambios...'
    },
    getLastSavedChangeMessage: function(){
      return 'Último cambio guardadado a las ' + moment.utc().subtract(5, 'hours').format('hh:mm a')
    },
    getValidationRulesForMinGroup: function(price){
      var shouldValidate = this.service.draft === false  || this.userWantsToFinalize === true
      var rules = ''
      if (shouldValidate){
        rules += 'required|not_in:' + String(price.max_people_group)
        if (price.max_people_group > 0) {
          var maxValue = price.max_people_group - 1
          if (maxValue >= this.service.min) {
              rules += '|max_value:' + String(price.max_people_group - 1)
          }
        } else {
          rules += '|max_value:' + this.service.max
        }
        rules += '|min_value:' + this.service.min
      }
      
      return rules
    },
    helpFocus: function(){
      console.log('asdas')
    }

  }
})

window.app = app
