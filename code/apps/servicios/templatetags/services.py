from django import template

register = template.Library()

@register.simple_tag(takes_context=True)
def user_has_service(context):
	user = context.request.user
	company_services_count = Service.objects.filter(company=user.profile.company).count()
	return company_services_count > 1
