from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE
from apps.usuarios.models import User
from stdimage.models import StdImageField
import uuid
# Create your models here.


class Agent(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    name = models.CharField(
        max_length=50,
        null=True,
        verbose_name="Nombre de Empresa"
    )
    url = models.CharField(
        max_length=100,
        null=True
    )
    phone = models.CharField(
        max_length=30,
        null=True,
        verbose_name="Teléfono de Empresa"
    )

    address = models.CharField(
        max_length=100,
        null=True,
        verbose_name="Dirección de Empresa"
    )

    email = models.EmailField(
        max_length=100,
        null=True,
        verbose_name="Correo de Empresa"
    )

    ruc = models.CharField(
        max_length=50,
        null=True,
        verbose_name="Dirección de Empresa"
    )

    country = models.CharField(
        max_length=100,
        null=True,
        verbose_name="País"
    )

    city = models.CharField(
        max_length=100,
        null=True,
        verbose_name="Ciudad"
    )

    is_company = models.BooleanField(
        default=False
    )

    private_key = models.TextField(
        null=True
    )

    public_key = models.TextField(
        null=True
    )

    logo = StdImageField(
        upload_to='companies_logo',
        null=True,
        verbose_name="Imagen",
        variations={
            'large': (600, 400),
            'medium': (300, 200),
            'small': (100, 100)
        }
    )

    user = models.ForeignKey(
        User,
        related_name="agent"
    )


class AgentInvite(SafeDeleteModel):
    token = models.UUIDField(
        verbose_name="Token",
        unique=True,
        default=uuid.uuid4
    )
    email = models.EmailField()

    state = models.CharField(
        max_length=10
    )

    date = models.DateField(
        auto_now=True
    )
    operator = models.ForeignKey(
        User,
        related_name="operator_invite"
    )

    agent = models.ForeignKey(
        User,
        null=True
    )

    agent_intern = models.BooleanField()
