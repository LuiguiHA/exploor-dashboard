from django.shortcuts import render
from django.db import connection
from django.views.generic import TemplateView, View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.core.mail import send_mail
from django.contrib.auth.models import Permission
from django.template.loader import render_to_string
from django.shortcuts import render, redirect
from django.urls import reverse
# from django.contrib.auth.models import User
from apps.usuarios.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.conf import settings
from django.contrib.auth import authenticate, login
from django.contrib.auth.views import login as login_view
from django.http import HttpResponse, HttpResponseNotFound, Http404
from apps.usuarios.models import Company, TypeUser, Country, City
from .models import Agent, AgentInvite
import random
import string
import json
import uuid
import uuid

from usuarios.tokens import account_activation_token
# Create your views here.


class AgentView(View):
    template_name = "agent_create.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AgentView, self).dispatch(request, *args, **kwargs)

    def get(self, request, token, *args, **kwargs):
        agent_invite = list(AgentInvite.objects.filter(token=token))
        if len(agent_invite) == 0:
            #raise Http404("Token no válido")
            return HttpResponseNotFound("<h1>Token Inválido</h1>")

        invite = agent_invite[0]
        user_exist = User.objects.filter(email=invite.email).exists()
        if user_exist:
            agent_invite = agent_invite[0]
            if agent_invite.agent:
                return redirect(reverse('auth_login_'))
            else:
                user_agent = User.objects.get(email=invite.email)
                agent_invite.agent = user_agent
                agent_invite.state = 'confirm'
                agent_invite.save()
                company = Company.objects.get(users=agent_invite.operator)
                company.users.add(user_agent)
                email_template = render_to_string(
                    "emails/confirm_registration_agent.html",
                    {
                        'user_agent': user_agent,
                        'operator': agent_invite.operator
                    }
                )
                send_mail(
                    'Bienvenido!',
                    'Texto plano',
                    settings.DEFAULT_FROM_EMAIL,
                    [agent_invite.operator.email],
                    fail_silently=False,
                    html_message=email_template,
                )

                self.template_name = "agent_confirm.html"
                context = {
                    'company': agent_invite.operator.company_name()
                }
        else:
            countries = Country.objects.all()
            countries_list = []
            dic = {}
            dic["value"] = 0
            dic["text"] = 'Selecciona tu país'
            countries_list.append(dic)
            for country in countries:
                dic = {}
                dic["value"] = country.id
                dic["text"] = country.name
                countries_list.append(dic)

            context = {
                'countries': json.dumps(countries_list),
                'email': invite.email
            }

        return render(request, self.template_name, context)

    @method_decorator(csrf_exempt)
    def post(self, request, token, *args, **kwargs):
        data = request.POST
        agent = json.loads(data.get('agent'))
        data = request.FILES
        image = data.get('image')
        # CREATE USER OF AGENT
        user = User()
        user.first_name = agent.get('name').capitalize()
        user.last_name = agent.get('lastName').capitalize()
        user.email = agent.get('email')
        user.is_active = True
        user.type_user_id = 3
        user.set_password(agent.get('password'))
        user.save()
        # CREATE AGENT
        agent_object = Agent()
        agent_object.logo = image
        agent_object.private_key = agent.get('privateKey')
        agent_object.public_key = agent.get('publicKey')
        agent_object.user = user
        agent_object.is_company = False
        if agent.get('toggleAgent') == 1:
            agent_object.is_company = True
            agent_object.name = agent.get('companyName').capitalize()
            agent_object.url = agent.get('companyWebUrl')
            agent_object.phone = agent.get('companyPhone')
            agent_object.address = agent.get('companyAddress')
            agent_object.email = agent.get('companyEmail')
            agent_object.ruc = agent.get('companyRuc')
            if agent.get('companyCountry') != 0:
                country = Country.objects.get(id=agent.get('companyCountry'))
                agent_object.country = country.name
            if agent.get('companyCity') != 0 and agent.get('companyCity') != '':
                if type(agent.get('companyCity')) is int:
                    city = City.objects.get(id=agent.get('companyCity'))
                    agent_object.city = city.name
                else:
                    agent_object.city = agent.get('companyCity').capitalize()

        agent_object.save()
        # CHANGE STATE OF INVITATION
        invite = AgentInvite.objects.get(token=token)
        invite.state = 'confirm'
        invite.agent = user
        invite.save()
        # RELATION COMPANY WITH USER OF AGENT
        company = Company.objects.get(users=invite.operator)
        company.users.add(user)
        # PERMISOS
        permission = Permission.objects.get(codename='add_order')
        user.user_permissions.add(permission)
        permission = Permission.objects.get(codename='change_order')
        user.user_permissions.add(permission)
        permission = Permission.objects.get(codename='delete_order')
        user.user_permissions.add(permission)
        # ENVIO DE CORREO AL OPERADOR - AVISO
        email_template = render_to_string(
            "emails/confirm_registration_agent.html",
            {
                'user_agent': user,
                'operator': invite.operator
            }
        )
        send_mail(
            'Bienvenido!',
            'Texto plano',
            settings.DEFAULT_FROM_EMAIL,
            [invite.operator.email],
            fail_silently=False,
            html_message=email_template,
        )

        return JsonResponse({'success': True})


class InviteView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(InviteView, self).dispatch(request, *args, **kwargs)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        emails = data.get('emails')
        for email in emails:
            agent_invite = AgentInvite.objects.filter(operator=request.user,
                                                      email=email).exists()
            if not agent_invite:
                token = uuid.uuid4().hex
                invite = AgentInvite()
                invite.token = token
                invite.agent_intern = False
                invite.email = email
                invite.state = 'pending'
                invite.operator = request.user
                invite.save()

                current_site = get_current_site(request)
                email_template = render_to_string(
                    "emails/invitation_agent.html",
                    {
                        'operator': request.user,
                        'token_invite': invite.token,
                        'domain': settings.DOMAIN
                    }
                )
                send_mail(
                    'Bienvenido!',
                    'Texto plano',
                    settings.DEFAULT_FROM_EMAIL,
                    [email],
                    fail_silently=False,
                    html_message=email_template,
                )

        return JsonResponse({'success': True})


class SearchAgentView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(SearchAgentView, self).dispatch(request, *args, **kwargs)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        query = data.get('query')
        query = query.strip()
        result_list = []
        # users = list(User.objects.filter(email__icontains=query,
        #                                  is_active=True,
        #                                  type_user=3)[:10])
        # agents_invite = AgentInvite.objects.filter(
        #     operator=request.user,
        #     email__in=[user.email for user in users])
        # invited_emails = [agent_invite.email for agent_invite in agents_invite]
        # # USUARIOS DISPONIBLES
        # users_available = filter(lambda user: user.email not in invited_emails, users)
        # for user in users_available:
        #     dic = {}
        #     dic['value'] = user.email
        #     dic['label'] = user.email
        #     dic['disabled'] = False
        #     result_list.append(dic)
        operator_exists = User.objects.filter(email=query, type_user__id__in=[1,2]).exists()
        if operator_exists:
            operator = User.objects.get(email=query, type_user__id__in=[1,2])
            dic = {}
            dic['value'] = operator.email
            dic['label'] = operator.email
            dic['disabled'] = True
            if operator.type_user.id == 1:
                dic['operator'] = True
            if operator.type_user.id == 2:
                dic['agency'] = True
            result_list.append(dic)
        # CORREOS INVITADOS
        count_available = 10 - len(result_list)
        if count_available > 0:
            invites_pending = list(AgentInvite.objects.filter(
                                    email__icontains=query,
                                    state="pending",
                                    operator=request.user))
            for invite in invites_pending:
                dic = {}
                dic['value'] = invite.email
                dic['label'] = invite.email
                dic['disabled'] = True
                dic['pending_invite'] = True
                result_list.append(dic)
        # CORREOS CONFIRMADOS
        count_available = 10 - len(result_list)
        if count_available > 0:
            invites_confirm = list(AgentInvite.objects.filter(
                                   email__icontains=query,
                                   state="confirm",
                                   operator=request.user)[:count_available])
            for invite in invites_confirm:
                dic = {}
                dic['value'] = invite.email
                dic['label'] = invite.email
                dic['disabled'] = True
                dic['confirm_invite'] = True
                result_list.append(dic)
        response = {'success': True, 'agents': result_list}

        return JsonResponse(response)


class AgentListView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AgentListView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        operator = request.user
        agents_confirm = list(AgentInvite.objects.filter(state='confirm',
                                                         operator=operator))
        agents_pending = list(AgentInvite.objects.filter(state='pending',
                                                         operator=operator))
        list_confirm = []
        for agent_confirm in agents_confirm:
            dic = {}
            dic['id'] = agent_confirm.id
            dic['name'] = agent_confirm.agent.first_name.title()
            dic['last_name'] = agent_confirm.agent.last_name.title()
            dic['email'] = agent_confirm.agent.email
            dic['agent_intern'] = agent_confirm.agent_intern
            list_confirm.append(dic)

        list_pending = []
        for agent_pending in agents_pending:
            dic = {}
            dic['email'] = agent_pending.email
            dic['date'] = agent_pending.date.strftime('%d-%m-%Y')
            list_pending.append(dic)

        return JsonResponse({'success': True,
                             'agents_confirm': list_confirm,
                             'agents_pending': list_pending})


class UnLinkAgentView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(UnLinkAgentView, self).dispatch(request, *args, **kwargs)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        email = data.get('email')
        user_agent = User.objects.get(email=email)
        AgentInvite.objects.get(operator=request.user, agent=user_agent).delete()
        company = Company.objects.get(users=request.user)
        company.users.remove(user_agent)
        return JsonResponse({"success": True})


class ReInviteView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ReInviteView, self).dispatch(request, *args, **kwargs)

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        from datetime import date

        data = json.loads(request.body)
        email = data.get('email')
        invite_date = date.today()
        agent_invite = AgentInvite.objects.get(operator=request.user, email=email)
        agent_invite.date = invite_date
        agent_invite.save()

        current_site = get_current_site(request)
        email_template = render_to_string(
            "emails/invitation_agent.html",
            {
                'operator': request.user,
                'token_invite': agent_invite.token,
                'domain': settings.DOMAIN
            }
        )
        send_mail(
            'Bienvenido!',
            'Texto plano',
            settings.DEFAULT_FROM_EMAIL,
            [email],
            fail_silently=False,
            html_message=email_template,
        )
        date = agent_invite.date.strftime('%d-%m-%Y')
        return JsonResponse({"success": True, 'date': date})


class AgentInternView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AgentInternView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        data = request.GET
        invitation = AgentInvite.objects.get(id=int(data.get('id')))
        is_intern = data.get('isIntern')
        invitation.agent_intern = True if is_intern == 'true' else False
        invitation.save()
        return JsonResponse({'success': True})
