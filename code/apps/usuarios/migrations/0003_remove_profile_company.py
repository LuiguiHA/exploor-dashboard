# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-02-21 19:40
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0002_auto_20180221_1435'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='company',
        ),
    ]
