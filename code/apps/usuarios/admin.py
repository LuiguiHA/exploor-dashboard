from django.contrib import admin
# from django.contrib.auth.models import User as UserDjango
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.apps import AuthConfig
from .models import User as UserExploor, OperatorAgency, Company
from .forms import *

AuthConfig.verbose_name = "Usuarios administradores"

# admin.site.unregister(UserDjango)
admin.site.unregister(Group)

# @admin.register(User)
class CustomUserAdmin(UserAdmin):

    add_form = CustomUserCreationForm
    ordering = ('email',)

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'first_name',
                'last_name',
                'email',
                'password1',
                'password2'
            ),
        }),
    )

    list_filter = []

    search_fields = ['first_name','last_name','email']

    fieldsets = (

        ("Información personal", {
            'fields': (
                'first_name',
                'last_name',
                'email',
                'password',
            )
        }),
    )

    def get_queryset(self, request):
        qs = super(CustomUserAdmin, self).get_queryset(request)
        return qs.filter(is_superuser=True)

    def __init__(self, *args, **kwargs):
        super(CustomUserAdmin,self).__init__(*args, **kwargs)
        CustomUserAdmin.list_display = list_display = ('first_name', 'last_name', 'email', 'is_superuser',)

    def get_form(self, request, obj=None, **kwargs):
        """
        Use special form during user creation
        """
        defaults = {}
        if obj is None:
            defaults['form'] = self.add_form
        defaults.update(kwargs)
        return super(CustomUserAdmin, self).get_form(request, obj, **defaults)


# def custom_user_display(self):
#     return "{} {}".format(self.first_name, self.last_name)

# User.add_to_class("__str__", custom_user_display)

admin.site.register(UserExploor, CustomUserAdmin)

# UserAdmin.list_filter = ('profile__company',)
# UserAdmin.list_display = ('first_name', 'last_name', 'email', 'is_superuser',)

# admin.site.register(User, UserAdmin)

class CustomOperatorAgencyAdmin(UserAdmin):

    list_filter = []
    ordering = ('email',)

    add_form = CustomOperatorAgencyCreationForm

    form = CustomOperatorAgencyChangeForm

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'first_name',
                'last_name',
                'email',
                'password1',
                'password2',
                'type_of_user',
                'company',
            ),
        }),
    )

    fieldsets = (

        ("Información personal", {
            'fields': (
                'first_name',
                'last_name',
                'email',
                'password1',
                'password2',
                'type_of_user',
                'company',
            ),
        }),
    )

    def get_queryset(self, request):
        qs = super(CustomOperatorAgencyAdmin, self).get_queryset(request)
        return qs.filter(is_superuser=False, type_user__in=[1, 2])


    def __init__(self, *args, **kwargs):
        super(CustomOperatorAgencyAdmin, self).__init__(*args, **kwargs)
        CustomOperatorAgencyAdmin.list_display = list_display = ('first_name', 'last_name', 'email', 'company_name')

    def get_form(self, request, obj=None, **kwargs):
        """
        Use special form during user creation
        """
        defaults = {}
        if obj is None:
            defaults['form'] = self.add_form
        defaults.update(kwargs)
        return super(CustomOperatorAgencyAdmin, self).get_form(request, obj, **defaults)

    # def company(self, obj):
    #     print(obj)
    #     print(self)
    #     # company = Company.objects.get(users=obj)
    #     # return company.name
    #     return 'adasdasdasd'
    #     # if obj.profile.company:
    #     #     return obj.profile.company.name
    #     # else:
    #     #     return ""
    # company.short_description = "Empresa"

admin.site.register(OperatorAgency, CustomOperatorAgencyAdmin)
