from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UsernameField
from django.contrib.auth import (
    authenticate, get_user_model, password_validation,
)
from django.utils.translation import gettext, gettext_lazy as _
import uuid
from .models import Company, User

from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.contrib.sites.shortcuts import get_current_site
from usuarios.tokens import account_activation_token
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth.models import Permission
from django.db import connection

class CustomUserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
        'exists_email': 'Ya existe un usuario con este correo'
    }
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput,
        strip=False,
        help_text=_("Enter the same password as before, for verification."),
    )

    class Meta:
        model = User
        fields = ("email",)
        # field_classes = {'username': UsernameField}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # if self._meta.model.USERNAME_FIELD in self.fields:
        #     self.fields[self._meta.model.USERNAME_FIELD].widget.attrs.update({'autofocus': True})

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def clean_email(self):
        email = self.cleaned_data.get("email")
        user = User.objects.filter(email=email).exists()
        if user:
            raise forms.ValidationError(
                self.error_messages['exists_email'],
                code='exists_email',
            )
        return email

    def _post_clean(self):
        super()._post_clean()
        # Validate the password after self.instance is updated with form data
        # by super().
        password = self.cleaned_data.get('password2')
        if password:
            try:
                password_validation.validate_password(password, self.instance)
            except forms.ValidationError as error:
                self.add_error('password2', error)

    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_active = True
        user.is_superuser = True
        user.set_password(self.cleaned_data["password1"])
        user.identifier = uuid.uuid4().hex
        if commit:
            user.save()
        return user


class CustomOperatorAgencyCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
        'empty_company_name': "El nombre de la compañía no puede ser vacío",
        'used_company_name': "Ya existe una compañía con ese nombre",
        'exists_email' : 'Ya existe un usuario con este correo'
    }
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput,
        strip=False,
        help_text=_("Enter the same password as before, for verification."),
    )
    company = forms.CharField(
        label="Nombre de la compañía",
        strip=False,
    )

    type_of_user = forms.ChoiceField(
        choices = ((1, "Operador"), (2, "Agencia")),
        label="Tipo de usuario",
        initial='1',
        widget=forms.Select(),
        required=True
    )

    class Meta:
        model = User
        fields = ("email",)
        # field_classes = {'username': UsernameField}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # if self._meta.model.USERNAME_FIELD in self.fields:
        #     self.fields[self._meta.model.USERNAME_FIELD].widget.attrs.update({'autofocus': True})

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def clean_company(self):
        company = self.cleaned_data.get("company")
        company_name = company.lower().replace(" ","")
        if not company or len(company) == 0:
            raise forms.ValidationError(
                self.error_messages['empty_company_name'],
                code='empty_company_name',
            )

        with connection.cursor() as cursor:
            cursor.execute(
                """SELECT name from usuarios_company
                    WHERE LOWER(REPLACE(name, ' ', '')) = %s and deleted is NULL""", [company_name]
            )
            db_all_rows = cursor.fetchall()

        if db_all_rows:
            raise forms.ValidationError(
                self.error_messages['used_company_name'],
                code='used_company_name',
            )

        return company

    def clean_email(self):
        email = self.cleaned_data.get("email")
        user = User.objects.filter(email=email).exists()
        if user:
            raise forms.ValidationError(
                self.error_messages['exists_email'],
                code='exists_email',
            )
        return email

    def _post_clean(self):
        super()._post_clean()
        # Validate the password after self.instance is updated with form data
        # by super().
        password = self.cleaned_data.get('password2')
        if password:
            try:
                password_validation.validate_password(password, self.instance)
            except forms.ValidationError as error:
                self.add_error('password2', error)

    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_active = False
        user.is_superuser = False
        user.set_password(self.cleaned_data["password1"])
        user.username = str(uuid.uuid4())
        user.type_of_user = str(self.cleaned_data["type_of_user"])
        user.save()
        company = Company.objects.create(name=self.cleaned_data["company"])
        company.user = user
        company.save()

        permission = Permission.objects.get(codename='add_service')
        user.user_permissions.add(permission)
        permission = Permission.objects.get(codename='change_service')
        user.user_permissions.add(permission)
        permission = Permission.objects.get(codename='delete_service')
        user.user_permissions.add(permission)

        permission = Permission.objects.get(codename='add_office')
        user.user_permissions.add(permission)
        permission = Permission.objects.get(codename='change_office')
        user.user_permissions.add(permission)
        permission = Permission.objects.get(codename='delete_office')
        user.user_permissions.add(permission)

        permission = Permission.objects.get(codename='add_order')
        user.user_permissions.add(permission)
        permission = Permission.objects.get(codename='change_order')
        user.user_permissions.add(permission)
        permission = Permission.objects.get(codename='delete_order')
        user.user_permissions.add(permission)

        current_site = settings.DOMAIN


        email_template = render_to_string(
            "emails/confirm_registration.html",
            {
                'user':user,
                'domain':current_site,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            }
        )
        send_mail(
            'Bienvenido!',
            'Texto plano',
            settings.DEFAULT_FROM_EMAIL,
            [user.email],
            fail_silently=False,
            html_message=email_template,
        )

        return user


class CustomOperatorAgencyChangeForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
        'empty_company_name': "El nombre de la compañía no puede ser vacío",
        'used_company_name': "Ya existe una compañía con ese nombre",
    }
    password1 = forms.CharField(
        label=_("Password"),
        strip=False,
        required=False,
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput,
        strip=False,
        required=False,
        help_text=_("Enter the same password as before, for verification."),
    )
    company = forms.CharField(
        label="Nombre de la compañía",
        strip=False,
    )

    type_of_user = forms.ChoiceField(
        choices = ((1, "Operador"), (2, "Agencia")),
        label="Tipo de usuario",
        initial='1',
        widget=forms.Select(),
        required=True
    )

    class Meta:
        model = User
        fields = ("email",)
        # field_classes = {'username': UsernameField}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk:
            self.initial['company'] = self.instance.profile.company.name
            self.initial['type_of_user'] = self.instance.profile.type_of_user

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if len(password1) or len(password2):
            if password1 and password2 and password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        return password2

    def clean_company(self):
        company = self.cleaned_data.get("company")
        if not company or len(company) == 0:
            raise forms.ValidationError(
                self.error_messages['empty_company_name'],
                code='empty_company_name',
            )

        company_obj = Company.objects.filter(name=company)
        if company_obj.exists():
            if self.instance.profile.company.id != company_obj.first().id:
                raise forms.ValidationError(
                    self.error_messages['used_company_name'],
                    code='used_company_name',
                )

        return company

    def _post_clean(self):
        super()._post_clean()
        # Validate the password after self.instance is updated with form data
        # by super().
        password = self.cleaned_data.get('password2')
        if password:
            try:
                password_validation.validate_password(password, self.instance)
            except forms.ValidationError as error:
                self.add_error('password2', error)

    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_active = True
        user.is_superuser = False
        if len(self.cleaned_data["password1"]):
            user.set_password(self.cleaned_data["password1"])
        user.username = str(uuid.uuid4())
        user.type_of_user = str(self.cleaned_data["type_of_user"])
        user.save()
        company = Company.objects.get(users=user)
        company.name = self.cleaned_data["company"]
        company.save()
        return user
