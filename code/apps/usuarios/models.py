from django.db import models
# from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from safedelete.models import SafeDeleteModel
from safedelete.managers import SafeDeleteManager, DELETED_INVISIBLE
from safedelete.models import SOFT_DELETE, SOFT_DELETE_CASCADE
from apps.canales.models import DistributionChannel
from apps.servicios.models import Service
from safedelete.signals import post_softdelete
from django.dispatch import receiver
from django.contrib.auth.models import (BaseUserManager, AbstractBaseUser,
                                        PermissionsMixin)
import uuid
from stdimage.models import StdImageField
from django.utils.translation import gettext, gettext_lazy as _


class UserManager(BaseUserManager, SafeDeleteManager):

    _safedelete_visibility = DELETED_INVISIBLE

    def create_user(self, identifier, email, type_user, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email), identifier=identifier,
            type_user=type_user)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, identifier, first_name, last_name, password, email):
        user = self.create_user(
            email, password=password, first_name=first_name,
            last_name=last_name)
        user.is_admin = True
        user.is_active = True
        user.is_superuser = True
        user.save()
        return user


class TypeUser(models.Model):
    name = models.CharField(
        verbose_name="Tipo de Usuario",
        max_length=60
    )


class User(AbstractBaseUser, PermissionsMixin, SafeDeleteModel):

    _safedelete_policy = SOFT_DELETE

    REQUIRED_FIELDS = ('first_name', 'last_name', 'email')
    USERNAME_FIELD = 'identifier'

    first_name = models.CharField(
        verbose_name="Nombres",
        max_length=60
    )

    last_name = models.CharField(
        verbose_name="Apellidos",
        max_length=60
    )

    identifier = models.UUIDField(
        verbose_name="Identificador",
        unique=True,
        default=uuid.uuid4
    )

    email = models.EmailField(
        verbose_name="Correo",
    )

    type_user = models.ForeignKey(
        TypeUser
    )

    is_active = models.BooleanField(
        verbose_name="Está activo",
        default=False
    )

    is_superuser = models.BooleanField(
        verbose_name="Es administrador",
        default=False
    )
    language = models.CharField(
        _('Language'),
        max_length=30,
        null=True,
        blank=True
    )

    timezone = models.CharField(
        _('Timezone'),
        max_length=30,
        null=True,
        blank=True
    )

    objects = UserManager()

    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        db_table = 'auth_user'
        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"

    def __str__(self):
        return u'%s %s' % (self.first_name, self.last_name)

    @property
    def is_staff(self):
        return True

    @property
    def full_name(self):
        return self.get_full_name()

    def is_agent(self):
        return True if self.type_user.id == 3 else False

    def is_operator(self):
        return True if self.type_user.id < 3 else False

    def get_full_name(self):
        return u'{} {}'.format(self.first_name.capitalize(), self.last_name.capitalize())

    def get_short_name(self):
        return self.email

    def get_company_ids(self):
        companies = self.company_set.all()
        ids = [company.id for company in companies]
        return ids

    def company_name(self):
        companies = self.company_set.all()
        return ','.join([company.name.capitalize() for company in companies])
    company_name.short_description = "Empresa"

    def quantity_services(self):
        services = list(Service.objects.filter(user=self))
        return len(services) if len(services) else 0

    def get_agents(self):
        """This method needs to be called as operator, otherwise
        will throw an error"""
        company = Company.objects.get(users__in=[self])
        agents = company.users.filter(type_user_id=3)
        return agents

    def can_set_point_of_sale(self):
        return self.is_superuser or self.is_operator()

    def get_operator_company_users(self):
        """This method needs to be called as operator, otherwise
        will throw an error"""
        company = Company.objects.get(users__in=[self])
        return company.users


# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         Profile.objects.create(user=instance)
#
#
# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, **kwargs):
#     instance.profile.save()


class Country(models.Model):
    name = models.CharField(
        max_length=100
    )
    code = models.CharField(
        max_length=2,
        unique=True
    )


class City(models.Model):
    name = models.CharField(
        max_length=100
    )
    country = models.ForeignKey(
        Country
    )


class Company(SafeDeleteModel):
    name = models.CharField(
        max_length=30
    )

    company_web_url = models.URLField(
        max_length=250,
        null=True,
        verbose_name="Website"
    )

    phone = models.CharField(
        max_length=30,
        null=True,
        verbose_name="Teléfono de Empresa"
    )

    address = models.CharField(
        max_length=100,
        null=True,
        verbose_name="Dirección de Empresa"
    )

    email = models.EmailField(
        max_length=100,
        null=True,
        verbose_name="Correo de Empresa"
    )

    ruc = models.CharField(
        max_length=50,
        null=True,
        verbose_name="Dirección de Empresa"
    )

    country = models.CharField(
        max_length=100,
        null=True,
        verbose_name="País"
    )

    city = models.CharField(
        max_length=100,
        null=True,
        verbose_name="Ciudad"
    )

    private_key = models.TextField(
        null=True
    )

    public_key = models.TextField(
        null=True
    )

    logo = StdImageField(
        upload_to='companies_logo',
        null=True,
        verbose_name="Imagen",
        variations={
            'large': (600, 400),
            'medium': (300, 200),
            'small': (100, 100)
        }
    )

    users = models.ManyToManyField(
        User
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Empresa'
        verbose_name_plural = 'Empresas'


# @receiver(post_softdelete, sender=User)
# def delete_user_company(sender, instance, **kwargs):
#     profile = instance.profile
#     profile.delete()


class OperatorAgency(User):
    class Meta:
        proxy = True
        verbose_name = "Operador/Agencia"
        verbose_name_plural = "operadores/Agencias"


@receiver(post_softdelete, sender=OperatorAgency)
def delete_operator_company(sender, instance, **kwargs):
    company = Company.objects.get(users=instance)
    company.delete()
