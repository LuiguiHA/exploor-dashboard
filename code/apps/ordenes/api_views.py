from rest_framework import status, serializers
from rest_framework.views import APIView
from rest_framework.response import Response
from apps.servicios.models import Service
from .serializers import OrderSerializer, OrderResponse, OrderResponseSerializer
from project.serializers import ErrorMessageResponseSerializer, ErrorMessageResponse
from project import api_errors
from rest_framework.utils.serializer_helpers import ReturnDict
from collections import OrderedDict

class OrderApiView(APIView):

	def post(self, request, format=None):
		token =request.auth
		try:
			profile = Profile.objects.get(token=token)
		except:
			return Response(ErrorMessageResponseSerializer(
				ErrorMessageResponse(api_errors.TOKEN_INVALID_SENT)
			).data)
		distribution_channel = profile.distribution_channel
		companies = distribution_channel.companies.all()
		companies_id = []
		for company in companies:
			companies_id.append(company.id)
		services = Service.objects.filter(
									company__in=companies_id,
									draft=False,
									published=True)
		services_id = [ service.id for service in services]

		service_pk = request.data.get('service')
		if not service_pk:
			return Response(ErrorMessageResponseSerializer(
				ErrorMessageResponse(api_errors.SERVICE_NOT_SENT)
			).data)

		if service_pk in services_id:
			request.data["channel_id"] = distribution_channel.id
			order_serializer = OrderSerializer(data=request.data)
			if order_serializer.is_valid():
				order = order_serializer.save()
				return Response(OrderResponseSerializer(
					OrderResponse(order)
					).data)
			else:
				errors = order_serializer.errors
				all_errors = ReturnDict(
					list(errors.items()) + [("error_code",[1006])],
					serializer = errors.serializer
				)
				return Response(all_errors,status = status.HTTP_400_BAD_REQUEST)
		else:
			return Response(ErrorMessageResponseSerializer(
			ErrorMessageResponse(api_errors.SERVICE_DONT_EXIST_PK)
			).data)
