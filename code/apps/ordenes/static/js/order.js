ELEMENT.locale(ELEMENT.lang.es)
// console.log()
Vue.use(VeeValidate,{fieldsBagName:'fieldsVee',errorBagName:'errorsVee', events:'input'})
VeeValidate.Validator.setLocale("es")

Vue.use(window.VueQuillEditor)

var app = new Vue({
  el: '#order',
  delimiters: ['${', '}'],
  components:{
  },
  data:{
    searchServiceUrl:window.searchServiceUrl,
    deleteOrderUrl:window.deleteOrderUrl,
    serviceCapacityUrl: window.serviceCapacityUrl,
    order: {
      current: window.dataOrder,
      firstName:"",
      lastName:"",
      email:"",
      mobileCustomer:"",
      reference:"",
      date:"",
      prices:[],
      schedule:[],
      date:"",
      hour:"",
      channel:"",
      payment:"",
      symbolCurrency:"",
      infoAdditional:"",
      serviceId:"",
      priceTotal:"",
      since: "",
      orderConfirmed: false,
      available: 0,
      resend: false,
      emailSend: window.emailSend,
      orderId: window.idOrder,
      client_country: ''
    },
    countries: window.countries,
    serviceData: window.dataService,
    isSuperAdmin: window.isSuperAdmin,
    isAgent: window.isAgent,
    referenceCode: window.referenceCode,
    serviceSelected:"",
    groupedServices:{},
    name:"",
    searchingService:false,
    sourceOfOrder:[],
    paymentTypes:window.paymentTypeList,
    searchTimeout:0,
    CancelToken: axios.CancelToken,
    sourceCancel: "",
    windowHeight:0,
    helpInfo:"",
    serviceType:"",
    serviceDateOptions: {},
    hoursVisible: false,
    dateDisabled: true,
    errorPrice: "",
    errorQuantity: "",
    // showOfficeDialog: false,
    agents: window.companyAgentsList,
    nameOffice: "",
    sinceDate : "",
    untilDate: "",
    minPeople: 0,
    maxPeople: 0,
    excluded_dates: [],
    savingOffice: false,
    savingOrder: false,
    deletingOrder: false,
    deletedHour: false,
    deletedDate: false,
    categoryDisabledMessage : "Para habilitar esta precio debe estar deshabilitado: ",
    deleteConfirmDialog: false,
    loadingCapacity: false,
    orderCopy: {}
  },
  created() {
    document.getElementById("order").style.display = "block";
    // this.$validator.attach('nameOffice', 'required');
  },
  mounted(){
    this.$nextTick(function() {
      window.addEventListener('resize', this.getWindowHeight);
      this.getWindowHeight()
    })

    // this.addPricesValidation()
    // var distributionChannels = this._.map(window.distributionChannels, function(distributionChannel){
    //   return {value: distributionChannel.id, label:distributionChannel.name, deleted:distributionChannel.deleted}
    // })

    // this.agents = this._.map(window.companyAgentsList, function(agent){
    //   return {value: agent.id, label:agent.name, deleted:agent.deleted}
    // })
    this.sourceOfOrder = [
    {
      label: "Canales de distribución",
      options: window.distributionChannels,
      type: "c"
    },
    {
      label: "Usuarios",
      options: this.agents,
      type: "u"
    }
    ]
    this.parseCurrentOrder()

    this.setDisabledDates()
    this.setHelp('order_help')

    window.addEventListener("beforeunload", this.beforeUnload);
  },
  watch: {
    "order.date": function(newValue){
      this.order.hour = ""
      this.$nextTick(function(){
        this.errorsVee.remove("hour")
      }.bind(this))
    },
    // showOfficeDialog: function(newValue){
    //   if (newValue){
    //     this.$validator.validate("nameOffice",this.nameOffice)
    //   }
    // },
    serviceSelected: function(newValue, oldValue){
      var allProducts = this._.flatten(this._.values(this.groupedServices))
      var productSelected = this._.find(allProducts, function(p){
        return p.id == newValue
      })
      this.order.serviceId = productSelected.id
      if(this.order.current == null){
        this.order.available = productSelected.available
      }
      this.errorPrice = ""
      this.order.date = ""
      this.deletedDate = false
      this.deletedHour = false
      this.$nextTick(function(){
        this.errorsVee.remove("date")
        this.errorsVee.remove("hour")
      }.bind(this))
      this.orderWithoutSchedule = true
      this.order.symbolCurrency = productSelected.currency
      this.order.schedule = productSelected.schedule
      this.sinceDate = moment(productSelected.since)
      this.untilDate = moment(productSelected.until)
      this.minPeople = productSelected.min_people
      this.maxPeople = productSelected.max_people
      this.excluded_dates = productSelected.excluded_dates
      this.dateDisabled = false
      var prices = this._.cloneDeep(productSelected.prices)
      this.order.prices = this._.map(prices, function(p){
        p.quantity = p.quantity || ""
        p.participants = p.participants || []
        p.enabled = p.enabled || false
        return p
      })
      this.quantityParticipantsValid()
      this.disableServicePrices()

    }
  },
  computed: {
    orderWithoutSchedule: function(){
      if (this.order.date == '' || this.order.hour == ''){
        return true
      }
      return false
    },
    hoursAvailable: function(){
      var dayOfWeek = -1
      if (this.order.date){
       dayOfWeek = moment(this.order.date).isoWeekday()
      }
      var allProducts = this._.flatten(this._.values(this.groupedServices))
      var productSelected = this._.find(allProducts, function(p){
        return p.id == this.serviceSelected
      }.bind(this))

      if(productSelected){
        var schedules = this._.filter(productSelected.schedule, function(schedule){
          return schedule.day == dayOfWeek
        })
        if (dayOfWeek == -1){ // cuando no se selecciono fecha
          return []
        }
        if (schedules.length == 0){ // existe la fecha pero no tiene horarios
          this.deletedHour = true
          return [{'label': this.order.hour, 'value': this.order.hour, 'disabled': true}]
        }
        else{
          schedules = this._.map(schedules, function(s){
            return {'label': s.hour, 'value': s.hour, 'disabled': false}
          })
          if (this.order.hour){
            hour = this._.find(schedules,function(s){
            return s.value == this.order.hour
            }.bind(this))

            if (hour == undefined){
              this.deletedHour = true
              schedules.push({'label': this.order.hour, 'value': this.order.hour, 'disabled': true})
            }
          }
          return schedules
        }
      } else {
        return []
      }
    },
    dynamicHeight: function(){
      return String(this.windowHeight - 130) + "px"
    },
    dynamicHeightHelp: function(){
      return String(this.windowHeight - 130) + "px"
    },
    _() {
      return _;
    }
  },
  methods:{
    beforeUnload: function(event){
      if (!(_.isEqual(this.orderCopy,this.order))){
        var confirmationMessage = "Si abandonas esta página, se perderán todos los cambios no guardados. ¿Estás seguro de que quieres salir de esta página?";
        event.returnValue = confirmationMessage;
        return confirmationMessage
        }
    },
    parseCurrentOrder:function(){
      if(this.order.current){
        var serviceCategory = this.order.current.service.category
        this.order.available = this.order.current.available
        this.groupedServices[serviceCategory] = [this.order.current.service]
        this.serviceSelected = this.order.current.service.id
        this.order.firstName = this.order.current.firstname
        this.order.lastName = this.order.current.lastname
        this.order.mobileCustomer = this.order.current.mobile_customer
        this.order.reference = this.order.current.reference
        this.order.email= this.order.current.email
        this.order.infoAdditional= this.order.current.info_additional
        this.order.orderConfirmed = this.order.current.confirmed
        this.order.payment = this.order.current.payment_type
        this.order.client_country = this.order.current.client_country
        if (this.order.current.user_assigned_id){
          this.order.channel = "u-" + this.order.current.user_assigned_id
        }
        else{
          this.order.channel = "c-" + this.order.current.distribution_channel_id
        }
        this.$nextTick(function(){
          this.order.date = moment(this.order.current.out_date,'YYYY-MM-DD H:mm:ss ZZ').toDate()
          this.$nextTick(function(){
            this.dateDeletedVerify(this.order.date)
          }.bind(this))
          this.$nextTick(function(){
            this.order.hour = this.order.current.hour
            this.orderCopy = _.clone(this.order)
          }.bind(this))
        }.bind(this))
      }
      else{
        this.order.reference = this.referenceCode
        this.orderCopy = _.clone(this.order)
        this.groupedServices[this.serviceData.category] = [this.serviceData]
        this.serviceSelected = this.serviceData.id
        this.$nextTick(function(){
          this.order.date = moment(this.serviceData.out_date,'YYYY-MM-DD H:mm:ss ZZ').toDate()
          this.$nextTick(function(){
            this.order.hour = this.serviceData.hour
            this.orderCopy = _.clone(this.order)
          }.bind(this))
        }.bind(this))

      }
    },
    changeDeletedHour: function(){
      this.deletedHour = false
      this.loadingCapacity = true
      axios.get(this.serviceCapacityUrl, {
        params:{
          service_id: this.serviceSelected,
          date: moment(this.order.date).format('YYYY-MM-DD'),
          hour: moment(this.order.hour, 'hh:mm a').add(5,'hours').format('HH:mm')

        }
      })
      .then((response) => {
        this.order.available = response.data.available
        this.loadingCapacity = false
      })

    },
    changeDeletedDate: function(){
      this.deletedDate = false
    },
    dateDeletedVerify: function(date){
      dateWeekday = moment(date).isoWeekday()
      var day = _.find(this.order.schedule, function(schedule){
          return schedule.day == dateWeekday
        })
      if (day == undefined){
        this.deletedDate = true
        return
      }
      var isAfterSince = moment(date).isSameOrAfter(moment(this.sinceDate))
      var isBeforeUntil = moment(date).isSameOrBefore(moment(this.untilDate))
      if (isAfterSince == false || isBeforeUntil == false){
        this.deletedDate = true
        return
      }
      var dateIsExcluded = false
      _.forEach(this.excluded_dates,function(excluded_date){
        var isAfterSinceExcluded = moment(date).isSameOrAfter(moment(excluded_date["since"]))
        var isBeforeUntilExcluded = moment(date).isSameOrBefore(moment(excluded_date["until"]))
        if (isAfterSinceExcluded && isBeforeUntilExcluded) dateIsExcluded = true
      })
      if (dateIsExcluded){
        this.deletedDate = true
        return
      }
    },
    setDisabledDates: function(){
      this.serviceDateOptions.disabledDate = function(date){
        var scheduleOfDay = _.find(this.order.schedule, function(schedule){
          return schedule.day == moment(date).isoWeekday()
        }.bind(this))
        if (scheduleOfDay == undefined){ // true cuando deshabilita la fecha
          return true
        }
        else{
          var dateIsExcluded = false
          _.forEach(this.excluded_dates,function(excluded_date){
            var isAfterSinceExcluded = moment(date).isSameOrAfter(moment(excluded_date["since"]))
            var isBeforeUntilExcluded = moment(date).isSameOrBefore(moment(excluded_date["until"]))
            if (isAfterSinceExcluded && isBeforeUntilExcluded) dateIsExcluded = true
          }.bind(this))
          if (dateIsExcluded) return true
          var isAfterSince = moment(date).isSameOrAfter(moment(this.sinceDate))
          var isBeforeUntil = moment(date).isSameOrBefore(moment(this.untilDate))
          return !(isAfterSince && isBeforeUntil)
        }
      }.bind(this)
    },
    getWindowHeight: function(event) {
      this.windowHeight = document.documentElement.clientHeight;
    },
    disableServicePrices: function(){
      this.validateErrorPrices()

      _.forEach(this.order.prices, function(p){
        p.disabled = false

      })

      var enabledPrices = _.filter(this.order.prices,function(p){
        return p.enabled
      })

      var disabledPrices = _.uniq(_.flatMap(enabledPrices,function(ep){
        return ep.category__fields_disabled.split(",")
      }))

      _.forEach(disabledPrices,function(dp){
        var priceFound = _.find(this.order.prices,function(p){
          return p.category__id == parseInt(dp)
        })
        if (priceFound){
          priceFound.enabled = false
          priceFound.disabled = true
        }
      }.bind(this))
    },
    validateServicePrice: function(price){
      var prices_disabled = []
      prices_disabled = price.category__fields_disabled.split(",")
      prices_disabled = _.uniq(prices_disabled)
      _.forEach(this.order.prices,function(p){
        var priceFound = _.find(prices_disabled,function(pd){
          return p.category__id == parseInt(pd)
        })
        if (priceFound){
          p.enabled = false
          p.disabled = true
        }
      })
    },
    getPriceTooltipInfo: function(price){
      if(price.disabled){
        var prices_disabled = price.category__fields_disabled.split(",")
        var categoriesName = _.map(prices_disabled,function(priceDisabledId){
          var priceFound = _.find(this.order.prices,function(p){
            return p.category__id == parseInt(priceDisabledId)
          })
          if (priceFound){
            return priceFound.category__name
          }
          return null
        }.bind(this))

        categoriesName = _.filter(categoriesName,function(cn){
          return cn != null
        })
        return this.categoryDisabledMessage + " " + _.join(categoriesName, ", ") + "."

      } else {
        return "Haz click aquí para habilitar esta opción."
      }
    },
    validateErrorPrices: function(){
      if (this.order.prices.length > 0){
        var error = _.find(this.order.prices,function(p){ return p.enabled})
        if (error){
          this.errorPrice = ""
          return false
        }
        else{
          return true
        }
      }
      else{
        return false
      }
    },
    updateData:function(price){
      var temp = ""
      if (price.update_price){
        temp = price.price
        price.price = price.update_price
        price.update_price = temp
      }
      if (price.update_max_people){
        temp = price.max_people
        price.max_people = price.update_max_people
        price.update_max_people = temp
      }
      if (price.update_min_people){
        temp = price.min_people
        price.min_people = price.update_min_people
        price.update_min_people = temp
      }
      if (price.update_group_applied){
        temp = price.group_price_applied_to
        price.group_price_applied_to = price.update_group_applied
        price.update_group_applied = temp
      }
    },
    // addOffice: function(){
    //   this.savingOffice = true
    //   axios.post(window.registerOfficeUrl, {"nameOffice":this.nameOffice})
    //   .then((response) =>{
    //     this.savingOffice = false
    //     var data = response.data
    //     if (data.state){
    //       this.showOfficeDialog = false
    //       this.offices.push(data.office)
    //       this.$message({
    //         message: 'Oficina agregada exitosamente',
    //         type: 'success'
    //       });
    //       this.order.channel = "o-" + data.office.value
    //     }
    //     else{
    //       window.ELEMENT.Message.closeAll()
    //       this.$message({
    //         message: 'Ya existe una oficina con ese nombre',
    //         type: 'error'
    //       });
    //     }
    //   })
    //     // this.order.offices.push(newOffice)
    // },
    validateErrorQuantity: function(){
      var prices_enabled = _.filter(this.order.prices,function(p) { return p.enabled})
      var people_quantity = 0
      _.forEach(prices_enabled,function(pe){
        people_quantity = people_quantity + pe.quantity
        this.order.people_quantity = people_quantity
      })
      if (this.maxPeople == 0){
        if (people_quantity < this.minPeople){
          this.errorQuantity = "El mínimo de personas permitido para el servicio seleccionado es de " + this.minPeople + "."
          return true
        }
      }
      else{
        if(people_quantity > this.order.available){
          this.errorQuantity = "La cantidad de participantes supera la capacidad disponible para el servicio y horario seleccionado."
          return true
        }
      }
      return false
    },
    quantityParticipantsValid: function(){
      var _this = this
      VeeValidate.Validator.extend('quantityParticipant', {
        getMessage: function(field, params, data){
          return 'La cantidad es menor que los participantes agregados'
        },
        validate: function(value, args) {
          var quantity = args[0]
          if(quantity == 0){
            return true
          }
          if(quantity > value){
            return false
          }
          return true
        }
      })
    },
    saveOrder: function(){
      this.savingOrder = true
      this.$validator.validateAll().then((result) => {
        var priceError = this.validateErrorPrices()
        if (priceError){
          this.$message({
            message: 'El formulario tiene errores, por favor revise los datos.',
            type: 'error'
          });
          this.savingOrder = false
          this.errorPrice= "Pendiente: Habilitar al menos un tipo de precio."
          return
        }
        this.errorPrice = ""
        var quantityError = this.validateErrorQuantity()
        if (quantityError){
          this.$message({
            message: this.errorQuantity,
            type: 'error'
          });
          this.savingOrder = false
          return
        }
        if (result){
          axios.post(window.saveOrderUrl, this.order)
          .then((response) =>{
            this.savingOrder = false
            window.removeEventListener("beforeunload", this.beforeUnload);
            window.location = window.changelistOrderUrl
          })
        }
        else{
          this.$message({
            message: 'El formulario tiene errores, por favor revise los datos.',
            type: 'error'
          });
          this.savingOrder = false
        }
      })
    },
    redirectListOrder: function(){
      window.addEventListener("beforeunload", this.beforeUnload);
      window.location = window.changelistOrderUrl
    },
    confirmDelete: function(){
      this.deletingOrder = true
      this.deleteConfirmDialog = true
      axios.post(this.deleteOrderUrl, {
        orderId: this.order.orderId
      })
      .then((response) =>{
        this.deletingOrder = false
        window.location = window.changelistOrderUrl
      })
    },
    deleteOrder: function(){
      this.deleteConfirmDialog = true
    },
    getPriceString: function(index){
      var price = this.order.prices[index]
      if(price.category__name != 'Grupo'){
        return this.order.symbolCurrency + price.price
      } else {
        return this.order.symbolCurrency + price.price + (price.group_price_applied_to == 1 ? " por persona" : " por grupo") + ", mínimo " + price.min_people + " personas y máximo " + price.max_people + "."
      }
    },
    getQuantityValidation: function(index){
      var price = this.order.prices[index]
      if(price.category__name != 'Grupo' ){
        return 'numeric'
      } else {
        return 'min_value:'+price.min_people+'|max_value:'+price.max_people
      }
    },
    getTotal: function(){
      var partitionPrices = _.partition(this.order.prices, function(o) { return o.category__name == "Grupo"; });
      var notGroupPrices = this._.reduce(partitionPrices[1], function(acc, price){
        if(price.quantity != "" && price.enabled){
          return acc + (price.quantity * price.price)
        } else {
          return acc
        }

      }, 0)

      var groupPrices = this._.reduce(partitionPrices[0], function(acc, price){
        if(price.quantity != "" && price.enabled){
          if(price.group_price_applied_to == 1){
            return acc + (price.quantity * price.price)
          } else {
            return acc + price.price
          }
        } else {
          return acc
        }
      }, 0)
      this.order.priceTotal = notGroupPrices + groupPrices
      return notGroupPrices + groupPrices
    },
    remoteMethod : function(query){
      this.searchingService = true
      clearTimeout(this.searchTimeout)
      if(this.sourceCancel != ""){
        this.sourceCancel.cancel("")
      }

      if(query == ""){
        this.searchingService = false
        this.groupedServices = []
      } else {
        this.sourceCancel = this.CancelToken.source()

        this.searchTimeout = setTimeout(function(){
          axios.get(this.searchServiceUrl, {
            cancelToken: this.sourceCancel.token,
            params:{
              q: query
            }
          })
          .then((response) =>{
            this.searchingService = false
            var data = JSON.parse(response.data)
            this.groupedServices = this._.groupBy(data, function(e){ return e.category })
            if(this.order.current){
              var serviceCategory = this.order.current.service.category
              if (this.groupedServices[serviceCategory]){
                if (this.order.current.service.deleted){
                  this.groupedServices[serviceCategory].unshift(this.order.current.service)
                }
              }
              else{
                this.groupedServices[serviceCategory] = [this.order.current.service]
              }
            }

          })
        }.bind(this), 500)
      }
    },
    addParticipant : function(index){
      if(this.order.prices[index].participants.length < this.order.prices[index].quantity){
        this.order.prices[index].participants.push({
          firstName: "",
          lastName: ""
        })
      }
      else{
        this.$message({
          message: 'No puedes tener mas participantes que la cantidad indicada.',
          type: 'error'
        });
      }
        
    },
    removeParticipant: function(index,participantIndex){
      this.order.prices[index].participants.splice(participantIndex,1)
    },
    setHelp: function(value){
      var textField = _.find(window.fieldsHelpList, function(o) { return o.field_name == value });
      if(textField){
        this.helpInfo = textField.help_text
      } else {
        this.helpInfo = ""
      }
    },
    setHelpChange: function(visible, textInfo){
      if (visible){
        setTimeout(function(){
          this.setHelp(textInfo)
        }.bind(this), 100)
      }
      // else{
      //   this.setHelp('order_help')
      // }
    }
  }
})
