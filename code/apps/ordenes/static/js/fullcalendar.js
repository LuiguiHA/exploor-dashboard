
Vue.component('full-calendar', {
    props: ['events', 'value', 'dateType', 'selectedRange'],
    template: "<div id='content'></div>",
    data: function (){
      return {
        views: ['agendaDay', 'agendaWeek', 'month'],
        defaultView: ''
      }
    },
   mounted: function () {
      var vm = this
      var eventsList = this.formatEvents(this.events)
      $(this.$el).fullCalendar({
        header: {
          // left: 'month,agendaWeek,agendaDay custom1',
          left: '',
          center: 'title',
          right: ''
          // right: 'prevYear,prev,next,nextYear'
        },
        defaultView: this.views[this.dateType - 1],
        footer: {
          left: '',
          center: '',
          right: ''
        },
        locale: 'es',
        events: eventsList,
        timeFormat: 'h(:mm)a',
        eventColor: 'white',
        eventTextColor: '#1b4692',
        eventBorderColor: '#1b4692',
        eventClick: function(calEvent) {
          this.$emit('click', calEvent.key, calEvent.identifier, false)
          // change the border color just for fun
        }.bind(this),
        eventRender: function(event, element) {
          if (event.people_total == 0){
            message = 'Disponibilidad: Ilimitado'
          }
          else{
            message = 'Disponibilidad: ' + String(event.available) + '/'  + String(event.total)
          }
          element.find('.fc-content').append('<div>' + message + '</div>')
        }
      });
      this.calculateView();
    },
    watch: {
      events: function (){
        this.calculateView();
        $(this.$el).fullCalendar('removeEvents')
        $(this.$el).fullCalendar('addEventSource', this.formatEvents(this.events))
        // if (this.dateType != ''){
        //   $(this.$el).fullCalendar('changeView', this.views[this.dateType - 1]);
        // }
        // else{
        //   var since = moment(this.selectedRange[0])
        //   var until = moment(this.selectedRange[1])
        //   var range = until.diff(since,'days') + 1
        //   console.log(range)
        //   if ( range == 1){
        //     $(this.$el).fullCalendar('changeView', 'agendaDay', since.clone().format('YYYY-MM-DD'));
        //   }
        //   if (1 < range && range < 8){
        //     var start = since.clone().format('YYYY-MM-DD')
        //     var end = until.clone().format('YYYY-MM-DD')
        //     console.log(end)
        //     $(this.$el).fullCalendar('changeView', 'agenda',{start: start, end: end});
        //   }
        // }
      },
    },
    destroyed: function () {
      $(this.$el).fullCalendar('destroy');
    },
    methods: {
      formatEvents: function(events){
        var eventsObject = _.reduce(events, function(acc, events , key){
          var obj = _.map(events, function(e){
            return { 'title': e.service_name, 'start': key + 'T' + moment(key + ' ' + e.hour, 'YYYY-MM-DD hh:mm A').format('HH:mm'), 'key': key, 'identifier': e.identifier, 'available': e.available, 'total': e.available + e.reserved, 'people_total': e.people_total}
          })
          return acc.concat(obj)
        }.bind(this), [])
        return eventsObject
      },
      calculateView: function(){
        
        var since = moment(this.selectedRange[0])
        var until = moment(this.selectedRange[1])
        var range = until.diff(since,'days') + 1
        if ( range == 1){
          $(this.$el).fullCalendar('changeView', 'agendaDay', since.clone().format('YYYY-MM-DD'));
          return;
        }
        if (1 < range && range < 8){
          var start = since.clone().format('YYYY-MM-DD')
          var end = until.clone().format('YYYY-MM-DD')
          $(this.$el).fullCalendar( 'gotoDate', start)
          $(this.$el).fullCalendar('changeView', 'agendaWeek',{start: start, end: end});
          return;
        }
        var start = since.clone().format('YYYY-MM-DD')
        var end = until.clone().format('YYYY-MM-DD')
        $(this.$el).fullCalendar( 'gotoDate', start)
        $(this.$el).fullCalendar('changeView', 'month');
        return;

      }
    }
  })