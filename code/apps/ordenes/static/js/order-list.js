ELEMENT.locale(ELEMENT.lang.es)
Vue.use(VeeValidate,{fieldsBagName:'fieldsVee',errorBagName:'errorsVee', events:"input|change"})
VeeValidate.Validator.setLocale("es")
var BeatLoader = VueSpinner.BeatLoader
var app = new Vue({
  el: '#orderList',
  delimiters: ['${', '}'],
  components: {
      BeatLoader
  },
  data: {
    schedules: [],
    createOrderUrl: window.createOrderUrl,
    deleteOrderUrl: window.deleteOrderUrl,
    servicesListUrl: window.servicesListUrl,
    ordersListUrl: window.ordersListUrl,
    serviceDetailUrl: window.serviceDetailUrl,
    orderCanceledUrl: window.orderCanceledUrl,
    editOrderUrl: window.editOrderUrl,
    orderConfirmedUrl: window.orderConfirmedUrl,
    mode: 1,
    dateType: 1,
    filterType: 3,
    onlyMyOrdenes: false,
    selectedRange: '',
    search: '',
    servicesParsed: [],
    serviceSelected: null,
    since: window.today,
    loadingOrdenes: false,
    loadingServices: false,
    notCanceledOrders: [],
    canceledOrders: [],
    loadedServices: [],
    tabOrder: false,
    windowHeight: 0,
    servicesHeight: 0,
    dialogService: false,
    serviceInfo: null,
    dialogCanceled: false,
    dialogDeleted: false,
    dialogConfirmed: false,
    orderIdToCancel: null,
    orderIdToDelete: null,
    orderIdToConfirm: null,
    canceledReason: '',
    loadingCanceled: false,
    loadingDeleted: false,
    loadingConfirmed: false,
    orderParticipantsToRemove: 0,
    servicesListEmptyAfterFilter: false,
    topServiceSelected: 0,
    scroll: false
  },
  created (){
    document.getElementById('div-order').style.display = 'block';
    
  },
  watch: {
    dateType: function() {
      if (this.dateType == 1){
        console.log("entroo")
        this.selectedRange = [this.today.toDate(), this.today.toDate()]
      }
      if (this.dateType == 2){
        this.selectedRange = [this.startOfWeek.toDate(), this.endOfWeek.toDate()]
      }
      if (this.dateType == 3){
        this.selectedRange = [this.startOfMonth.toDate(), this.endOfMonth.toDate()]
      }
    },
    filterType: function() {
      this.serviceSelected = null
    },
    onlyMyOrdenes: function() {
      this.serviceSelected = null
    },
    selectedRange: function () {
      this.serviceSelected = null
      var startDate = moment(this.selectedRange[0]).startOf('date')
      var endDate = moment(this.selectedRange[1]).startOf('date')
      var diferentDate = true
      if (startDate.isSame(this.today) && endDate.isSame(this.today)){
        diferentDate = false
        this.dateType = 1
      }
      if (startDate.isSame(this.startOfWeek) && endDate.isSame(this.endOfWeek)){
        diferentDate = false
        this.dateType = 2
      }
      if (startDate.isSame(this.startOfMonth) && endDate.isSame(this.endOfMonth)){
        diferentDate = false
        this.dateType = 3
      }
      if (diferentDate == true){
        this.dateType = ''
      }
      var differenceDays = endDate.diff(startDate, 'days') + 1
      if (differenceDays > 31){
        this.$message({
          message: 'El rango máximo a seleccionar es 1 mes.',
          type: 'error'
        });
        this.selectedRange = [this.today.toDate(), this.today.toDate()]
        return;
      }
      this.loadServices(startDate.format('YYYY-MM-DD'), endDate.format('YYYY-MM-DD'))
    
    },
    search: function () {
      if (this.serviceSelected != null && this.search == ''){
        this.$nextTick(function() {
          $('#container-services').scrollTop(0)
          $('#container-services').scrollTop(this.topServiceSelected)
        })
      }
    }
  },
  computed: {
    _(){
      return _
    },
    today: function () {
      return moment(window.today)
    },
    startOfWeek: function () {
      return this.today.clone().isoWeekday('Monday')
    },
    endOfWeek: function () {
      return this.today.clone().isoWeekday('Sunday')
    },
    startOfMonth: function () {
      return this.today.clone().startOf('month').startOf('date')
    },
    endOfMonth: function () {
      return this.today.clone().endOf('month').startOf('date')
    },
    showOrders: function () {
      if (this.tabOrder == false){
        return this.notCanceledOrders
      } else {
        return this.canceledOrders
      }
    },
    servicesList: function () {
      if (this.loadedServices.schedules == undefined){
        return []
      }
      this.servicesListEmptyAfterFilter = false
      if (this.loadedServices.services_empty == false){
        this.schedules = _.orderBy(this.loadedServices.schedules, ['date'], ['asc']);
        this.schedules = _.groupBy(this.schedules, function(s) { return s.date })
        this.servicesParsed = _.mapValues(this.schedules, function(schedules) {
          var services = _.reduce(schedules, function(acc, s) { 
            // OBTENGO EL SERVICIO DEL HORARIO SELECCIONADO
            if (s.service_id == undefined){
              return acc
            }
              
            var service =  _.find(this.loadedServices.services, function(ls){
                return ls.id == s.service_id
            }.bind(this));

            var is_excluded = _.find(service.excluded_dates, function(ed){
              var isAfterSince = moment(s.date).isSameOrAfter(moment(ed[0]))
              var isBeforeUntil = moment(s.date).isSameOrBefore(moment(ed[1]))
              return isAfterSince && isBeforeUntil
            });
            if (is_excluded){
              return acc
            }
            // SE AÑADE AL SERVICIO SU HORA DE SALIDA
            var serviceWithHour = _.assign(
              _.clone(service), 
              _.clone(s), 
              {'identifier': md5(String(service.id) + s.hour + s.date)},
              {'index': moment(s.date + ' ' + s.hour, 'YYYY-MM-DD hh:mm A').valueOf()}
            )
            return acc.concat(serviceWithHour)
          }.bind(this), []);
          // FILTRO LOS SERVICIOS QUE TENGAN ORDENES MIAS
          var showMineOrAllOrders = _.filter(services, function(s) {
            if(this.onlyMyOrdenes){
              return s.has_mine == this.onlyMyOrdenes
            }
            return true
          }.bind(this))
        // FILTRO DE ACUERDO A LOS TIPOS DE FILTRO SELECCIONADOS EN TEMPLATE
          var servicesShow = _.filter(showMineOrAllOrders, function(s){
            if(this.filterType == 1){
              return s.reserved > 0
            } else if(this.filterType == 2){
              if(s.people_total == 0){
                return true
              }else{
                return s.available > 0
              }
            }
            return true
          }.bind(this));
          // SETEO MI LISTA DE SERVICIOS AL KEY 
          return _.orderBy(servicesShow, ['index'], ['asc']);
        }.bind(this));
        if (this.search != ''){
          this.search = this.search.toLowerCase().trim()
          this.servicesParsed = _.mapValues(this.servicesParsed, function(sp){
            var servicesFilter = _.reduce(sp, function(acc, s){
              var foundTitle = s.service_name.toLowerCase().indexOf(this.search) >= 0
              var foundCompany = s.service_company.toLowerCase().indexOf(this.search) >= 0

              var foundInUsers = _.find(s.users, function(u){
                var foundName = u.name.toLowerCase().indexOf(this.search) >= 0 
                var foundEmail = u.email.toLowerCase().indexOf(this.search) >= 0 
                return foundName || foundEmail
              }.bind(this))
              if (foundTitle || foundInUsers || foundCompany){
                if (foundInUsers){
                  var newService = _.assign(_.clone(s), {'foundUsers': true})
                  return acc.concat(newService)
                }
                else{
                  var newService = _.clone(s)
                  return acc.concat(newService)
                }
              }
              return acc
            }.bind(this),[]);
            return servicesFilter
          }.bind(this))
          this.servicesParsed = _.reduce(this.servicesParsed, function(acc, s, key){
            if (s.length == 0){
              return acc
            }
            acc[key] = s
            return acc
          }.bind(this), {})
          if (Object.keys(this.servicesParsed).length == 0){
            this.servicesListEmptyAfterFilter = true
          }
        }
      }
      else{
        this.servicesParsed = {}
      }
      
      
      return this.servicesParsed
    }
  },
  mounted () {
    window.addEventListener('resize', this.getWindowHeight)
    this.getWindowHeight()
    // SETEO DE LAS FECHAS PARA EL DIA DE HOY
    this.selectedRange = [this.today.toDate(), this.today.toDate()]
    moment.locale('es')
    moment.updateLocale('es',{
      months:['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
              'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
             ]
    })
  },
  methods: {
    getWindowHeight: function(event) {
      this.windowHeight = (document.documentElement.clientHeight - 150) + 'px';
      this.servicesHeight = (document.documentElement.clientHeight - 340) + 'px';
    },
    loadServices: function (since, until){
      this.loadingServices = true
      this.since = since
      axios.get(this.servicesListUrl,{
        params:{
          since: since, 
          until: until
        }
      })
      .then((response) =>{
        this.loadedServices = response.data
        this.loadingServices = false
      });
    },
    dayOfDate: function (key){
      return moment(key).format('dddd').toUpperCase()
    },
    formatDate: function(key){
      return moment(key).format('DD MMMM')
    },
    showOrdenes: function (key, identifier, modifyOrder) {
      if (this.serviceSelected == null || this.serviceSelected.identifier != identifier || modifyOrder){
        this.mode = 1
        this.loadingOrdenes = true
        if(modifyOrder != true){
          this.search = ''
          this.$nextTick(function() {
            // TO DO: CUANDO DAS CLIC AL BOTON VER ORDENES NO SE ESTA SCROLEANDO
            $('#container-services').scrollTop(0)
            this.$nextTick(function() {
              this.topServiceSelected = $('#' + identifier).parent().position().top
              this.topServiceSelected = this.topServiceSelected + $('#' + identifier).position().top
              $('#container-services').scrollTop(this.topServiceSelected)
            }.bind(this))
            
          }.bind(this))
          var services = this.servicesList[key]
          this.serviceSelected = _.clone(_.find(services, function(s) { return s.identifier == identifier }));
        }
        axios.get(this.ordersListUrl,{
          params:{
            hour: this.serviceSelected.hour_original,
            date: key,
            service_id: this.serviceSelected.id
          }
        })
        .then((response) =>{
          this.loadingOrdenes = false
          var orders = response.data.orders
          orders = _.partition(orders, function(o){ return o.canceled == false})
          this.notCanceledOrders = orders[0]
          this.canceledOrders = orders[1]
        });
      }
    },
    closeOrdenes: function () {
      this.serviceSelected = null
    },
    changeTab: function (value) {
      this.tabOrder = value
    },
    serviceDetail: function (id) {
      axios.get(this.serviceDetailUrl,{
        params:{
          id: id
        }
      })
      .then((response) =>{
        var serviceInfo = response.data.service_detail[0]
        var transformedSchedule = _.map(serviceInfo.schedule, function(s){
          var transformedHour = _.map(s.hours, function(h){
            return moment('2018-03-16 ' + h, 'YYYY-MM-DD HH:mm').subtract(5,'hours').format('hh:mm A')
          })
          var newSchedule = Object.assign(s, {'hours': transformedHour})
          return newSchedule
        })
        this.serviceInfo = serviceInfo
        this.dialogService = true

      });
    },
    reloadSchedules: function(participants){
      // ACTUALIZO LA DISPONIBILIDAD DEL SERVICIO SELECCIONADO
      var newAvailable = 0
      if (this.serviceSelected.people_total != 0){
        newAvailable = this.serviceSelected.available + this.orderParticipantsToRemove
      }
      var newReserved = this.serviceSelected.reserved - this.orderParticipantsToRemove
      this.serviceSelected = _.assign(this.serviceSelected, {'available': newAvailable}, {'reserved': newReserved})
      // ACTUALIZO LA DISPONIBILIDAD DEL SERVICIO SELECCIONADO EN LA LISTA DE SERVICIOS
      var ss = this.loadedServices.schedules
      var index = _.findIndex(ss, function(s){
        return s.service_id == this.serviceSelected.service_id && s.date == this.serviceSelected.date && s.hour == this.serviceSelected.hour
      }.bind(this));
      var schedule = ss[index]
      var tmp = _.assign(schedule, {'available': newAvailable}, {'reserved': newReserved})
      this.$set(this.loadedServices.schedules, index, tmp)
      this.orderParticipantsToRemove = 0
    },
    cancelOrder: function(id){
      this.dialogCanceled = true;
      this.orderIdToCancel = id
    },
    confirmCanceledOrder: function(){
      this.loadingCanceled = true
      axios.get(this.orderCanceledUrl,{
        params: {
          id: this.orderIdToCancel,
          note: this.canceledReason
        }
      })
      .then((response) => {
        this.dialogCanceled = false
        this.loadingCanceled = false
        this.orderParticipantsToRemove = response.data.participants_removed
        this.reloadSchedules(this.orderParticipantsToRemove)
        this.orderIdToCancel = null
        this.canceledReason = ''
        // SE RECALCULA LAS ORDENES
        this.showOrdenes(this.serviceSelected.date, this.serviceSelected.identifier, true)
        this.$message({
          message: 'La orden se cancelo exitosamente',
          type: 'success'
        });
        this.tabOrder = true
        
      });
      
    },
    getEditOrderUrl: function(id, available, reserved, people_total){
      var url = this.editOrderUrl.replace('id', id)
      url = url + '?available=' + available + '&reserved=' + reserved + '&people_total=' + people_total
      return url
    },
    deleteOrder: function(id){
      this.orderIdToDelete = id
      this.dialogDeleted = true
    },
    confirmDeleteOrder: function(){
      this.loadingDeleted = true
      axios.post(this.deleteOrderUrl,{
        orderId: this.orderIdToDelete
      })
      .then((response) => {
        this.dialogDeleted = false
        this.loadingDeleted = false
        this.orderParticipantsToRemove = response.data.participants_removed
        this.reloadSchedules(this.orderParticipantsToRemove)
        this.orderIdToDelete = null
        // SE RECALCULA LAS ORDENES
        this.showOrdenes(this.serviceSelected.date, this.serviceSelected.identifier, true)
        this.$message({
          message: 'La orden se eliminó exitosamente',
          type: 'success'
        });
      })
    },
    getCreateOrderUrl: function(service_id, date, hour, available){
      return this.createOrderUrl + "?service_id=" + service_id + "&date=" + date + "&hour=" + hour + "&available=" + available
    },
    confirmOrder: function(id){
      this.orderIdToConfirm = id
      this.dialogConfirmed = true
    },
    confirmOrderSure: function(){
      this.loadingConfirmed = true
      axios.post(this.orderConfirmedUrl,{
        id: this.orderIdToConfirm,
        hour_service: this.serviceSelected.hour,
        date_service: this.serviceSelected.date,
        name_service: this.serviceSelected.service_name
      })
      .then((response) => {
        this.loadingConfirmed = false
        this.dialogConfirmed = false
        this.orderIdToConfirm = null
        // SE RECALCULA LAS ORDENES
        this.showOrdenes(this.serviceSelected.date, this.serviceSelected.identifier, true)
        this.$message({
          message: 'La orden se confirmó exitosamente',
          type: 'success'
        });
      })
    },
    goDateNext: function(){
      var since = moment(this.selectedRange[0])
      var until = moment(this.selectedRange[1])
      var range = until.diff(since,'days') + 1
      if (range == 1){
        var start = since.clone().add(1,'days')
        this.selectedRange = [start.toDate(), start.toDate()]
        return;
      }
      if (1 < range && range < 8){
        var start = since.clone().add(1,'week')
        var end = until.clone().add(1,'week')
        this.selectedRange = [start.toDate(), end.toDate()]
        return;
      }
      var start = ''
      var end = ''
      if (since.clone().startOf('month').startOf('date').isSame(since.startOf('date')) && until.clone().endOf('month').endOf('date').isSame(until.endOf('date'))){
        start = since.clone().add(1, 'months').startOf('month')
        end = until.clone().add(1, 'months').endOf('month')
      }else{
        start = since.clone().add(1, 'months')
        end = until.clone().add(1, 'months')
      }
      this.selectedRange = [start.toDate(), end.toDate()]
      return;
    },
    goDatePreview: function(){
      var since = moment(this.selectedRange[0])
      var until = moment(this.selectedRange[1])
      var range = until.diff(since,'days') + 1
      if (range == 1){
        var start = since.clone().subtract(1,'days')
        this.selectedRange = [start.toDate(), start.toDate()]
        return;
      }
      if (1 < range && range < 8){
        var start = since.clone().subtract(1,'week')
        var end = until.clone().subtract(1,'week')
        this.selectedRange = [start.toDate(), end.toDate()]
        return;
      }
      var start = ''
      var end = ''
      if (since.clone().startOf('month').startOf('date').isSame(since.startOf('date')) && until.clone().endOf('month').endOf('date').isSame(until.endOf('date'))){
        start = since.clone().subtract(1, 'months').startOf('month')
        end = until.clone().subtract(1, 'months').endOf('month')
      }else{
        start = since.clone().subtract(1, 'months')
        end = until.clone().subtract(1, 'months')
      }
      this.selectedRange = [start.toDate(), end.toDate()]
      return;
    }
  }
})
