from functools import reduce
import datetime
import arrow
import pytz
from django.template.loader import render_to_string
from django.core.mail import send_mail
from apps.ordenes.models import Order, OrderPrice
from apps.configuracion.models import Notification
from jinja2 import Environment, BaseLoader
from django.conf import settings
from django.utils.timezone import utc
from apps.agentes.models import AgentInvite, Agent
from apps.usuarios.models import Company
def explicit_schedule(schedules, since, days):
    utc = pytz.timezone('UTC')

    def format_schedule(schedule):
        dict_dates = {}
        def set_date_schedule(acc, day):
            date = arrow.get(since).shift(days=+day)
            day_of_week = date.isoweekday()
            date_string = arrow.get(date).format('YYYY-MM-DD')
            schedule_since = arrow.get(schedule['since']).replace(tzinfo='UTC').shift(hours=-5)
            schedule_until = arrow.get(schedule['until']).replace(tzinfo='UTC').shift(hours=-5)
            if day_of_week == schedule_day_of_week and \
                date >= schedule_since and date <= schedule_until:
                s = schedule.copy()
                s['date'] = date_string
                dict_dates[date_string] = date_string
                acc.append(s)
            else:
                if date_string not in dict_dates:
                    s = {}
                    s['date'] = date_string
                    acc.append(s)
            return acc

        schedule_day_of_week = schedule['day']

        return reduce(set_date_schedule, range(days), [])
    schedules_formated = map(format_schedule, schedules)
    schedules_formated = [s for l in schedules_formated for s in l]
    return schedules_formated

def send_confirm_order_email(order, user, date_service, hour_service, name_service, email_language, order_date):
    payment_total = order.service.currency.symbol + '{:.2f}'.format(order.price_total)
    duration = order.service.duration
    data = ''
    if user.type_user.id == 3:
        invitation = AgentInvite.objects.get(agent=user, operator=order.service.user)
        #  Si soy agente interno
        if invitation.agent_intern:
            # Para poder utilizar la configuración de correo del operador
            user = order.service.user
    noti_exists = Notification.objects.filter(user=user).exists()
    if noti_exists:
        noti = Notification.objects.get(user=user)
        info = noti.info if email_language == 'es' else noti.english_info
        rtemplate = Environment(loader=BaseLoader).from_string(info)
        data = rtemplate.render({
                                'nombre_cliente': order.firstname, 
                                'apellidos_cliente': order.lastname, 
                                'monto_total': payment_total, 
                                'duración_servicio': duration, 
                                'hora_servicio': hour_service, 
                                'fecha_servicio': date_service, 
                                'nombre_servicio': name_service})

    order_prices = list(OrderPrice.objects.filter(order=order))
    prices = []
    for order_price in order_prices:
        dic = {}
        dic['quantity'] = order_price.quantity
        price_total = order_price.price * order_price.quantity
        price_total = order.service.currency.symbol + '{:.2f}'.format(price_total)
        if order_price.name == 'Grupo':
            name = 'Grupo [' + str(order_price.min_people) + "-" + str(order_price.max_people) + "]"
            price = order.service.currency.symbol + '{:.2f}'.format(order_price.price) + "/por persona"
            if order_price.group_applied_to == 2:
                price = order.service.currency.symbol + '{:.2f}'.format(order_price.price) + "/por grupo" 
                price_total = order.service.currency.symbol + '{:.2f}'.format(order_price.price) 
        else:
            name = order_price.name
            price = order.service.currency.symbol + '{:.2f}'.format(order_price.price)
        
        dic['name'] = name
        dic['price'] = price
        dic['price_total'] = price_total
        prices.append(dic)
            
    
    if email_language == 'es':
        subject = "Confirmación de Orden"
    else:
        subject = "Order Confirmation"

    if user.type_user.id == 1 or user.type_user.id == 2:
        company = Company.objects.get(users=user)
        company_info = {
                'name': company.name.capitalize(),
                'url': company.company_web_url,
                'phone': company.phone if company.phone else "",
                'address': company.address if company.address else "",
                'email': company.email if company.email else "",
                'ruc': company.ruc if company.ruc else "",
            }
    else:
        agent = Agent.objects.get(user=user)
        agent_invite = AgentInvite.objects.get(agent=user, operator=order.service.user)
        if agent_invite.agent_intern:
            company = Company.objects.get(users=order.service.user)
            company_info = {
                    'name': company.name.capitalize(),
                    'url': company.company_web_url,
                    'phone': company.phone if company.phone else "",
                    'address': company.address if company.address else "",
                    'email': company.email if company.email else "",
                    'ruc': company.ruc if company.ruc else "",
                }
        else:
            if agent.is_company:
                company_info = {
                    'name': agent.name if agent.name else "",
                    'ruc': agent.ruc if agent.ruc else "",
                    'phone': agent.phone if agent.phone else "",
                    'email': agent.email if agent.email else "",
                    'url': agent.url if agent.url else "",
                    'address': agent.address if agent.address else "",
                }
            else:
                company_info = {
                    'name': user.first_name.title() + " " + user.last_name.title(),
                    'ruc': "",
                    'phone': "",
                    'email': user.email,
                    'url': "",
                    'address': "",
                }


    email_template = render_to_string(
        "emails/confirm_order_{}.html".format(email_language),
        {
            'info': data,
            'order': order,
            'client': order,
            'order_date': order_date,
            'company': company_info,
            'payment_total': payment_total,
            'service_date': date_service,
            'service_hour': hour_service,
            'service_name': name_service,
            'prices': prices
        }
    )
    send_mail(
        subject,
        'Texto plano',
        settings.DEFAULT_FROM_EMAIL,
        [order.email],
        fail_silently=False,
        html_message=email_template,
    )
    