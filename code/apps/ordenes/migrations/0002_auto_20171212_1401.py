# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-12 19:01
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('ordenes', '0001_initial'),
        ('oficinas', '0001_initial'),
        ('canales', '0001_initial'),
        ('servicios', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderprice',
            name='service_price',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='servicios.ServicePrice'),
        ),
        migrations.AddField(
            model_name='order',
            name='distribution_channel',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='canales.DistributionChannel', verbose_name='Canal de distribución'),
        ),
        migrations.AddField(
            model_name='order',
            name='office',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='oficinas.Office', verbose_name='Oficina'),
        ),
        migrations.AddField(
            model_name='order',
            name='payment_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='ordenes.PaymentType'),
        ),
        migrations.AddField(
            model_name='order',
            name='service',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='servicios.Service', verbose_name='Servicio'),
        ),
    ]
