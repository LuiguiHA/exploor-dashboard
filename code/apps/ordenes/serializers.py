from rest_framework import serializers
from apps.ordenes.models import PaymentType
from apps.servicios.models import Service, ServiceExcludedDates, ServiceSchedule, ServicePrice
from apps.canales.models import DistributionChannel
from .models import Order, ParticipantsOrderPrice, OrderPrice

from datetime import datetime, timedelta
import pytz

class OrderSerializer(serializers.Serializer):

	service = serializers.IntegerField()
	name = serializers.CharField(max_length=100)
	last_name = serializers.CharField(max_length=100)
	phone = serializers.CharField(max_length=20)
	email = serializers.EmailField()
	selected_prices = serializers.ListField()
	payment_type = serializers.IntegerField()
	reference = serializers.CharField(max_length=6)
	selected_date = serializers.CharField()
	selected_hour = serializers.CharField()
	additional_information = serializers.CharField(required=False)
	channel_id = serializers.IntegerField(required=False)

	def __init__(self, *args, **kwargs):
		super(OrderSerializer, self).__init__(*args, **kwargs)
		self.fields["name"].error_messages["required"] = "This field is required"
		self.fields["name"].error_messages["null"] = "This field can't be null"
		self.fields["name"].error_messages["blank"] = "This field can't be empty"
		self.fields["name"].error_messages["max_length"] = "This field doesn't allow more than {max_length} characters"

		self.fields["last_name"].error_messages["required"] = "This field is required"
		self.fields["last_name"].error_messages["null"] = "This field can't be null"
		self.fields["last_name"].error_messages["blank"] = "This field can't be empty"
		self.fields["last_name"].error_messages["max_length"] = "This field doesn't allow more than {max_length} characters"

		self.fields["phone"].error_messages["required"] = "This field is required"
		self.fields["phone"].error_messages["null"] = "This field can't be null"
		self.fields["phone"].error_messages["blank"] = "This field can't be empty"
		self.fields["phone"].error_messages["max_length"] = "This field doesn't allow more than {max_length} characters"
		self.fields["email"].error_messages["required"] = "This field is required"
		self.fields["email"].error_messages["null"] = "This field can't be null"
		self.fields["email"].error_messages["blank"] = "This field can't be empty"
		self.fields["email"].error_messages["invalid"] = "Enter a valid email"
		
		self.fields["selected_prices"].error_messages["required"] = "This field is required"
		self.fields["selected_prices"].error_messages["null"] = "This field can't be null"
		self.fields["selected_prices"].error_messages["not_a_list"] = "This field must be a list"

		self.fields["payment_type"].error_messages["required"] = "This field is required"
		self.fields["payment_type"].error_messages["null"] = "This field can't be null"
		self.fields["payment_type"].error_messages["invalid"] = "Enter a int number valid"

		self.fields["reference"].error_messages["required"] = "This field is required"
		self.fields["reference"].error_messages["null"] = "This field can't be null"
		self.fields["reference"].error_messages["blank"] = "This field can't be empty"
		self.fields["reference"].error_messages["max_length"] = "This field doesn't allow more than {max_length} characters"

		self.fields["selected_date"].error_messages["required"] = "This field is required"
		self.fields["selected_date"].error_messages["null"] = "This field can't be null"
		self.fields["selected_date"].error_messages["blank"] = "This field can't be empty"

		self.fields["selected_hour"].error_messages["required"] = "This field is required"
		self.fields["selected_hour"].error_messages["null"] = "This field can't be null"
		self.fields["selected_hour"].error_messages["blank"] = "This field can't be empty"

		self.fields["additional_information"].error_messages["null"] = "This field can't be null"
		self.fields["additional_information"].error_messages["blank"] = "This field can't be empty"

	def validate_payment_type(self, value):
		payment_types = PaymentType.objects.all()
		payment_type_ids = [payment_type.id for payment_type in payment_types]

		if value in payment_type_ids:
			return value
		else:
			raise serializers.ValidationError('Payment type is not valid')

	def validate_selected_date(self,value):
		service_id = self.initial_data["service"]
		service = Service.objects.get(id=service_id)
		excluded_dates = ServiceExcludedDates.objects.filter(service=service)

		try:
			value_date = datetime.strptime(value,'%Y-%m-%d')
			value_date = value_date.replace(tzinfo=pytz.utc) + timedelta(hours=5)
		except:
			raise serializers.ValidationError('Date format is not valid')

		if value_date >= service.since and value_date <= service.until:
			for excluded_date in excluded_dates:
				if value_date >= excluded_date.since and value_date <= excluded_date.until:
					raise serializers.ValidationError('Select_date is not valid')

			return value_date
		else:
			raise serializers.ValidationError('Select_date out of date')

	def validate_selected_hour(self,value):
		service_id = self.initial_data["service"]
		service = Service.objects.get(id=service_id)
		selected_date = self.initial_data["selected_date"]
		week_day = datetime.strptime(selected_date,'%Y-%m-%d').weekday() + 1
		schedule_list = ServiceSchedule.objects.filter(service=service,day=week_day)
		is_valid = False

		try:
			hour = datetime.strptime(value, '%H:%M').time()
			hour_string = hour.strftime('%H:%M')
		except:
			raise serializers.ValidationError('Hour format is not valid')

		for schedule in schedule_list:
			schedule_hour = schedule.hour.strftime('%H:%M')
			if hour_string == schedule_hour:
				is_valid = True
				break

		if is_valid:
			return hour
		else:
			raise serializers.ValidationError('Selected_hour is not valid')

	def validate_selected_prices(self,value):
		prices_list = value
		service_id = self.initial_data["service"]
		service = Service.objects.get(id=service_id)
		service_prices = ServicePrice.objects.filter(service=service,enabled=True)
		service_price_ids = [service_price.id for service_price in service_prices]

		if len(prices_list) > 0:
			for price in prices_list:
				# VALIDATE PRICE ID
				if len(price.keys()) != 3:
					raise serializers.ValidationError('Expected dict with fields price_id, quantity and participants')

				if price.get("price_id"):
					try:
						price_id = price.get("price_id")
						price_id = int(price_id)
					except:
						raise serializers.ValidationError('Expected number in price_id')
				else:
					raise serializers.ValidationError('Missing field: price_id')

				if not price_id in service_price_ids:
					raise serializers.ValidationError('Price_id invalid')
				
				#VALIDATE QUANTITY
				quantity = price.get("quantity")
				if quantity == None:
					raise serializers.ValidationError('Missing field: quantity')
				
				quantity_is_number = isinstance(quantity,int)
				if not quantity_is_number:
					raise serializers.ValidationError('Quantity field must be a number')
				if service.max_people == 0:
					if quantity < service.min_people:
						raise serializers.ValidationError('Quantity is less than the minimum')
				elif quantity < service.min_people or quantity > service.max_people:
					raise serializers.ValidationError('Quantity is not in the available range')

				service_price = ServicePrice.objects.get(id=price_id)
				if service_price.category.name == 'Grupo':
					if quantity < service_price.min_people or quantity > service_price.max_people:
						raise serializers.ValidationError( \
							  'Quantity is not in the available range of price type')

				#VALIDATE PARTICIPANTS
				participants = price.get("participants")
				if participants == None:
					raise serializers.ValidationError('Missing field: participants')

				participants_is_list = isinstance(participants,list)
				if not participants_is_list:
					raise serializers.ValidationError('Participants field must be a list')

				if len(participants) > 0:
					for participant in participants:
						if not isinstance(participant,dict):
							raise serializers.ValidationError( \
											'Participants information must be a dict')
						if not "name" in participant or not "last_name" in participant:
							raise serializers.ValidationError( \
											'Missing fields of participants')
						if not isinstance(participant.get("name"),str) or \
										not isinstance(participant.get("last_name"),str):
							raise serializers.ValidationError( \
												'The fields name and last_name must be string')
						if len(participant.get("name")) == 0 or \
						   len(participant.get("last_name")) == 0:
						   raise serializers.ValidationError( \
									'Fields of participants empty')
		else:
			raise serializers.ValidationError('You must select at least one price')

		return prices_list

	def create(self,validated_data):
		additional_information = None
		if validated_data.get("additional_information"):
			additional_information = validated_data.get("additional_information")
		reference = validated_data.get("reference").upper()
		channel = DistributionChannel.objects.get(id=validated_data["channel_id"])
		payment_type = PaymentType.objects.get(id=validated_data["payment_type"])
		service = Service.objects.get(id=validated_data["service"])
		prices_list = validated_data["selected_prices"]
		
		order = Order.objects.create(distribution_channel=channel,
									firstname=validated_data["name"],
									lastname=validated_data["last_name"],
									email=validated_data["email"],
									info_additional=additional_information,
									mobile_customer=validated_data["phone"],
									reference=reference,
									payment_type=payment_type,
									service=service,
									out_date=validated_data["selected_date"],
									hour=validated_data["selected_hour"],
									price_total=0)
		order.save()

		total_price = 0
		for price in prices_list:
			service_price = ServicePrice.objects.get(id=price.get("price_id"))
			order_price = OrderPrice.objects.create(name=service_price.category.name,
													service_price=service_price,
													quantity=price.get("quantity"),
													price=service_price.price,
													group_applied_to=service_price.group_price_applied_to,
													min_people=service_price.min_people,
													max_people=service_price.max_people,
													order=order)
			order_price.save()

			if order_price.name == "Grupo" and order_price.group_applied_to == 2:
				total_price = total_price + order_price.price
			else:
				total_price = total_price + order_price.price * order_price.quantity

			participants_list = price.get("participants")
			for participant in participants_list:
				order_participant = ParticipantsOrderPrice. \
									objects.create(firstname=participant.get("name"),
										          lastname=participant.get("last_name"),
										          order_price=order_price)
				order_participant.save()

		order.price_total = total_price
		order.save()

		return order

#OBJECTS AND SERIALIZERS PERSONALIZER
class OrderResponseSerializer(serializers.Serializer):
	order = serializers.DictField()
class OrderResponse(object):
	def __init__(self, order):
		order_dic = {}
		order_dic["service"] = order.service.id

		selected_date = order.out_date.strftime('%Y-%m-%d')
		order_dic["selected_date"] = selected_date

		selected_hour = order.hour.strftime('%H:%M')
		order_dic["selected_hour"] = selected_hour

		service = Service.objects.get(id=order.service.id)
		total_price = service.currency.symbol + '{:.2f}'.format(order.price_total)
		order_dic["total_price"] = total_price
		
		self.order = order_dic









