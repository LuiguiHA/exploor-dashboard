from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE
from apps.canales.models import DistributionChannel
from apps.usuarios.models import User
from apps.oficinas.models import Office
from apps.servicios.models import Service, PriceCategory, ServicePrice
from apps.servicios.models import DeletedObjectManager
import datetime
import arrow
from dateutil import tz
import locale

# Create your models here.


class PaymentType(SafeDeleteModel):
    objects = DeletedObjectManager()

    name = models.CharField(
        max_length=50
    )


class Order(SafeDeleteModel):
    distribution_channel = models.ForeignKey(
        DistributionChannel,
        on_delete=models.DO_NOTHING,
        null=True,
        verbose_name="Canal de distribución"
    )

    user_assigned = models.ForeignKey(
        'usuarios.User',
        null=True,
        verbose_name="Oficina",
        on_delete=models.DO_NOTHING
    )

    firstname = models.CharField(
        max_length=50,
        verbose_name="Nombre"
    )

    email = models.CharField(
        max_length=50
    )
    lastname = models.CharField(
        max_length=50,
        verbose_name="Apellido"
    )

    client_country = models.ForeignKey(
        'usuarios.Country', 
        null=True
    )

    info_additional = models.TextField(
        null=True
    )

    mobile_customer = models.CharField(
        max_length=50,
        verbose_name="Móvil"
    )

    reference = models.CharField(
        max_length=50,
        verbose_name="Referencia"
    )

    payment_type = models.ForeignKey(
        PaymentType,
        on_delete=models.DO_NOTHING
    )

    service = models.ForeignKey(
        Service,
        verbose_name="Servicio",
        on_delete=models.DO_NOTHING
    )

    out_date = models.DateTimeField(
        verbose_name="Fecha de salida"
    )

    hour = models.TimeField(
        verbose_name="Hora"
    )

    price_total = models.IntegerField()

    creator = models.ForeignKey(
        User,
        null=True,
        related_name="ordenes"
    )

    reserved_quantity = models.IntegerField(
        null=True
    )

    created_at = models.DateTimeField(
       auto_now_add=True,
       editable=False,
       verbose_name="Fecha de creación"
    )

    canceled = models.BooleanField(
        default=False
    )

    confirmed = models.BooleanField(
        default=False
    )

    canceled_reason = models.TextField(
        null=True
    )

    email_confirm_send = models.BooleanField(
        default=False
    )

    updated_at = models.DateTimeField(
       auto_now=True,
       editable=False
    )

    class Meta:
        verbose_name = "Orden"
        verbose_name_plural = "Órdenes"

    def service_title(self):
        return self.service.title
    service_title.short_description = "Servicio"

    def set_hour(self):
        date = datetime.datetime.now()
        date = date.replace(
            hour=self.hour.hour,
            minute=self.hour.minute,
            second=self.hour.second
        )
        hour = arrow.get(date, tz.gettz('America/Lima')).shift(hours=-5).format('hh:mm a')
        return hour
    set_hour.short_description = "Hora"

    def set_out_date(self):
        months_array = ["Enero", "Febrero", "Marzo",
                        "Abril", "Mayo", "Junio", "Julio",
                        "Agosto", "Septiembre", "Octubre",
                        "Noviembre", "Diciembre"]
        month_string = months_array[self.out_date.month - 1]
        out_date = self.out_date.strftime("%d de " + month_string + " de %Y")
        return out_date
    set_out_date.short_description = "Fecha de salida"

    def company_name(self):
        companies = self.creator.company_set.all()
        return ','.join([company.name.capitalize() for company in companies])
    company_name.short_description = "Empresa"

    def client_name(self):
        return self.firstname.title() + ' ' + self.lastname.title()
    client_name.short_description = "Nombre de Cliente"

    def __str__(self):
        return str(self.id)


class OrderPrice(SafeDeleteModel):
    name = models.CharField(
        max_length=50
    )
    service_price = models.ForeignKey(
        ServicePrice,
        on_delete=models.DO_NOTHING
    )
    quantity = models.IntegerField()

    price = models.FloatField()

    group_applied_to = models.IntegerField(
        null=True
    )

    min_people = models.IntegerField(
        null=True
    )

    max_people = models.IntegerField(
        null=True
    )

    order = models.ForeignKey(
        Order,
        on_delete=models.DO_NOTHING
    )

    def __str__(self):
        return self.service_price.category.name


class ParticipantsOrderPrice(SafeDeleteModel):
    firstname = models.CharField(
        max_length=50
    )

    lastname = models.CharField(
        max_length=50
    )

    order_price = models.ForeignKey(
        OrderPrice,
        on_delete=models.DO_NOTHING

    )


class Currency(SafeDeleteModel):
    name = models.CharField(
        max_length=50
    )

    symbol = models.CharField(
        max_length=5
    )

    code = models.CharField(
        max_length=5
    )

class LanguageCountryForOrder(SafeDeleteModel):
    language = models.CharField(
        max_length=50
    )

    country = models.ForeignKey(
        'usuarios.Country', 
        null=True
    )