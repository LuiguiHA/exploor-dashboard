from django.contrib import admin
from django.db.models import Sum
from django.conf.urls import url
from django.urls import reverse
from django.core import serializers
from django.shortcuts import redirect
from django.db.models import CharField, Value as V
from django.db.models.functions import Concat
from django.db.models import F
from .models import Order, PaymentType, OrderPrice, ParticipantsOrderPrice
from apps.servicios.models import Service, ServicePrice, FieldsHelp, PriceCategory
from apps.usuarios.models  import Company, Country, User
from apps.canales.models import DistributionChannel
# from apps.oficinas.models import Office
from .views import RegisterOrderView, DeleteOrderView, ServicesListView, OrdersListView, ServiceOrderDetailView, ServiceDetailPdfView, OrderCanceledView, OrderConfirmedView, ServiceCapacityView
import json
import jsonpickle
import arrow
from datetime import timedelta, tzinfo
import datetime
from dateutil import tz
import base64
from hashids import Hashids
import random
# Register your models here.


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = [
        "service_title", "created_at", "firstname", "lastname",
        "mobile_customer", "reference", "set_out_date", "set_hour",
        "company_name", "user_assigned", "distribution_channel"]

    search_fields = [
        "service__title", "reference", "firstname", "user_assigned__name",
        "distribution_channel__name", "service__company__name"]

    def has_add_permission(self, request):
        # services_quantity = len(Service.objects.filter(user__company__in=request.user.company_set.all()))
        # if request.user.is_superuser or services_quantity == 0:
        #     return False
        # else:
        #     return True
        return True

    def get_queryset(self, request):
        qs = super(OrderAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(
            service__user__company__in=request.user.company_set.all())

    def get_list_display(self, request):
        if request.user.is_superuser:
            return self.list_display
        else:
            return ["service_title","created_at", "firstname","lastname","mobile_customer","reference","set_out_date","set_hour","user_assigned", "distribution_channel"]

    def get_search_fields(self,request):
        if request.user.is_superuser:
            return self.search_fields
        else:
            return ["service__title", "reference", "firstname","user_assigned__name","distribution_channel__name"]

    def changelist_view(self, request, extra_context=None):
        today = arrow.utcnow()
        today = today.to('America/Lima').format('YYYY-MM-DD')
        context = {
            'today': today
        }
        return super(OrderAdmin, self).changelist_view(request, extra_context=context)
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        def datetime_serializer(obj):
                """Default JSON serializer."""
                import calendar, datetime

                if isinstance(obj, datetime.datetime):
                    if obj.utcoffset() is not None:
                        obj = obj - obj.utcoffset()
                    millis = int(
                        calendar.timegm(obj.timetuple()) * 1000 +
                        obj.microsecond / 1000
                    )
                    return millis
                raise TypeError('Not sure how to serialize %s' % (obj,))

        def get_excluded_dates(excluded_date):
                since = arrow.get(excluded_date.since).format('YYYY-MM-DD HH:mm:ss ZZ')
                until = arrow.get(excluded_date.until).format('YYYY-MM-DD HH:mm:ss ZZ')
                repeat = excluded_date.repeat_every_year
                return {'since':since, 'until':until, 'repeat':repeat}

        def get_schedule(schedule):
                import datetime
                date = datetime.datetime.now()
                date = date.replace(
                    hour=schedule.hour.hour,
                    minute=schedule.hour.minute,
                    second=schedule.hour.second
                )
                return {
                    "day": schedule.day,
                    "hour": arrow.get(
                        date, tz.gettz('America/Lima')
                    ).shift(hours=-5).format('hh:mm A')
                }

        hashids = Hashids(min_length=6)
        payment_type_list = list(PaymentType.objects.all()\
            .annotate(
                value=F('id'),
                label=F('name')
            )\
            .extra(select={'deleted':False})\
            .values("value", "label", "deleted"))

        distribution_channel_list = list(DistributionChannel.objects.all()\
            .annotate(
                value=F('id'),
                label=F('name')
            )\
            .extra(select={'deleted':False})\
            .values("value", "label", "deleted"))

        fields_help = list(FieldsHelp.objects.all().values('field_name','help_text'))

        if request.user.can_set_point_of_sale():
            company_agents = list(request.user.get_operator_company_users()\
                .annotate(
                    name=Concat('first_name', V(' ') ,'last_name'),
                    value=F('id'),
                    label=F('name')
                )\
                .extra(select={'deleted':False})\
                .values("value", "label", "deleted"))
        else:
            company_agents = []

        date = datetime.datetime.now()
        info_order = None
        service_data = None
        email_send = False
        id_order = 0
        reference_code = hashids.encode(random.randrange(0, 101, 2)).upper()
        data = request.GET

        # CREAR ORDEN
        if object_id == None:
            service_id = data.get('service_id')
            service = Service.objects.get(id=service_id)
            service_data = {}
            service_data["deleted"] = False
            service_data["out_date"] = data.get('date')
            service_data["hour"] = data.get('hour')
            service_data["available"] = data.get('available')
            service_data["title"] = service.title
            service_data["id"] = service.id
            service_data["since"] = datetime_serializer(service.since)
            service_data["until"] = datetime_serializer(service.until)
            service_data["min_people"] = service.min_people
            service_data["max_people"] = service.max_people
            service_data["category"] = service.category.name
            service_data["currency"] = service.currency.symbol
            service_data["category"] = service.category.name
            # hour = arrow.get('2018-04-02 ' + data.get('hour'), 'YYYY-MM-DD hh:mm A').shift(hours=+5).format('HH:mm')
            # reserved_quantity = Order.objects.filter(service__id=service.id, 
            #                     out_date=data.get('date'),hour=hour,canceled=False).aggregate(Sum('reserved_quantity'))
            # reserved_quantity = reserved_quantity['reserved_quantity__sum']
            # print(reserved_quantity)
            service_data['prices'] = \
                    list(
                        service.prices\
                            .filter(enabled=True)\
                            .values(
                                "id", "price", "min_people", "max_people",
                                "group_price_applied_to", "category__name",
                                "category__id","last_price","category__fields_disabled"
                            )
                    )
            service_data["excluded_dates"] = \
                    list(
                        service.excluded_dates\
                            .values("since", "until")
                    )
            list_schedule = service.schedule.all()
            list_schedule = [get_schedule(schedule) for schedule in list_schedule ]
            service_data["schedule"] = list_schedule
        # CUANDO VAS A EDITAR UNA ORDEN
        else:
            info_order = {}
            id_order = object_id
            order = Order.objects.get(id=int(object_id))
            email_send = order.email_confirm_send
            reference_code = order.reference
            info_order["firstname"] = order.firstname
            info_order["lastname"] = order.lastname
            info_order["info_additional"] = order.info_additional
            info_order["mobile_customer"] = order.mobile_customer
            info_order["reference"] = order.reference
            info_order["email"] = order.email
            info_order['confirmed'] = order.confirmed
            if int(data.get('available')) == 0 and int(data.get('people_total')) == 0:
                info_order['available'] = 0
            else:
                info_order['available'] = int(data.get('available')) + int(data.get('reserved'))
            print(info_order['available'])
            info_order["out_date"] = arrow.get(order.out_date).format('YYYY-MM-DD HH:mm:ss ZZ')
            date = date.replace(
                hour=order.hour.hour,
                minute=order.hour.minute,
                second=order.hour.second
                )
            info_order["hour"] = arrow.get(date, tz.gettz('America/Lima'))\
                                    .shift(hours=-5).format('hh:mm A')
            info_order["price_total"] = order.price_total

            if order.distribution_channel:
                distribution_channel = DistributionChannel.objects.get(pk=order.distribution_channel.id)
                # VALIDAR SI EL CANAL DE DISTRIBUCIÓN ESTA SELECCIONADO Y ELIMINADO
                if distribution_channel.deleted:
                    dic = {}
                    dic["id"] = distribution_channel.id
                    dic["deleted"] = True
                    dic["name"] = distribution_channel.name + "  (Canal eliminado)"
                    distribution_channel_list.append(dic)

                info_order["distribution_channel"] = distribution_channel.name
                info_order["distribution_channel_id"] = distribution_channel.id
                info_order["user_assigned"] = None
                info_order["user_assigned_id"] = None
            elif order.user_assigned:
                user_assigned = User.objects.get(pk=order.user_assigned.id)
                # VALIDAR SI LA OFICINA ESTA SELECCIONADO Y ELIMINADO
                if user_assigned.deleted:
                    dic = {}
                    dic["id"] = user_assigned.id
                    dic["deleted"] = True
                    dic["name"] = "{} (Usuario eliminado)".format(user_assigned.full_name)
                    user_assigned_list.append(dic)

                info_order["user_assigned"] = user_assigned.full_name
                info_order["user_assigned_id"] = user_assigned.id
                info_order["distribution_channel"] = None
                info_order["distribution_channel_id"] = None

            info_order["payment_type"] = order.payment_type.id
            info_order["client_country"] = order.client_country.id
            # VALIDAR SI EL TIPO DE PAGO ESTA SELECCIONADO Y ELIMINADO
            payment_type_deleted = PaymentType.objects.get(pk=order.payment_type.id)
            if payment_type_deleted.deleted:
                dic = {}
                dic["id"] = payment_type_deleted.id
                dic["deleted"] = True
                dic["name"] = payment_type_deleted.name + "  (Modo de pago eliminado)"
                payment_type_list.append(dic)

            service = order.service
            service_item = {}
            if service.deleted:
                service_item["deleted"] = True
                service_item["title"] = service.title + " (Servicio eliminado)"
            else:
                service_item["deleted"] = False
                service_item["title"] = service.title
            
            service_item["id"] = service.id
            service_item["since"] = datetime_serializer(service.since)
            service_item["until"] = datetime_serializer(service.until)
            service_item["min_people"] = service.min_people
            service_item["max_people"] = service.max_people
            # service_item['available'] = 
            service_item["category"] = service.category.name
            service_item["currency"] = service.currency.symbol

            order_prices = OrderPrice.objects.filter(order=order)
            order_prices_list = []
            order_service_price_ids = []
            for order_price in order_prices:
                dic = {}
                dic["name"] = order_price.name
                dic["quantity"] = order_price.quantity
                dic["min_people"] = order_price.min_people
                dic["max_people"] = order_price.max_people
                dic["group_price_applied_to"] = order_price.group_applied_to
                dic["price"] = order_price.price
                participants = ParticipantsOrderPrice.objects.filter(order_price=order_price)
                participants_list = []
                for participant in participants:
                    participant_dic = {}
                    participant_dic["firstName"] = participant.firstname
                    participant_dic["lastName"] = participant.lastname
                    participants_list.append(participant_dic)
                dic["participants"] = participants_list
                dic["service_price_id"] = order_price.service_price.id
                order_service_price_ids.append(order_price.service_price.id)
                order_prices_list.append(dic)


            service_prices = list(ServicePrice.objects.select_related("category").filter(service=service))
            service_prices_list = []
            service_price_ids = []
            for service_price in service_prices:
                service_price_dic =  {}
                if service_price.id in order_service_price_ids:

                    for order_price in order_prices_list:

                        if order_price["service_price_id"] == service_price.id:
                            service_price_ids.append(service_price.id)
                            service_price_dic["id"] = service_price.id
                            service_price_dic["quantity"] = order_price["quantity"]
                            service_price_dic["min_people"] = order_price["min_people"]
                            service_price_dic["max_people"] = order_price["max_people"]
                            service_price_dic["group_price_applied_to"] = order_price["group_price_applied_to"]
                            service_price_dic["participants"] = order_price["participants"]
                            service_price_dic["enabled"] = True
                            service_price_dic["deleted"] = False
                            service_price_dic["category__name"] = service_price._category_cache.name
                            service_price_dic["category__id"] = service_price._category_cache.id
                            service_price_dic["category__fields_disabled"] = service_price._category_cache.fields_disabled
                            service_price_dic["price"] = order_price["price"]
                            service_price_dic["update_price"] = None
                            service_price_dic["update"] = False
                            service_price_dic["enabledupdate"] = True
                            service_price_dic["update_max_people"] = None
                            service_price_dic["update_min_people"] = None
                            service_price_dic["update_group_applied"] = None
                            if service_price.enabled:
                                if service_price.price != order_price["price"]:
                                    service_price_dic["update_price"] = service_price.price
                                    service_price_dic["enabledupdate"] = False
                                    service_price_dic["update"] = True

                                if order_price["name"] == 'Grupo':
                                    if service_price.max_people != order_price["max_people"]:
                                        service_price_dic["update"] = True
                                        service_price_dic["update_max_people"] = service_price.max_people
                                        service_price_dic["enabledupdate"] = False
                                    if service_price.min_people != order_price["min_people"]:
                                        service_price_dic["update"] = True
                                        service_price_dic["update_min_people"] = service_price.min_people
                                        service_price_dic["enabledupdate"] = False
                                    if service_price.group_price_applied_to != order_price["group_price_applied_to"]:
                                        service_price_dic["update"] = True
                                        service_price_dic["update_group_applied"] = service_price.group_price_applied_to
                                        service_price_dic["enabledupdate"] = False
                            else:
                                service_price_dic["enabled"] = True
                                service_price_dic["deleted"] = True

                            service_prices_list.append(service_price_dic)

                else:
                    if service_price.enabled: # si esta enabled y no esta en order se tiene que pintar
                        service_price_ids.append(service_price.id)
                        service_price_dic["deleted"] = False
                        service_price_dic["enabled"] = False
                        service_price_dic["id"] = service_price.id
                        service_price_dic["quantity"] = None
                        service_price_dic["min_people"] = service_price.min_people
                        service_price_dic["max_people"] = service_price.max_people
                        service_price_dic["group_price_applied_to"] = service_price.group_price_applied_to
                        service_price_dic["participants"] = None
                        service_price_dic["price"] = service_price.price
                        service_price_dic["update"] = False
                        service_price_dic["enabledupdate"] = True
                        service_price_dic["update_max_people"] = None
                        service_price_dic["update_min_people"] = None
                        service_price_dic["update_group_applied"] = None
                        service_price_dic["update_price"] = None
                        service_price_dic["enabledupdate"] = False
                        service_price_dic["category__name"] = service_price._category_cache.name
                        service_price_dic["category__id"] = service_price._category_cache.id
                        service_price_dic["category__fields_disabled"] = service_price._category_cache.fields_disabled

                        service_prices_list.append(service_price_dic)
            # PARA GRUPOS ELIMINADOS
            service_price_deleted_ids = list(set(order_service_price_ids) - set(service_price_ids))
            for service_id in  service_price_deleted_ids:
                service_price_deleted = {}
                service_price = ServicePrice.objects.select_related("category").get(pk=service_id)
                order_instance = OrderPrice.objects.get(order=order,service_price=service_price)
                service_price_deleted["id"] = service_price.id
                service_price_deleted["price"] = order_instance.price
                service_price_deleted["udpdate_price"] = None
                service_price_deleted["enabledupdate"] = False
                service_price_deleted["min_people"] = order_instance.min_people
                service_price_deleted["max_people"] = order_instance.max_people
                service_price_deleted["group_price_applied_to"] = order_instance.group_applied_to
                service_price_deleted["category__name"] = service_price._category_cache.name
                service_price_deleted["category__id"] = service_price._category_cache.id
                service_price_deleted["category__fields_disabled"] = service_price._category_cache.fields_disabled
                service_price_deleted["deleted"] = True
                service_price_deleted["enabled"] = True
                service_price_deleted["quantity"] = order_instance.quantity
                participants = ParticipantsOrderPrice.objects.filter(order_price=order_instance)
                participants_list = []
                for participant in participants:
                    participant_dic = {}
                    participant_dic["firstName"] = participant.firstname
                    participant_dic["lastName"] = participant.lastname
                    participants_list.append(participant_dic)
                service_price_deleted["participants"] = participants_list

                service_prices_list.append(service_price_deleted)
            service_item["prices"] = service_prices_list

            list_excluded_dates = list(service.excluded_dates.all())
            list_excluded_dates = [get_excluded_dates(excluded_date) for excluded_date in list_excluded_dates ]

            service_item["excluded_dates"] = list_excluded_dates

            list_schedule = service.schedule.all()
            
            list_schedule = [get_schedule(schedule) for schedule in list_schedule ]
            service_item["schedule"] = list_schedule

            info_order["service"] = service_item


        countries = list(Country.objects.all().values("id", "name"))

        return super(OrderAdmin, self).changeform_view(request, object_id, form_url, {
            'info_service':json.dumps(service_data, default=datetime_serializer),
            'email_send': json.dumps(email_send),
            'countries': json.dumps(countries),
            'payment_type':json.dumps(payment_type_list),
            'distribution_channels':json.dumps(distribution_channel_list),
            'company_agents':json.dumps(company_agents),
            'fields_help': json.dumps(fields_help),
            'info_order': json.dumps(info_order),
            'id_order': id_order,
            'reference_code': reference_code
        })
    def get_urls(self):
        urls = super(OrderAdmin, self).get_urls()

        return [
            url(r'^save-order/$', RegisterOrderView.as_view(),name="save_order"),
            # url(r'^register-office/$', RegisterOfficeView.as_view(),name="register_office"),
            url(r'^delete-order/$', DeleteOrderView.as_view(),name="delete_order"),
            url(r'^services-list/$', ServicesListView.as_view(),name="services_list"),
            url(r'^orders-list/$', OrdersListView.as_view(),name="orders_list"),
            url(r'^service-order-detail/$', ServiceOrderDetailView.as_view(),name="service_order_detail"),
            url(r'^order-canceled/$', OrderCanceledView.as_view(),name="order_canceled"),
            url(r'^order-confirmed/$', OrderConfirmedView.as_view(),name="order_confirmed"),
            url(r'^service-detail-pdf/$', ServiceDetailPdfView.as_view(),name="service_pdf"),
            url(r'^service-capacity/$', ServiceCapacityView.as_view(),name="service_capacity"),
        ] + urls
