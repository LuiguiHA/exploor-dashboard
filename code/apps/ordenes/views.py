from django.shortcuts import render, redirect
from django.db.models import Sum
from django.views.generic import TemplateView, View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from apps.canales.models import DistributionChannel
# from apps.oficinas.models import Office
from apps.servicios.models import Service, ServiceSchedule, ServicePrice, ServiceExcludedDates,                                          ServiceImage
from .models import PaymentType, Order, OrderPrice, ParticipantsOrderPrice
from apps.configuracion.models import Notification
from apps.servicios.serializers import ServiceSerializer
from apps.usuarios.models import Company, Country, User
from django.db import connection
from easy_pdf.rendering import render_to_pdf_response
from .utils import explicit_schedule, send_confirm_order_email
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.conf import settings
import json
import jsonpickle
import arrow
import time
import datetime
from dateutil import tz

# Create your views here.


class RegisterOrderView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(RegisterOrderView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        id_order = data.get("orderId")

        if id_order == 0:
            order_instance = Order()
            order_instance.creator = request.user
        else:
            order_instance = Order.objects.get(id=id_order)
            

        channel = data.get("channel")
        type_channel = channel.split("-")
        if request.user.can_set_point_of_sale():
            if type_channel[0] == "c":
                distribution_channel = DistributionChannel.objects.get(pk=int(type_channel[1]))
                order_instance.distribution_channel = distribution_channel
                order_instance.user_assigned = None
            else:
                user_assigned = User.objects.get(pk=int(type_channel[1]))
                order_instance.user_assigned = user_assigned
                order_instance.distribution_channel = None
        else:
            if not order_instance.user_assigned:
                order_instance.user_assigned = request.user

        firstname = data.get("firstName")
        order_instance.firstname = firstname

        lastname = data.get("lastName")
        order_instance.lastname = lastname

        client_country = data.get("client_country")
        country = Country.objects.get(pk=client_country)
        order_instance.client_country = country
        if country.code == 'PE':
            email_language = 'es'
        else:
            email_language = 'en'

        info_additional = data.get("infoAdditional")
        order_instance.info_additional = info_additional

        mobile_customer = data.get("mobileCustomer")
        order_instance.mobile_customer = mobile_customer

        reference = data.get("reference")
        order_instance.reference = reference
        # MODO DE PAGO
        payment_id = data.get("payment")
        payment = PaymentType.objects.get(id=payment_id)
        order_instance.payment_type = payment
        # SERVICIO SELECCIONADO PARA LA ORDEN
        service_id = data.get("serviceId")
        service = Service.objects.get(pk=service_id)
        order_instance.service = service

        out_date = arrow.get(data.get("date")).datetime
        order_instance.out_date = out_date

        email = data.get("email")
        order_instance.email = email

        hour = data.get("hour")
        hour = datetime.datetime.strptime(hour, '%I:%M %p').time()
        date = datetime.datetime.now()
        date = date.replace(
            hour=hour.hour,
            minute=hour.minute,
            second=hour.second
        )
        hour = arrow.get(date, tz.gettz('America/Lima'))\
            .shift(hours=+5).format('HH:mm a')
        order_instance.hour = hour
        order_instance.price_total = data.get("priceTotal")
        order_instance.confirmed = data.get('orderConfirmed')
        order_instance.save()

        prices = data.get("prices")
        OrderPrice.objects.filter(order=order_instance).delete()
        quantity = 0
        for price in prices:
            min_people = None
            max_people = None
            group_applied_to = None
            if price.get("enabled"):
                order_price = OrderPrice()
                order_price.name = price.get("category__name")
                order_price.quantity = price.get("quantity")
                quantity = quantity + order_price.quantity
                if price.get("category__name") == "Grupo":
                    min_people =  price.get("min_people")
                    max_people = price.get("max_people")
                    group_applied_to = price.get("group_price_applied_to")
                service_price = ServicePrice.objects.get(pk=price.get("id"))
                order_price.service_price = service_price
                order_price.min_people = min_people
                order_price.max_people = max_people
                order_price.group_applied_to = group_applied_to
                order_price.price = price.get("price")
                order_price.order = order_instance
                order_price.save()
                participants = price.get("participants")
                for participant in participants:
                    participant_instance = ParticipantsOrderPrice()
                    firstname_participant = participant.get("firstName")
                    participant_instance.firstname = firstname_participant
                    lastname_participant = participant.get("lastName")
                    participant_instance.lastname = lastname_participant
                    participant_instance.order_price = order_price
                    participant_instance.save()
        
        order_instance.reserved_quantity = quantity
        order_instance.save()
        order_date = arrow.get(order_instance.created_at).format('YYYY-MM-DD')
        date_formated = arrow.get(data.get('date')).format('YYYY-MM-DD')
        if data.get('emailSend') is False and data.get('orderConfirmed'):
            order_instance.email_confirm_send = True
            order_instance.save()
            send_confirm_order_email(order_instance, request.user, date_formated, data.get('hour'),service.title.capitalize(), email_language, order_date)
            return JsonResponse({'state': True})
        if data.get('resend') and data.get('orderConfirmed'):
            send_confirm_order_email(order_instance, request.user, date_formated, data.get('hour'), service.title.capitalize(), email_language, order_date)
            return JsonResponse({'state': True})
       
        return JsonResponse({'state': True})


# class RegisterOfficeView(View):
#     @method_decorator(csrf_exempt)
#     def dispatch(self, request, *args, **kwargs):
#         return super(RegisterOfficeView, self).dispatch(request, *args, **kwargs)

#     def post(self, request, *args, **kwargs):
#         data = json.loads(request.body)
#         name_office = data.get("nameOffice")
#         name_office = name_office.lower()
#         company = request.user.profile.company
#         company_id = request.user.profile.company.id

#         with connection.cursor() as cursor:
#             cursor.execute(
#                 """SELECT name from oficinas_office
#                     WHERE LOWER(name) = %s AND company_id = %s""", [name_office,company_id]
#             )
#             db_all_rows = cursor.fetchall()

#         # office_exists = Office.objects.filter(name=name_office,company=company).exists()
#         if db_all_rows:
#             return JsonResponse({'state': False})
#         else:
#             office_instance = Office()
#             office_instance.name = name_office
#             office_instance.company = company
#             office_instance.save()
#             obj_office = {}
#             obj_office["value"] = office_instance.id
#             obj_office["label"] = office_instance.name
#             obj_office["deleted"] = office_instance.deleted

#             return JsonResponse({'state': True,'office':obj_office})

class DeleteOrderView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteOrderView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        order = Order.objects.get(id=data.get("orderId"))
        participants = order.reserved_quantity
        order.delete()

        return JsonResponse({'success': True, 'participants_removed': participants})


class ServicesListView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ServicesListView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        # TODOS LOS SERVICIOS DE LAS COMPAÑIAS DEL USUARIO
        if request.user.is_superuser:
            companies = list(Company.objects.all())
        else:
            companies = list(Company.objects.filter(users=request.user))
        services = list(Service.objects.filter(company__id__in=[company.id for company in companies],                                           published=True, draft=False))
        schedules_list = []
        services_list = []
        # EL IF ES PARA VALIDAR QUE EL OPERADOR NO TIENE SERVICIOS PUBLICADOS O EL AGENTE NO TIENE SERVICIOS CON PRECIOS SETEADOS
        if len(services) > 0:
            services_empty = False
            # OBTENGO LOS DIAS DE LA SEMANA EN NUMERO EN EL RANGO DE FECHAS
            data = request.GET
            since = datetime.datetime.strptime(data.get('since'), "%Y-%m-%d").date()
            until = datetime.datetime.strptime(data.get('until'), "%Y-%m-%d").date()
            days = (until - since).days + 1
            days_id = []
            date = since
            for i in range(days):
                days_id.append(date.isoweekday())
                date = date + datetime.timedelta(days=1)
            # OBTENGO SOLO LOS HORARIOS DE LOS DIAS DE SEMANA SOLICITADOS
            schedules = list(ServiceSchedule.objects.filter(service__id__in=[service.id for service in                          services], day__in=days_id))
            # schedules_list = []
            date = datetime.datetime.now()
            for schedule in schedules:
                date = date.replace(
                        hour=schedule.hour.hour,
                        minute=schedule.hour.minute,
                        second=schedule.hour.second)
                dic = {}
                dic['service_id'] = schedule.service.id
                dic['service_company'] = schedule.service.company.name.capitalize()
                dic['since'] = schedule.service.since
                dic['until'] = schedule.service.until
                dic['people_total'] = schedule.service.max_people
                dic['day'] = schedule.day
                dic['hour_original'] = arrow.get(date, tz.gettz('America/Lima')).format('HH:mm')
                dic['hour'] = arrow.get(date, tz.gettz('America/Lima')).shift(hours=-5).format('hh:mm A')
                schedules_list.append(dic)
            if len(schedules_list) > 0:
                schedules_list = list(explicit_schedule(schedules_list, since, days))
                for schedule in schedules_list:
                    if schedule.get('service_id') != None:
                        has_mine = Order.objects.filter(service__id=schedule['service_id'], out_date=schedule['date'], hour=schedule['hour_original'], creator=request.user).exists()
                        schedule['has_mine'] = has_mine
                        reserved_quantity = Order.objects.filter(service__id=schedule['service_id'], 
                                                                out_date=schedule['date'], 
                                                                hour=schedule['hour_original'], 
                                                                canceled=False).aggregate(Sum('reserved_quantity'))
                        reserved_quantity = reserved_quantity['reserved_quantity__sum']
                        if schedule['people_total'] == 0:
                            schedule['available'] = 0
                        else:
                            schedule['available'] = schedule['people_total'] - reserved_quantity if reserved_quantity else schedule['people_total']
                        schedule['reserved'] = reserved_quantity if reserved_quantity else 0
                        users = []
                        orders = list(Order.objects.filter(service__id=schedule['service_id'], out_date=schedule['date'], hour=schedule['hour_original']))
                        for order in orders:
                            dic = {}
                            dic['name'] = order.firstname + ' ' + order.lastname
                            dic['email'] = order.email
                            users.append(dic)
                        schedule['users'] = users
                # OBTENGO SOLO LOS SERVICIOS CONTENIDOS EN LOS HORARIOS FILTRADOS
                services = list(Service.objects.filter(id__in=[ schedule.service.id for schedule in schedules]))
                # services_list = []
                for service in services:
                    dic = {}
                    dic['id'] = service.id
                    dic['service_name'] = service.title.capitalize()
                    excluded_dates = list(ServiceExcludedDates.objects.filter(service__id=service.id))
                    excluded_dates = [ [datetime.datetime.strftime(excluded.since, '%Y-%m-%d'), 
                                        datetime.datetime.strftime(excluded.until, '%Y-%m-%d')
                                    ] for excluded in excluded_dates
                                    ]
                    dic['excluded_dates'] = excluded_dates
                    images = ServiceImage.objects.filter(service=service).order_by('id')[:1]
                    dic['photo'] = images[0].image.medium.url if images.count() else None
                    services_list.append(dic)
            else:
                for i in range(days):
                    dic = {}
                    date = since
                    dic['date'] = date.strftime('%Y-%m-%d')
                    date = date + datetime.timedelta(days=1)
                    schedules_list.append(dic)
        else:
            services_empty = True

        return JsonResponse({'success': True, 'services': services_list, 'schedules': schedules_list, 'services_empty': services_empty })


class OrdersListView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(OrdersListView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        data = request.GET
        hour = data.get('hour')
        date = data.get('date')
        service_id = data.get('service_id')
        orders = list(Order.objects.filter(service__id=service_id, out_date=date, hour=hour))
        orders_list = []
        for order in orders:
            dic = {}
            dic['id'] = order.id
            dic['client_name'] = order.client_name()
            dic['client_number'] = order.mobile_customer
            dic['client_email'] = order.email
            dic['code'] = order.reference
            currency = order.service.currency.symbol
            dic['amount'] = currency + '{:.2f}'.format(order.price_total)
            dic['reserved_quantity'] = order.reserved_quantity
            dic['type_payment'] = order.payment_type.name
            dic['creator'] = order.creator.get_full_name()
            dic['confirmed'] = order.confirmed
            order_prices = list(OrderPrice.objects.filter(order=order))
            prices = ''
            for order_price in order_prices:
                prices = prices + order_price.name + ' (' + str(order_price.quantity) + ')' + ' | '
            dic['prices_type'] = prices[:-3]
            dic['canceled'] = order.canceled
            orders_list.append(dic)
        
        return JsonResponse({'success': True, 'orders': orders_list})


class ServiceOrderDetailView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ServiceOrderDetailView, self).dispatch(request, *args, **kwargs)
    
    def get(self, request, *args, **kwargs):
        data = request.GET
        service = Service.objects.filter(id=data.get('id'))
        service_detail = ServiceSerializer(service, many=True).data
        return JsonResponse({'success': True, 'service_detail': service_detail})         


class ServiceDetailPdfView(View):
    template_name = "admin/ordenes/order/service_detail.html"
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ServiceDetailPdfView, self).dispatch(request, *args, **kwargs)
    
    def get(self, request, *args, **kwargs):
        data = request.GET
        service = Service.objects.filter(id=data.get('id'))
        context = dict(ServiceSerializer(service, many=True).data[0])
        schedules = context.get('schedule')
        days = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo']
        for schedule in schedules:
            schedule['day_index'] = days[schedule.get('day_index') - 1]
            schedule_hours = []
            for hour in schedule.get('hours'):
                formated_hour = arrow.get('2018-03-16 ' + hour, 'YYYY-MM-DD HH:mm').shift(hours=-5).format('HH:mm A')
                schedule_hours.append(formated_hour)
            schedule['hours'] = schedule_hours
        context['schedule'] = schedules

        context['pdf'] = True
        pdf = render_to_pdf_response(request, self.template_name, context, download_filename="Detalle-de-servicio.pdf")
        pdf['Content-Disposition'] = 'attachment; filename=Detalle-de-servicio.pdf'
        return pdf


class OrderCanceledView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(OrderCanceledView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        data = request.GET
        order = Order.objects.get(id=data.get('id'))
        order.canceled = True
        order.canceled_reason = data.get('note')
        order.save()
        return JsonResponse({'success': True, 'participants_removed': order.reserved_quantity})

        
class OrderConfirmedView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(OrderConfirmedView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        order = Order.objects.get(id=data.get('id'))
        order.confirmed = True
        order.email_confirm_send = True
        order.save()

        hour_service = data.get('hour_service')
        date_service = data.get('date_service')
        name_service = data.get('name_service')

        order_date = arrow.get(order.created_at).format('YYYY-MM-DD')

        if order.client_country.code == 'PE':
            email_language = 'es'
        else:
            email_language = 'en'

        send_confirm_order_email(order, request.user, date_service, hour_service, name_service, email_language, order_date)

        return JsonResponse({'success': True})


class ServiceCapacityView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ServiceCapacityView, self).dispatch(request, *args, **kwargs)
    
    def get(self, request, *args, **kwargs):
        data = request.GET
        service = Service.objects.get(id=data.get('service_id'))
        reserved_quantity = Order.objects.filter(service=service, 
                                                out_date=data.get('date'), 
                                                hour=data.get('hour'), 
                                                canceled=False).aggregate(Sum('reserved_quantity'))
        reserved_quantity = reserved_quantity['reserved_quantity__sum']
        if service.max_people == 0:
            available = 0
        else:
            available = service.max_people - reserved_quantity if reserved_quantity else service.max_people
        return JsonResponse({'success': True, 'available': available})
