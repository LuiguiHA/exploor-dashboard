from django.shortcuts import render
from django.db import connection
from django.views.generic import TemplateView, View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.core.mail import send_mail
from django.contrib.auth.models import Permission
from django.template.loader import render_to_string
from django.shortcuts import render, redirect
# from django.contrib.auth.models import User
from apps.usuarios.models import User as UserExploor
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.conf import settings
from django.urls import reverse
from django.contrib.auth import authenticate, login
from django.contrib.auth.views import login as login_view
from django.http import HttpResponse
from apps.usuarios.models import Company, TypeUser

import random
import string
import json
import uuid

from usuarios.tokens import account_activation_token


class RegisterView(View):
    template_name = "register.html"

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(RegisterView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        details = None
        if 'details' in request.session and request.session.get("details"):
            details = dict(request.session.get("details"))
            del request.session["details"]
        return render(request, self.template_name, {'details': details})

    def post(self, request, *args, **kwargs):

        data = json.loads(request.body)

        company_name_preview = data.get("companyName")
        company_name = company_name_preview.lower().replace(" ","")
        company_web_url = data.get("companyWebUrl")
        name = data.get("name")
        lastname = data.get("lastname")
        email = data.get("email")
        password = data.get("password")
        type_of_user = data.get("typeOfUserError")
        type_user = TypeUser.objects.get(id=type_of_user)

        with connection.cursor() as cursor:
            cursor.execute(
                """SELECT name from usuarios_company
                    WHERE LOWER(REPLACE(name, ' ', '')) = %s""", [company_name]
            )
            db_all_rows = cursor.fetchall()

        if db_all_rows:
            return JsonResponse({'success': False, 'company': db_all_rows})
        else:
            # pass
            identifier = uuid.uuid4().hex
            # username = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(10))
            user = UserExploor.objects.create_user(
                identifier=identifier,
                email=email,
                password=password,
                type_user=type_user
            )
            user.is_active = False
            user.first_name = name
            user.last_name = lastname

            # Create Company
            company = Company.objects.create(name=company_name,company_web_url=company_web_url)
            company.save()

            user.save()
            company.users.add(user)

            permission = Permission.objects.get(codename='add_service')
            user.user_permissions.add(permission)
            permission = Permission.objects.get(codename='change_service')
            user.user_permissions.add(permission)
            permission = Permission.objects.get(codename='delete_service')
            user.user_permissions.add(permission)

            permission = Permission.objects.get(codename='add_office')
            user.user_permissions.add(permission)
            permission = Permission.objects.get(codename='change_office')
            user.user_permissions.add(permission)
            permission = Permission.objects.get(codename='delete_office')
            user.user_permissions.add(permission)

            permission = Permission.objects.get(codename='add_order')
            user.user_permissions.add(permission)
            permission = Permission.objects.get(codename='change_order')
            user.user_permissions.add(permission)
            permission = Permission.objects.get(codename='delete_order')
            user.user_permissions.add(permission)

            current_site = get_current_site(request)

            # try:
            email_template = render_to_string(
                "emails/confirm_registration.html",
                {
                    'user':user,
                    'domain':current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                }
            )
            send_mail(
                'Bienvenido!',
                'Texto plano',
                settings.DEFAULT_FROM_EMAIL,
                [email],
                fail_silently=False,
                html_message=email_template,
            )
            # except:
            #     print("Fail silently email send")

            return JsonResponse({'success': True})

class Activate(View):
    def get(self, request, uidb64, token):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = UserExploor.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, UserExploor.DoesNotExist):
            user = None

        if user is not None and account_activation_token.check_token(user, token):
            user.is_active = True
            user.save()
            login(request, user, backend="project.backends.EmailBackend")
            return redirect('admin:index')
        else:
            return redirect('admin:index')


class ResendEmail(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ResendEmail, self).dispatch(request, *args, **kwargs)


    def post(self, request, *args, **kwargs):

        data = json.loads(request.body)

        company_name = data.get("companyName")
        company_web_url = data.get("companyWebUrl")
        name = data.get("name")
        lastname = data.get("lastname")
        email = data.get("email")

        current_site = get_current_site(request)

        try:
            user = User.objects.get(
                email=email,
                first_name=name,
                last_name=lastname,
            )

            email_template = render_to_string(
                "emails/confirm_registration.html",
                {
                    'user':user,
                    'domain':current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                }
            )
            send_mail(
                'Bienvenido!',
                'Texto plano',
                settings.DEFAULT_FROM_EMAIL,
                [email],
                fail_silently=False,
                html_message=email_template,
            )

        except:
            pass

        return JsonResponse({'success': False})


class HTMLView(View):
    def render(self, template, context={}):
        return render(self.request, template, context)


class LoginSuccess(HTMLView):

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(reverse('admin:index'))
        return login_view(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(reverse('admin:index'))
        return login_view(request, *args, **kwargs)


class ValidateEmailView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ValidateEmailView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        email = data.get("email")
        email = email.lower()
        try:
            UserExploor.objects.get(email=email)
            return JsonResponse({'success': True})
        except UserExploor.DoesNotExist:
            return JsonResponse({'success': False})
