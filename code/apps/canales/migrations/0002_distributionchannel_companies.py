# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-12 19:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('canales', '0001_initial'),
        ('usuarios', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='distributionchannel',
            name='companies',
            field=models.ManyToManyField(blank=True, to='usuarios.Company'),
        ),
    ]
