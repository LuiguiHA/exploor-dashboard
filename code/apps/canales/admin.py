from django.contrib import admin
from django.conf.urls import url
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib import admin
from django.urls import reverse
from django.core import serializers
from django.shortcuts import redirect
from .models import DistributionChannel
from .forms import DistributionChannelForm


@admin.register(DistributionChannel)
class DistributionChannelAdmin(admin.ModelAdmin):
    search_fields = ['name']
    fields = ['name', 'companies']
    list_display = ['name', 'related_companies', 'tokenApi']
    form = DistributionChannelForm
