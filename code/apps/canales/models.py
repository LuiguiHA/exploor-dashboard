from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE
from apps.servicios.models import DeletedObjectManager
from safedelete.config import DELETED_VISIBLE_BY_PK
from safedelete.signals import post_softdelete
from django.dispatch import receiver
from datetime import datetime

from safedelete.models import SafeDeleteModel
from safedelete.managers import SafeDeleteManager
class DistributionChannelManager(SafeDeleteManager):
    _safedelete_visibility = DELETED_VISIBLE_BY_PK
    


class DistributionChannel(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE
    objects = DistributionChannelManager()

    name = models.CharField(
        max_length=50,
        verbose_name="Nombre"
    )

    created_at = models.DateTimeField(
       auto_now_add=True,
       editable=False
    )

    updated_at = models.DateTimeField(
       auto_now=True,
       editable=False
    )

    companies = models.ManyToManyField(
        "usuarios.Company",
        blank=True,
        verbose_name="Empresas"
    )

    user = models.ForeignKey(
        "usuarios.User",
        blank=True,
        verbose_name="UsuarioChannel"
    )

    token = models.TextField(
        null=True
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Canal de distribución"
        verbose_name_plural = "Canales de distribución"

    def tokenApi(self):
        return "<span style='word-wrap:break-word; width:500px; display:block'>" + self.token + "</span>"
    tokenApi.short_description = "API token"
    tokenApi.allow_tags = True

    def related_companies(self):
        companies = list(self.companies.all().values("name","id"))
        if companies:
            companies = ["{} (id={})".format(company.get("name"),company.get("id")) for company in companies]
            companies = "<br> ".join(companies)
            return companies
        else:
            return "sin empresas"
    related_companies.short_description = "Empresas relacionadas"
    related_companies.allow_tags = True


@receiver(post_softdelete, sender=DistributionChannel)
def delete_channel_profile(sender, instance, **kwargs):
    user = instance.user
    user.delete()
