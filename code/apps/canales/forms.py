from django import forms
from rest_framework_jwt.settings import api_settings
from apps.usuarios.models import User as UserExploor
from .models import DistributionChannel
import random
import string
import uuid
from django.db import connection
from django.conf import settings
from django.contrib.admin.widgets import FilteredSelectMultiple


class DistributionChannelForm(forms.ModelForm):

    def clean_name(self):
        name = self.cleaned_data['name']
        name = name.lower()
        channel_id = self.instance.pk if self.instance.pk else 0
        with connection.cursor() as cursor:
            cursor.execute(
                """SELECT name from canales_distributionchannel
                    WHERE LOWER(name) = %s AND deleted IS NULL AND id != %s""", [name, channel_id]
            )
            db_all_rows = cursor.fetchall()

        if db_all_rows:
            raise forms.ValidationError('Ya existe un canal con este nombre')

        return name

    def save(self, commit=True):
        is_new = not self.instance.pk
        channel = super(DistributionChannelForm, self).save(commit=False)

        if is_new:
            identifier = uuid.uuid4().hex
            email = channel.name.replace(' ', '') + '@' + uuid.uuid4().hex + '.com'

            user_channel = UserExploor.objects.create_user(
                email=email,
                identifier=identifier,
                password='m0rd0n3z',
                type_user=4
            )
            user_channel.save()
            user_channel.first_name = identifier
            user_channel.last_name = identifier
            user_channel.is_active = True
            # user_channel.profile.distribution_channel = channel
            user_channel.save()
            channel.user = user_channel
            # CREATE TOKEN
            jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
            jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
            payload = jwt_payload_handler(user_channel)
            payload["identifier"] = payload["identifier"]
            payload["username"] = payload["username"]
            token = jwt_encode_handler(payload)
            # user_channel.profile.token = token
            channel.token = token
            channel.save()

        channel.save()
        return channel

    class Meta:
        widgets = {"companies": FilteredSelectMultiple("Empresas", is_stacked=False), }

    class Media:
        css = {
            'all': (settings.STATIC_URL + 'css/canales.css', )
        }

        js = (settings.STATIC_URL + 'js/canales.js', )
