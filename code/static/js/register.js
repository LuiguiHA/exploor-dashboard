
Vue.use(VeeValidate, {events: 'input', fieldsBagName: 'fieldsVee', errorBagName: 'errorsVee'})
VeeValidate.Validator.setLocale("es")

var BeatLoader = VueSpinner.BeatLoader

var app = new Vue({
    el: '#app',
    components: {
        BeatLoader
    },
    delimiters: ['${', '}'],
    data: {
        form:{
            name:window.first_name,
            lastname:window.last_name,
            email:window.email,
            password:"",
            companyName:"",
            companyWebUrl:"",
            typeOfUserError: ""
        },
        step:1,
        toggleAgency:false,
        toggleOperator:false,
        typeOfUserError:false,
        saving:false,
        savingError:false,
        validatingEmail:false,
        activeButton: ''
    },
    created() {
        document.getElementById("container").style.display = "block";
    },
    watch: {

        step: function(newValue){

        }
      },
    methods: {
        clickAgency: function(){
            this.activeButton = 'agency'
            // if(newValue){
                this.toggleOperator = false
                this.toggleAgency = true
                this.typeOfUserError = false
                this.form.typeOfUserError = 2
            // }
        },
        clickOperator: function(){
            this.activeButton = 'operator'
            // if(newValue){
                this.toggleAgency = false
                this.toggleOperator = true
                this.typeOfUserError = false
                this.form.typeOfUserError = 1
            // }
        },
        resendEmail: function(){
            new Noty({
                type:'success',
                text: 'Email enviado!',
                timeout: 3000,
                killer:true,
                force:true
            }).show();
            axios.post('/resend', this.form)
            .then(function (response) {
                if(!response.body.success){
                    this.savingError = true
                }
                this.saving = false
            })
            .catch(function (error) {
                this.saving = false
                this.savingError = true
            });
        },
        nextStep: function(){
            this.$validator.validateAll().then((result) => {

                if (result) {
                    if (this.step==1){
                        this.validatingEmail = true
                        this.savingError = false
                        axios.post('validate-email/',this.form)
                        .then((response) =>{
                            this.validatingEmail = false

                            if(response.data.success == true){ //si el email fue encontrado
                                this.savingError = true
                                return
                            } else{
                                this.step +=1
                            }
                        })
                    }else if(this.step == 2){
                        if(this.toggleAgency || this.toggleOperator){
                            this.step += 1
                            this.typeOfUserError = false
                        } else {
                            this.typeOfUserError = true
                            }
                        }else if(this.step == 3){
                            this.saving = true
                            this.savingError = false
                            axios.post('/', this.form)
                            .then( (response) => {
                                this.saving = false
                                if(response.data.success == false){
                                    this.savingError = true
                                    return
                                }

                                this.step += 1
                            })
                            .catch( (error) => {
                                this.saving = false
                                this.savingError = true
                            });
                        } else {
                            if(this.step < 4){
                                this.step += 1
                            }
                        }
                }
            });
        },
        prevStep: function(){
            if(this.step > 1){
                this.step -= 1
            }
        }
      }
  })
