Vue.use(VeeValidate,{fieldsBagName:'fieldsVee',errorBagName:'errorsVee', events:"input|change"})
VeeValidate.Validator.setLocale("es")
var BeatLoader = VueSpinner.BeatLoader
var app = new Vue({
  el: '#agent',
  delimiters: ['${', '}'],
  components: {
      BeatLoader
  },
  data: {
    countryUrl: window.countryUrl,
    countries: window.countries,
    cities: [{'value': 0, 'text': 'Selecciona tu ciudad'}],
    step: 1,
    activeButton: '',
    typeOfUserError: false,
    saving: false,
    savingError: false,
    validatingEmail: false,
    validatingAgent: false,
    loadingCountries: false,
    image: null,
    imageUrl: '',
    form: {
      companyName: '',
      companyWebUrl: '',
      companyEmail: '',
      companyAddress: '',
      companyRuc: '',
      companyPhone: '',
      companyCity: 0,
      companyCountry: 0,
      toggleAgent: null,
      name: '',
      lastName: '',
      password: '',
      email: window.email,
      publicKey: '',
      privateKey: ''
    }
  },
  watch: {
    "form.companyCountry": function () {
      this.loadingCountries = true
      if (this.form.companyCountry == 0){
        this.cities = [{'value': 0, 'text': 'Selecciona tu ciudad'}]
        this.$nextTick(function (){
          this.form.companyCity = 0
          this.loadingCountries = false
        }.bind(this))
      }
      else{
        axios.post(this.countryUrl, {
          countryId: this.form.companyCountry
        })
        .then((response) =>{
          var cities = response.data.cities
          cities.unshift({'value': 0, 'text': 'Selecciona tu ciudad'})
          this.cities = cities
          if (this.cities.length > 1){
            this.form.companyCity = 0
          }
          else{
            this.form.companyCity = ''
          }
          this.$nextTick(function (){
            this.loadingCountries = false
          }.bind(this))
        })
      }
    }
  },
  mounted () {
    document.getElementById('container').style.display = 'block';
    // this.validateKeys();
  },
  methods:{
    clickCompany: function () {
      this.activeButton = 'company'
      this.typeOfUserError = false
      this.form.toggleAgent = 1
    },
    clickPerson: function () {
      this.activeButton = 'person'
      this.typeOfUserError = false
      this.form.toggleAgent = 2
    },
    nextStep: function(){
        this.$validator.validateAll().then((result) => {

            if (result) {
                if (this.step==1){
                  if(this.form.toggleAgent){
                    this.step += 1
                    this.typeOfUserError = false
                  } else {
                    this.typeOfUserError = true
                  }
                  return
                }
                if(this.step == 2){
                  this.validatingEmail = true
                  this.savingError = false
                  axios.post('/validate-email/',{'email': this.form.email})
                  .then((response) =>{
                      this.validatingEmail = false

                      if(response.data.success == true){ //si el email fue encontrado
                          this.savingError = true
                      } else{
                        this.step +=1
                      }
                  })
                }
                if(this.step == 3){
                  this.validatingAgent = true
                  this.saving = true
                  var formData = new FormData()
                  formData.append('image', this.image)
                  formData.append('agent', JSON.stringify(this.form))
                  axios.post('', formData)
                  .then( (response) => {
                    this.validatingAgent = false
                    this.saving = false
                    if(response.data.success == false){
                        this.savingError = true
                    }
                    else{
                      this.step += 1
                    }
                  })
                }
              }
        });
    },
    prevStep: function(){
        if(this.step > 1){
            this.step -= 1
        }
    },
    handleAvatarPreview: function(file){
      this.imageUrl = file.url
      this.image = file.raw
    },
    removeLogo: function (){
      this.imageUrl = ''
      this.image = null
    },
    //  REGLAS PARA VEE-Validate
    validateKeys: function(){
      VeeValidate.Validator.extend('validatepublickey', {
        messages: {
          es: function(field, args){
            return ""
          },
        },
        validate: function(value) {
          var regex = /pk_live_[A-Za-z0-9]{16}$/g;
          console.log(value)
          console.log(regex.test(value))
          return regex.test(value)
        }
      })
    },
  }
})
