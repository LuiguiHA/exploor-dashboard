$(document).ready(function(){
    window.addEventListener('online', updateStatus)
    window.addEventListener('offline', updateStatus)
});

function updateStatus() {
    if (typeof window.navigator.onLine === 'undefined') {
      isOnline = true
    } else {
      isOnline = window.navigator.onLine
    }
    var msg = "Estás trabajando sin conexión a internet."
    if (window.userName != ''){
      msg = window.userName + "! estás trabajando sin conexión a internet."
    }
    if (!isOnline){
      new Noty({
        type:"error",
        text: msg,
        layout: "top",
        timeout: false,
        force: true,
        killer: true
      }).show();
    } else {
      Noty.closeAll()
    }
}